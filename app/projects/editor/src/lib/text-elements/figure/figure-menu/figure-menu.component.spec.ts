import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FigureMenuComponent } from './figure-menu.component';

describe('FigureMenuComponent', () => {
  let component: FigureMenuComponent;
  let fixture: ComponentFixture<FigureMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FigureMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FigureMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
