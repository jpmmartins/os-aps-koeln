import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, merge } from 'rxjs';
import { take, map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CiteService {

  constructor(private store: Store<any>) { }

}
