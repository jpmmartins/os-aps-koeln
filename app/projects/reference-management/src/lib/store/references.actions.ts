import { createAction, props } from '@ngrx/store';
import { CitationWithId } from '@sciflow/cite';

export const uploadFile = createAction('[File] Uploading file');
/** Updates the list of references available in the project */
export const setReferences = createAction('[ReferenceManagement] Add references', props<{ references: any[]; }>());
/** An in-text citation has been updated */
export const updateCitation = createAction('[ReferenceManagement] Update citation in editor', props<{ citation: CitationWithId; }>());
export const addCitations = createAction('[ReferenceManagement] Add citations from editor', props<{ citations: CitationWithId[]; }>());
