import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CitationEditorComponent } from './references/citation-editor/citation-editor.component';
import { ReferencesComponent } from './references/references.component';

const routes: Routes = [
    { path: '', component: ReferencesComponent },
    { path: 'citation/:citationID', component: CitationEditorComponent },
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReferenceManagementRoutingModule { }
