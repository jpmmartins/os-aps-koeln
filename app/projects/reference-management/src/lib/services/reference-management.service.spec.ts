import { TestBed } from '@angular/core/testing';

import { ReferenceManagementService } from './reference-management.service';

describe('ReferenceManagementService', () => {
  let service: ReferenceManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReferenceManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
