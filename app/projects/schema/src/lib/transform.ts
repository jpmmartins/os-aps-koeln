import { SFNodeType } from "./types";

export interface SFDocumentNode<SFNodeType> {
    type: SFNodeType;
    attrs?: any;
    content?: SFDocumentNode<SFNodeType>[];
    text?: string;
    meta?: any;
    marks?: any[];
}

/**
 * Rendering engine that can be called with any document node.
 * Initial data and additional renderers may be provided at instantiation time.
 */
export interface RenderingEngine<OutputFormat> {

    /** renderers for all node types */
    renderers: { [key: string]: Renderer<OutputFormat> };

    constructor(renderers: { [key: string]: Renderer<OutputFormat> }, data?: any);

    /** renders a node to the output format. */
    render(node: SFDocumentNode<SFNodeType>, parent?: SFDocumentNode<SFNodeType>): Promise<OutputFormat>;
}

/** A renderer for a specific node type. */
export interface Renderer<OutputFormat> {

    /** the engine responsible for rendering the content 
     * (can be used from within Renderer.render to render child nodes)
     * */
    engine?: RenderingEngine<OutputFormat>;

    constructor(engine: RenderingEngine<OutputFormat>);

    render(node: SFDocumentNode<SFNodeType>, parent?: SFDocumentNode<SFNodeType>): Promise<OutputFormat | OutputFormat[]>;
}
