import { createAction, props } from '@ngrx/store';

export const updateTemplate = createAction('[Export] Setting the active template', props<{ template: any; }>());

export const updateMetadata = createAction('[Export] meta data updated', props<{ metaData: any; }>());
