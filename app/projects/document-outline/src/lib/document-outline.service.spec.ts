import { TestBed } from '@angular/core/testing';

import { DocumentOutlineService } from './document-outline.service';

describe('DocumentOutlineService', () => {
  let service: DocumentOutlineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentOutlineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
