import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OutlineComponent } from './outline/outline.component';

const routes: Routes = [
    { path: '', component: OutlineComponent }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class DocumentOutlineRoutingModule { }
