import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorManagementComponent } from './author-management/author-management.component';

const routes: Routes = [
    { path: '', component: AuthorManagementComponent },
    { path: 'author/:authorId', component: AuthorManagementComponent }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthorManagementRoutingModule { }
