import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, filter, switchMap, take, takeUntil } from 'rxjs/operators';

import { AuthorManagementNavigationComponent } from '../author-management-navigation/author-management-navigation.component';
import { AddAuthorComponent, BACKDROP_PORTAL_DATA } from '../add-author/add-author.component';
import { Apollo, gql } from 'apollo-angular';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { createAuthor, initializeUsers, maximized, setSelectedAuthor, updateAuthor } from '../author-management.actions';
import { getUser, selectSelectedAuthor } from '../author-management.reducer';
import { DcaRoutingService } from '../dca-routing.service';
import { selectAuthors, selectDocumentId } from 'projects/reference-app/src/app/app-state.reducer';
import { HelperService } from '../helper.service';
import { ComponentPortal, ComponentType, PortalInjector } from '@angular/cdk/portal';
import { PortalRef } from '../portal-ref';
import { AuthorService } from '../services/author.service';

export const createId = (): string => 'abcdefghijklmnopqrstuvwxyz'.charAt(Math.floor(Math.random() * 26)) + Math.random().toString(36).substr(2, 9);

export interface BackdropConfig {
  data?: any;
}

@Component({
  selector: 'sf-author-management',
  templateUrl: './author-management.component.html',
  styleUrls: ['./author-management.component.scss']
})
export class AuthorManagementComponent implements OnInit {
  portal: ComponentPortal<any>;

  @ViewChild('front', { static: true }) front: AuthorManagementComponent;
  @ViewChild('back', { static: true }) back: AuthorManagementComponent;

  stop$ = new Subject();

  document$ = this.helperService.getProjectAndDocument()
    .pipe(
      map(r => r?.documentId),
      filter(e => e != null),
      switchMap((documentId) =>
        {
          return this.authorService.addAuthor(documentId);
        }
      )
    );

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private authorService: AuthorService, private drs: DcaRoutingService, private helperService: HelperService, private apollo: Apollo, private snackBar: MatSnackBar, private ar: ActivatedRoute, private store: Store, private _injector: Injector) {

    // check route changes
    this.activatedRoute.params.subscribe(async (params) => {
      const selectedAuthor = await this.store.select(selectSelectedAuthor).pipe(take(1)).toPromise();
      const authors = await this.store.select(selectAuthors).pipe(take(1)).toPromise();
      // if the param has a newer id, navigate there if the author exists
      if (params.authorId && selectedAuthor !== params.authorId) {
        if (authors.some(author => author.authorId === params.authorId)) {
          this.store.dispatch(setSelectedAuthor({ authorId: params.authorId }));
        }
      }
      return;
    });

    // check selection changes
    this.store.select(selectSelectedAuthor).pipe(takeUntil(this.stop$)).subscribe(async (selectedAuthor) => {
      const authors = await this.store.select(selectAuthors).pipe(take(1)).toPromise();
      if (selectedAuthor && authors.some(author => author.authorId === selectedAuthor)) {
          this.store.dispatch(maximized());
      }
    });
  }

  async ngOnInit() {
    this.loadNavigation();
  }

  // portal: ComponentPortal<any>;

  attach(component: ComponentType<any>, config?: BackdropConfig): PortalRef<any, any> {
    const ref = new PortalRef(component, config);
    this.portal = new ComponentPortal(component, undefined, this._createInjector(ref, config!));
    return ref;
  }

  _createInjector(ref: PortalRef<any, any>, config: BackdropConfig): PortalInjector {
    const injectionTokens = new WeakMap<any, any>([
      [PortalRef, ref],
      [BACKDROP_PORTAL_DATA, config ? config.data : {}]
    ]);

    return new PortalInjector(this._injector, injectionTokens);
  }

  async setContext(context: boolean) {
    const userOptions = await this.store.select(getUser).pipe(filter(u => u != null), take(1)).toPromise();
    this.store.dispatch(initializeUsers({ user: {
        user: userOptions.user,
        userProfile: userOptions.userProfile,
        activeDocumentContext: context
      }}
      ));
  }

  async loadNavigation() {
    this.attach(AuthorManagementNavigationComponent).onResult().subscribe(async (data) => {
      switch (data.action) {
        case 'close':
          this.drs.closeDca();
          this.setContext(false);
          break;
        case 'add_author':
          this.loadAddAuthorBackdrop();
          break;
        case 'new_author':
          const authorId = createId();
          this.store.dispatch(updateAuthor({ author: createAuthor(authorId), updateProfile: false, clearData: false }));
          this.router.navigate(['author', authorId], { relativeTo: this.activatedRoute });
          break;
      }
    });
  }

  async loadAddAuthorBackdrop() {
    const documentvalue = {'data': {'document': {'id': 'ZG9jdW1lbnQ6ZWFlbXdxZm14azI=', 'inviteUrl': [{'role': 'Admin', 'url': 'https://app.sciflow.net/write/welcome/q5z7o5jidp_bf43356a-146a-4b77-a0f2-33e1012ddb5f/eaemwqfmxk2', '__typename': 'InviteUrl'}, {'role': 'Author', 'url': 'https://app.sciflow.net/write/welcome/q5z7o5jidp_3847e71e-87dc-441d-a66a-60d805d1b126/eaemwqfmxk2', '__typename': 'InviteUrl'}, {'role': 'CommentOnly', 'url': 'https://app.sciflow.net/write/welcome/q5z7o5jidp_5025d064-e9d6-4213-875c-fbbf7f1eee05/eaemwqfmxk2', '__typename': 'InviteUrl'}], '__typename': 'Document'}}, 'extensions': {'tracing': {'version': 1, 'startTime': '2021-08-17T16:33:18.165Z', 'endTime': '2021-08-17T16:33:18.338Z', 'duration': 173300139, 'execution': {'resolvers': [{'path': ['document'], 'parentType': 'Query', 'fieldName': 'document', 'returnType': 'Document', 'startOffset': 6805365, 'duration': 166042671}, {'path': ['document', 'id'], 'parentType': 'Document', 'fieldName': 'id', 'returnType': 'ID!', 'startOffset': 172890146, 'duration': 22742}, {'path': ['document', 'inviteUrl'], 'parentType': 'Document', 'fieldName': 'inviteUrl', 'returnType': '[InviteUrl]', 'startOffset': 172956728, 'duration': 38463}, {'path': ['document', 'inviteUrl', 0, 'role'], 'parentType': 'InviteUrl', 'fieldName': 'role', 'returnType': 'DocumentRole', 'startOffset': 173028037, 'duration': 14445}, {'path': ['document', 'inviteUrl', 0, 'url'], 'parentType': 'InviteUrl', 'fieldName': 'url', 'returnType': 'String', 'startOffset': 173053868, 'duration': 17798}, {'path': ['document', 'inviteUrl', 1, 'role'], 'parentType': 'InviteUrl', 'fieldName': 'role', 'returnType': 'DocumentRole', 'startOffset': 173098326, 'duration': 18303}, {'path': ['document', 'inviteUrl', 1, 'url'], 'parentType': 'InviteUrl', 'fieldName': 'url', 'returnType': 'String', 'startOffset': 173127764, 'duration': 11772}, {'path': ['document', 'inviteUrl', 2, 'role'], 'parentType': 'InviteUrl', 'fieldName': 'role', 'returnType': 'DocumentRole', 'startOffset': 173203525, 'duration': 14180}, {'path': ['document', 'inviteUrl', 2, 'url'], 'parentType': 'InviteUrl', 'fieldName': 'url', 'returnType': 'String', 'startOffset': 173228062, 'duration': 11295}]}}, 'cacheControl': {'version': 1, 'hints': [{'path': ['document'], 'maxAge': 0}, {'path': ['document', 'inviteUrl'], 'maxAge': 0}]}}};
// const document = await this.document$.pipe(take(1)).toPromise();
    this.attach(AddAuthorComponent, { data: { documentId: documentvalue.data.document.id, inviteUrl: documentvalue.data.document.inviteUrl } }).onResult().subscribe(async (data) => {
      switch (data.action) {
        case 'close':
          if (data.refetch) {
            const user = await this.store.select(getUser).pipe(filter(u => u != null), take(1)).toPromise();
            // this.documentApiService.populateAuthors(document.documentId, user?.user.userId);
          }
          this.loadNavigation();
          break;
      }
    });
  }

  ngOnDestroy() {
    this.stop$.next();
  }
}
