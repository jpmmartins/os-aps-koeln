import { NgModule } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { ApolloClientOptions, InMemoryCache, ServerError } from '@apollo/client/core';
import { createUploadLink } from 'apollo-upload-client';
import { HttpLink } from 'apollo-angular/http';
import { onError } from "@apollo/client/link/error";
import { NetworkError } from "@apollo/client/errors";
import { MatSnackBar } from '@angular/material/snack-bar';

export function createApollo(httpLink: HttpLink, snackBar: MatSnackBar): ApolloClientOptions<any> {
  const uri = '/api/graphql';

  const uploadLink = createUploadLink({
    uri,
    headers: {
      'X-Apollo-Operation-Name': 'uploadFile'
    }
  });

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.map(({ message, locations, path }) =>
        console.error(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        ),
      );

    if (networkError) {
      const error = networkError as ServerError;
      if (error.statusCode > 500) {
        snackBar.open(`An network error occured`);
        console.log(`[Network error]: ${networkError}`);
      } else {
        snackBar.open(`An error occured`);
        console.log(`[Network error]: ${networkError}`);
      }
    }
  });

  return {
    link: errorLink.concat(uploadLink),
    cache: new InMemoryCache(),
  };
}

@NgModule({
  imports: [
    ApolloModule
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink, MatSnackBar],
    },
  ],
})
export class GraphQLModule { }
