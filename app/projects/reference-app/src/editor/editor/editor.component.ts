import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenavContainer } from '@angular/material/sidenav';
import { Store } from '@ngrx/store';
import { createId } from '@sciflow/schema';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';

import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { CdkScrollable } from '@angular/cdk/scrolling';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentOutlineService } from 'document-outline';
import { PMEditorComponent, PM_EDITOR_CONFIG, updateDocumentState } from 'editor';
import { initializeUsers, minimized } from 'projects/author-management/src/lib/author-management.actions';
import { Plugin } from 'prosemirror-state';
import { ReferenceManagementService, setReferences } from 'reference-management';
import { FileService } from 'shared';
import { checkSelectedAuthor, getDocumentContext, getUser, selectIsDirty, selectSelectedAuthor } from '../../app/app-state.reducer';
import { selectIsDocumentLoaded } from '../../app/file.reducer';
import { ErrorDialogService } from '../error-handler/error-dialog/error-dialog.service';
import { ProjectService } from '../services/project.service';
import { selectOpenProjectDca } from '../userAction.reducer';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditorComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(MatSidenavContainer) sidenavContainer;
  @ViewChild('select') select;
  @ViewChild('dcaDrawer') dcaDrawer;
  @ViewChild(CdkScrollable, { static: true }) scrollable: CdkScrollable;

  @ViewChild('contextOutlet') contextOutlet;

  private stop$ = new Subject();

  editorHole: Portal<any>;

  id = createId();

  checkAuthorSelected$ = this.store.select(selectSelectedAuthor);
  dcaExpanded$ = this.store.select(checkSelectedAuthor);
  dcaDrawer$ = this.store.select(getDocumentContext);
  openProjectDca$ = this.store.select(selectOpenProjectDca);

  isDocumentLoaded$ = this.store.select(selectIsDocumentLoaded);

  @HostListener('window:beforeunload', ['$event'])
  async unloadNotification($event: any): Promise<void> {
    const isDirty = await this.store.select(selectIsDirty).pipe(take(1)).toPromise();
    if (isDirty) { $event.returnValue = true; }
  }

  constructor(
    private store: Store,
    private cdr: ChangeDetectorRef,
    private ar: ActivatedRoute,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private snackBar: MatSnackBar,
    private injector: Injector,
    private referenceManagementService: ReferenceManagementService,
    private documentOutlineService: DocumentOutlineService,
    private errorDialogService: ErrorDialogService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private projectService: ProjectService
  ) {
    this.iconRegistry.addSvgIcon('toc', this.sanitizer.bypassSecurityTrustResourceUrl('assets/sf-icons/toc.svg'));
    this.iconRegistry.addSvgIcon('authors', this.sanitizer.bypassSecurityTrustResourceUrl('assets/sf-icons/authors.svg'));
    this.iconRegistry.addSvgIcon('templates', this.sanitizer.bypassSecurityTrustResourceUrl('assets/sf-icons/templates.svg'));
    this.iconRegistry.addSvgIcon('references', this.sanitizer.bypassSecurityTrustResourceUrl('assets/sf-icons/references.svg'));
  }

  async ngOnInit(): Promise<void> {
    await this.openProjectDca$.subscribe(openProject => {
      if (openProject) {
        this.router.navigate([{ outlets: { context: 'project' } }], {
          relativeTo: this.activatedRoute
        });
        this.dcaDrawer?.open();
      }
    });
  }

  async canDeactivate(): Promise<boolean> {
    const isDirty = await this.store.select(selectIsDirty).pipe(take(1)).toPromise();
    if (isDirty) {
      if (confirm('You have unsaved changes! If you leave, your changes will be lost.')) {
        this.store.dispatch(updateDocumentState({ dirty: false }));
        return true;
      } else { return false; }
    }
    else { return true; }
  }

  private constructEditor(id: { id: string; projectId: string }, mode, plugins: Plugin[] = [], views = {}): void {
    const offset$ = new BehaviorSubject(-this.scrollable.getElementRef().nativeElement.getBoundingClientRect().top);
    this.scrollable.elementScrolled().pipe(map(_ => {
      return this.scrollable.measureScrollOffset('top') - this.scrollable.getElementRef().nativeElement.getBoundingClientRect().top;
    })).subscribe(offset$);

    const injector = Injector.create({
      providers: [{
        provide: PM_EDITOR_CONFIG, useValue: {
          id,
          mode,
          plugins,
          views,
          offset$
        }
      }]
    });

    if (this.editorHole?.isAttached) {
      this.editorHole.detach();
    }

    const editor = new ComponentPortal(PMEditorComponent, null, injector);
    this.editorHole = editor;
    this.cdr.detectChanges();
  }

  /*** Opens or closes a dca context route*/
  async toggleDca(context?: string): Promise<void> {
    this.store.dispatch(minimized());
    const outlet = this.activatedRoute.children.find(o => o.outlet === 'context')?.snapshot;
    if (context === outlet?.routeConfig?.path) { context = undefined; }

    if (!context) {
      this.router.navigate([{ outlets: { context: null } }], { relativeTo: this.activatedRoute });
    } else {
      this.router.navigate([{ outlets: { context } }], { relativeTo: this.activatedRoute });
    }
  }

  async ngAfterViewInit(): Promise<void> {
    const dca = this.activatedRoute.snapshot.children.find(c => c.outlet === 'context');

    // if the context is null or the provided target is the
    // same as the active one, close the dca
    if (dca?.routeConfig?.path) {
      this.setContext(true);
      this.dcaDrawer?.open();
    }

    this.ar.params.pipe(takeUntil(this.stop$))
      .pipe(filter(({ documentName }) => documentName))
      .subscribe(async ({ projectId, documentName, version }) => {

        // remove any old editors before we load the new one
        if (this.editorHole?.isAttached) {
          this.editorHole.detach();
        }

        if (version === 'original') {
          version = null;
        }

        const sb = this.snackBar.open(' ☕ Loading manuscript into the editor ..');

        const references = await this.projectService.getReferences(projectId);
        this.store.dispatch(setReferences({ references }));

        const nameArray = documentName?.split('.');
        const ext = nameArray?.length > 0 && nameArray[nameArray.length - 1];
        switch (ext) {
          case 'odt':
          case 'docx':
          case 'json':
            try {
              const result: any = await this.projectService.getDocument(projectId, documentName, { version });

              const id = FileService.getFileIdFromName(documentName);
              const { plugins: refMgmtPlugins, views: refMgmtViews } = this.referenceManagementService.getProsemirror(this.injector);
              const { plugins: documentOutlinePlugins, views: documentOutlineViews } = this.documentOutlineService.getProsemirror(this.injector);

              this.constructEditor({
                projectId,
                id
              }, 'full', [...refMgmtPlugins, ...documentOutlinePlugins], {
                ...refMgmtViews, ...documentOutlineViews
              });

            } catch (e: any) {
              // this.snackBar.open(e.message, 'Close', { duration: 5000 });
              console.error(e);
              this.errorDialogService.openDialog(e.message);
              this.router.navigate([`/write/${projectId}`]);
            }
            break;
          default:
            this.errorDialogService.openDialog(`No support for displaying ${ext}, yet`);
            this.router.navigate([`/write/${projectId}`]);
        }

        sb?.dismiss();
      });
  }

  async setContext(context: boolean): Promise<void> {
    const userOptions = await this.store.select(getUser).pipe(filter(u => u != null), take(1)).toPromise();
    this.store.dispatch(initializeUsers({
      user: {
        user: userOptions.user,
        userProfile: userOptions.userProfile,
        activeDocumentContext: context
      }
    }
    ));
  }

  onActivate(event) {
    this.dcaDrawer?.open();
  }

  onDeactivate(event) {
    this.dcaDrawer?.close();
  }

  ngOnDestroy(): void {
    this.stop$.next();
  }
}
