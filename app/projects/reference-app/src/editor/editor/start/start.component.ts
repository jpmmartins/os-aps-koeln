import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import generate from 'project-name-generator';
import { Observable } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { FileService } from 'shared';
import { ProjectService } from '../../services/project.service';
import { v4 as uuidv4 } from 'uuid';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {

  @ViewChild('fileUpload', { static: true }) fileUpload;

  instanceData = JSON.parse(this.document.getElementById('instance-data').innerHTML);
  projectId$ = this.route.params.pipe(map(({ projectId }) => projectId));
  project$ = this.projectId$.pipe(filter(projectId => projectId?.length > 0), switchMap(projectId => this.projectService.loadFiles(projectId).valueChanges.pipe(map((result) => {
    const project = result.data?.project;
    return {
      ...project,
      sizeInMb: Math.round(project.size / 1024 / 1024 * 100) / 100,
      resources: project.resources.map(r => ({
        ...r,
        type: r.Key.split('.').pop(),
        name: r.Key.replace(projectId + '/', '').replace('.' + r.Key.split('.').pop(), ''),
        latestVersion: r.versions?.[0] || { LastModified: r.LastModified }
      })).filter(r => ['docx', 'json'].includes(r.type))
    }
  }))));

  constructor(private router: Router, private fileService: FileService, private snackBar: MatSnackBar,
    private route: ActivatedRoute, private store: Store, private projectService: ProjectService, @Inject(DOCUMENT) private document: any) { }

  ngOnInit(): void {
  }

  openResource(resource) {
    const version = resource.latestVersion;
    if (resource.Key.indexOf('.json') === resource.Key.length - 5) {
      this.router.navigateByUrl(`/write/${resource.Key}/${version?.VersionId}`);
    } else {
      this.router.navigateByUrl(`/write/${resource.Key}`);
    }
  }

  public async onUpload(event): Promise<void> {
    event.preventDefault();
    event.stopPropagation();
    let projectId = this.route.snapshot.params?.projectId;
    if (!projectId) {
      // starting a new project
      projectId = generate().dashed + '-' + uuidv4();
    }
    const uploadFile: any = await this.fileService.uploadFile(projectId, (event.srcElement || event.target).files.item(0));
    this.fileUpload.nativeElement.value = '';
    if (!uploadFile) {
      this.snackBar.open('Could not process file', 'Close', { duration: 5000 });
    } else {
      console.log('Updated successfully', uploadFile);
      this.router.navigateByUrl(`/write/${uploadFile.key}`);
    }
  }
}
