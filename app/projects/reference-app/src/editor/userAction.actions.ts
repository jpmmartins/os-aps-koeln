import { createAction, props } from '@ngrx/store';

export const openProjectDca = createAction('[userAction] Open Project DCA', props<{ openProject: boolean }>());

/**
 * set Template.
 */
export const setTemplate = createAction('[userAction] set Template', props<{ template: string }>());
