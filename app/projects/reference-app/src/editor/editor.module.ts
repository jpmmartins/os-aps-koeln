import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorComponent } from './editor/editor.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { EditorModule as SfoEditorModule } from 'editor';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider'; 
import { MatIconModule } from '@angular/material/icon'; 
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { ProjectComponent } from './project/project.component';
import { MatListModule } from '@angular/material/list';
import { StartComponent } from './editor/start/start.component';
import { StoreModule } from '@ngrx/store';
import { projectFeatureKey, reducer } from './project/project.reducer';
import { editorFeatureKey, editorReducer } from './userAction.reducer';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EffectsModule } from '@ngrx/effects';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { DeactivateGuard } from './editor/deactivate-guard';
import { ErrorDialogComponent } from './error-handler/error-dialog/error-dialog.component';
import { MatDialogModule,MatDialogRef } from '@angular/material/dialog';
import { ErrorDialogService } from './error-handler/error-dialog/error-dialog.service';
import { PageNotFoundComponent } from './error-handler/page-not-found/page-not-found.component';
import { PreviewComponent } from './preview/preview.component';
import { MatCardModule } from '@angular/material/card';
import {  MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [EditorComponent, ProjectComponent, StartComponent, ErrorDialogComponent, PageNotFoundComponent, PreviewComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatToolbarModule,
    OverlayModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatCardModule,
    PortalModule,
    MatTooltipModule,
    MatToolbarModule,
    MatListModule,
    MatProgressBarModule,
    SfoEditorModule,
    MatDialogModule,
    StoreModule.forFeature(projectFeatureKey, reducer),
    StoreModule.forFeature(editorFeatureKey, editorReducer),
    RouterModule.forChild([
      {
        path: '', component: StartComponent
      },
      {
        path: ':projectId', component: StartComponent
      },
      {
        path: ':projectId/:documentName', component: PreviewComponent
      },
      {
        path: ':projectId/:documentName/:version', component: EditorComponent, canDeactivate: [DeactivateGuard], children: [
          { path: 'project', outlet: 'context', component: ProjectComponent },
          { path: 'authors', outlet: 'context', loadChildren: () => import('projects/author-management/src/public-api').then(m => m.AuthorManagementModule) },
          { path: 'references', outlet: 'context', loadChildren: () => import('projects/reference-management/src/projects').then(m => m.ReferenceManagementModule) },
          { path: 'outline', outlet: 'context', loadChildren: () => import('projects/document-outline/src/public-api').then(m => m.DocumentOutlineModule) },
          { path: 'export', outlet: 'context', loadChildren: () => import('projects/export/src/public-api').then(m => m.ExportModule) }
        ]
      }
    ])
  ],
  providers: [
    DeactivateGuard,
    {
      provide: MatDialogRef,
      useValue: {}
    },
    ErrorDialogService
  ],
  exports: [EditorComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class EditorModule { }
