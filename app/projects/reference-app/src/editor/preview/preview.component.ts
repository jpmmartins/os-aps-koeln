import { AfterViewInit, Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap, take } from 'rxjs/operators';
import { FileService } from 'shared';

import { stringify } from 'json-to-pretty-yaml';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements AfterViewInit {
  params$ = this.ar.params.pipe(map(({ projectId, documentName }) => ({ projectId, documentName })));
  preview$ = this.params$.pipe(
    switchMap(({ projectId, documentName }) => this.fileService.getPreview(projectId, documentName)),
    switchMap(async (result) => {
      if (result.errors) {
        const documentError = result.errors.find(error => error.path[0] === 'document' && error.path[1] === 'preview');
        console.error({ documentError, errors: result.errors }, result.preview, result.key);
        if (documentError.extensions?.errors) {
          const errors = documentError.extensions?.errors.map(e => ({
            ...e,
            html: e.payload.pandocNodeHTML ? this.domSanitizer.bypassSecurityTrustHtml(e.payload.pandocNodeHTML) : undefined
          }));
          console.log('pandoc errors', errors);
          return { key: result.key, errors, manuscript: result.manuscript };
        }

        return { key: result.key, errors: result.errors };
      }
      if (!result?.preview) { return { errors: [{ message: 'Could not load preview' }] }; }
      const preview = {
        key: result.key,
        ...result.preview,
        references: (result.preview?.references || []).map(ref => this.domSanitizer.bypassSecurityTrustHtml(stringify(ref))),
        html: this.domSanitizer.bypassSecurityTrustHtml(result?.preview?.html)
      };
      return { ...preview, manuscript: result?.manuscript };
    }));

  constructor(
    private ar: ActivatedRoute,
    private fileService: FileService,
    private domSanitizer: DomSanitizer,
    private router: Router
  ) { }

  async startEditing() {
    const { projectId, documentName } = this.ar.snapshot.params;
    const preview = await this.preview$.pipe(take(1)).toPromise();
    
    // save the manuscripts and extract all media files
    const files = await this.fileService.extractMedia(projectId, documentName, preview.assets);
    const manuscript = {
      ...preview.manuscript,
      files
    };
    const file = new File([JSON.stringify(manuscript, null, 2)], FileService.getFileIdFromName(documentName) + '.json', { type: 'application/json' });
    const firstDocument: any = await this.fileService.uploadFile(projectId, file);
    console.log(firstDocument?.version?.VersionId, { firstDocument, manuscript });
    this.router.navigateByUrl(`write/${firstDocument?.key}/${firstDocument?.version?.VersionId || 'unversioned'}`);
  }

  ngAfterViewInit(): void {
  }

}
