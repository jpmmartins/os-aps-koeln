# EditingUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli)

## Upgrading Angular

```
cd app
```
Make sure angular knows to use yarn (since we are not in the directory with the yarn.lock)
```
ng config --global cli.packageManager yarn
ng update @angular/cli @angular/core @angular/material @angular/animations
```
Since both Angular and our server use Webpack to build the code it might make sense to upgrade our serverside to the latest version used by Angular.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
