const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const glob = require('glob');

const path = require("path");
const fs = require("fs");

let entry = {
    'tdk/index.js': 'tdk/src/public-api.ts',
    'server/server.js': 'server/src/server.ts'
};

glob.sync('libs/*/src/public-api.ts').forEach((fileName) => {
    const name = fileName.replace('libs/', '').replace('/src/public-api.ts', '');
    entry['libs/' + name + '/index.js'] = fileName;
});

// mark all node modules as external
var nodeModules = {};

function addExternals(dir) {
    if (fs.existsSync(dir)) {
        fs.readdirSync(dir)
            .filter(function (x) {
                return ['.bin'].indexOf(x) === -1;
            })
            .forEach(function (mod) {
                // if (mod.indexOf('@sciflow') === 0) {
                //     return; // let @sciflow modules be compiled into the bundle
                // }
                nodeModules[mod] = 'commonjs ' + mod;
            });
    } else {
        console.log('Directory did not exist', dir);
    }
}

addExternals('node_modules');
addExternals('server/node_modules');

module.exports = {
    entry,
    target: 'node',
    mode: process.env.NODE_ENV === 'development' ? 'development' : 'production',
    optimization: {
        minimize: false,
        removeAvailableModules: false,
        removeEmptyChunks: false,
        splitChunks: false
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: "libs/*/package.json",
                    to: "[path]/[name][ext]",
                    toType: 'template'
                },
                {
                    from: "libs/*/package.json",
                    to: "[path]/[name][ext]",
                    toType: 'template'
                },
                { from: "tdk/package.json", to: "tdk" },
                { from: "tdk/src/styles", to: "tdk/styles" },
                {
                    from: "libs/export/src/html/**/*.schema.ts", to({ absoluteFilename }) {
                        // export the (renderers|components)/name/name.schema.ts part
                        return Promise.resolve('libs/export' + absoluteFilename.replace(path.join(absoluteFilename, '..', '..', '..'), ''));
                    }
                },
                { from: "libs/export/src/html/**/*.schema.ts", to: "server" },
                { from: "server/package.json", to: "server" },
                { from: "libs/cite/csl", to: "server/csl" },
                { from: "server/src/static", to: 'server/static' }
            ]
        })
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name]',
        libraryTarget: 'commonjs2'
    },
    externals: nodeModules,
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.xml$/i,
                use: [
                    {
                        loader: 'raw-loader',
                        options: {
                            esModule: false,
                        },
                    },
                ],
            },
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: process.env.TRANSPILE_ONLY ? true : false
                        }
                    }
                ],
                exclude: /node_modules/
            },
            {
                test: /\.(graphql|gql)$/,
                exclude: /node_modules/,
                loader: 'graphql-tag/loader',
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: 'raw-loader',
                        options: {
                            esModule: false,
                        },
                    },
                ]
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", ".d.ts", ".graphql"],
        plugins: [
            new TsconfigPathsPlugin({ configFile: "./tsconfig.json" })
        ]
    }
};
