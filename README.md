# SciFlow Org Base Repo

This repository contains SciFlow Open Source components und an MIT style license.

## Development Setup

You should have [node](https://nodejs.org/en/) >= 16 available (either through nvm or a global install).

[Visual Studio Code](https://code.visualstudio.com/) is recommended.

We recommend adding the [TypeScript + Webpack Problem Matchers for VS Code](https://marketplace.visualstudio.com/items?itemName=amodio.tsl-problem-matcher) so tasks can be monitored in the background.

To install everything, run:

```
yarn
```

Then, in VS Code go to the Run & Debug menu and start the server and app. The configuration is asuming Chrome as a default. This can be changed to Edge, Chromium and  others.