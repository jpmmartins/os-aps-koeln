#!/bin/sh

wget https://github.com/jgm/pandoc/releases/download/2.19.2/pandoc-2.19.2-1-amd64.deb
sudo dpkg -i pandoc-2.19.2-1-amd64.deb
sudo apt-get install -f
rm pandoc-2.19.2-1-amd64.deb
