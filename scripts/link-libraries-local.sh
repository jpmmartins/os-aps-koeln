#!/bin/bash

# to undo:
rm -rf ~/.config/yarn/link/@sciflow
# yarn link @sciflow/export @sciflow/schema @sciflow/import @sciflow/tdk @sciflow/support @sciflow/cite @sciflow/tdk

basedir=$(pwd)
echo $basedir

echo "Installing local copies of libraries in dist folder to node_modules"

libs=("cite" "export" "import" "schema" "support")
for lib in "${libs[@]}"; do
    cd $basedir/dist/libs/$lib
    pwd
    yarn link
    cd $basedir
    yarn link @sciflow/$lib
done

cd $basedir/dist/tdk
pwd
yarn link
cd $basedir
yarn link @sciflow/tdk
