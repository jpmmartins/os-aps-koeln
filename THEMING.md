# Theming 


## Why?

We use angular material although, we **do not** follow the recommended [theming guide](https://material.angular.io/guide/theming) to the letter. Due to:

- it duplicates the whole css for dark/light themes
- we want to be able to share the colors between framework and custom platform specific elements

Our approach uses modern css variables. They are changeable at runtime, and can be used within components without the overhead of importing extra styles.

## How?

There are a couple of hacks in place and it does not go without some downsides. But in the end the benefits outweigh the costs, and it does seem to be straightforward to understand.

##### Relevant files
- [_functions.scss](https://gitlab.com/sciflow/development/-/blob/main/libs/editing-ui/projects/reference-app/src/scss/_functions.scss)
- [_root_vars.scss](https://gitlab.com/sciflow/development/-/blob/main/libs/editing-ui/projects/reference-app/src/scss/_root_vars.scss)
- [_theming.scss](https://gitlab.com/sciflow/development/-/blob/main/libs/editing-ui/projects/reference-app/src/scss/_theming.scss)

**[_functions.scss](https://gitlab.com/sciflow/development/-/blob/main/libs/editing-ui/projects/reference-app/src/scss/_functions.scss)**<br>
This is where we hack our way trough angular material theming by overriding the [mat-color](https://gitlab.com/sciflow/development/-/blob/layout-prototype/libs/editing-ui/projects/reference-app/src/scss/_functions.scss#L2) function. Which in our case returns a rgb value, due the usage of different opacity levels when setting the pallets.
The function [hex-to-color](https://gitlab.com/sciflow/development/-/blob/layout-prototype/libs/editing-ui/projects/reference-app/src/scss/_functions.scss#L32) helps defining the values of R(ed) G(reen) and Blue for our root vars.


**[_root_vars.scss](https://gitlab.com/sciflow/development/-/blob/main/libs/editing-ui/projects/reference-app/src/scss/_root_vars.scss)**<br>
Here we define our root variables for both themes `:root` for the light theme, `prefers-color-scheme: dark` and `.dark-theme` for the dark. All variables that are common to both themes should be set within the `:root` ex: sizes, fonts...

Color related variables are declared with comma separated values for `R(ed), (Green), B(lue)` so we can use the these values with `rgba()` ex: `rgba( --some-root-var, 1)`<br/>
While setting up the theme colors we use the [hex-to-color](https://gitlab.com/sciflow/development/-/blob/layout-prototype/libs/editing-ui/projects/reference-app/src/scss/_functions.scss#L32) function so we can use the hex value for convenience. This makes the color preview easier to read within editors as well as extracting the colors from the current styleguide.


**[_theming.scss](https://gitlab.com/sciflow/development/-/blob/main/libs/editing-ui/projects/reference-app/src/scss/_theming.scss)**<br>
The theme is defined following [angular material guidelines](https://material.angular.io/guide/theming) given three color pallets (primary, accent, warn) and a pallet for both background and foreground colors. Although upon setting the pallet values we use the rgb value from the root variables and define it's opacity value for a given color weight

<details>
  <summary>Example $palette-sf-primarys</summary>
  ```
  $palette-sf-primary: (
    50 : rgba(var(--primary-color-50), 1),
    100 : rgba(var(--primary-color-100), 1),
    200 : rgba(var(--primary-color-200), 1),
    300 : rgba(var(--primary-color-300), 1),
    400 : rgba(var(--primary-color-400), 1),
    500 : rgba(var(--primary-color-500), 1),
    600 : rgba(var(--primary-color-600), 1),
    700 : rgba(var(--primary-color-700), 1),
    800 : rgba(var(--primary-color-800), 1),
    900 : rgba(var(--primary-color-900), 1),
    A100 : rgba(var(--primary-color-A100), 1),
    A200 : rgba(var(--primary-color-A200), 1),
    A400 : rgba(var(--primary-color-A400), 1),
    A700 : rgba(var(--primary-color-A700), 1),
    contrast: (
        50 : #000000,
        100 : #000000,
        200 : #000000,
        300 : #000000,
        400 : #ffffff,
        500 : #ffffff,
        600 : #ffffff,
        700 : #ffffff,
        800 : #ffffff,
        900 : #ffffff,
        A100 : #000000,
        A200 : #000000,
        A400 : #000000,
        A700 : #ffffff,
    )
);
```
</details>

## Function to apply own foreground and background in angular 12
 - set the background colors, customize the entire background and foreground palette by emulating the mat-light-theme or mat-dark-theme functions with your own replacement. 
 - theme-creating function that lets us apply our own foreground and background palettes.
  @function create-custom-theme($primary, $accent, $warn) {
    @return (
      primary: $primary,
      accent: $accent,
      warn: $warn,
      is-dark: false,
      foreground: $mat-light-theme-foreground,
      background: $mat-light-theme-background,
    );
  }

## Quirks
- We can not set `base` within the foreground pallet as it messes up a few components (ripples and sliders) 
- We can not set `elevation` within the foreground pallet as it messes up `box-shadow` proprieties

Extra fixes related to our theming changes **should be centralized in the end of this file** so they do not end up scattered all around the code.

## Usage

TODO: how we use it with samples, adding color utility classes...