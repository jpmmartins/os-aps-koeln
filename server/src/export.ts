import { listObjects, s3ToStream } from '@sciflow/support';
import express, { static as serveStatic } from 'express';
import { createReadStream, createWriteStream, existsSync, mkdirSync, readFile, readFileSync, writeFileSync } from 'fs';
import { JSDOM, VirtualConsole } from 'jsdom';
import mime from 'mime';

import JSZip from 'jszip';
import { extname, join } from 'path';
import Prince from 'prince';
import { dirSync } from 'tmp';
import winston from 'winston';
import { ExportService, injector } from './services';
const exportService: ExportService = injector.get(ExportService);

const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console() });

const fetchAsset = (req, res) => {
    const { templateId, filename, folder } = req.params;

    if (process.env.TEMPLATE_SOURCE) {
        const parts = [templateId, 'assets', folder, filename].filter(s => s != null);
        if (parts.includes('..')) { return res.status(404).send('File not found!'); }
        const filePath = join(process.env.TEMPLATE_SOURCE, ...parts);
        if (!existsSync(filePath)) {
            return res.status(404).send('File not found');
        }

        readFile(filePath, (err, data) => {
            const type = (mime as any).types[extname(filename).replace('.', '')];
            res.setHeader('Content-Type', type);
            return res.send(data);
        });
    }

    // TODO implement fetching from S3
}

/**
 * We provide a number of default exports that can be used without further installation
 * @param app the express app
 */
export const registerExports = (app) => {

    const fontPath = process.env.FONT_PATH || join(__dirname, 'fonts');
    const fontUrlPrefix = process.env.FONT_URL_PREFIX || '/export/fonts/';
    const distPath = join(__dirname, 'static', 'export');
    const nodeModulesPath = existsSync(join(__dirname, '..', 'node_modules')) ? join(__dirname, '..', 'node_modules') : join(__dirname, '..', '..', 'node_modules');

    /**
     * Returns an asset from storage (e.g. s3).
     */
    app.get('/export/asset/:projectId/:filename', async (req, res) => {
        try {
            const type = (mime as any).types[extname(req.params.filename).replace('.', '')];
            res.setHeader('Content-Type', type);
            return await s3ToStream({ Key: `${req.params.projectId}/${req.params.filename}`, VersionId: undefined }, res);
        } catch (e) {
            res.status(500).send('Could not stream file');
        }
    });

    app.use('/export/static', serveStatic(distPath));
    app.use(fontUrlPrefix, serveStatic(fontPath));
    app.get('/export/tpl/:templateId/assets/:filename', fetchAsset);
    app.get('/export/tpl/:templateId/assets/:folder/:filename', fetchAsset);

    const handleExport = async (req, res) => {
        const { filename, projectId, format = 'snapshot', templateId } = req.params;
        const fileKey = `${projectId}/${filename}`;

        try {
            const directory = await listObjects(projectId);
            const file = directory.find(({ Key }) => Key === fileKey);
            if (!file) {
                return res.status(404).json({ error: 'File not found ' });
            }

            const dirObj = dirSync({ unsafeCleanup: true });

            const path = file.Key;
            const sourceFile = join(dirObj.name, filename);

            const stream = createWriteStream(sourceFile);
            await s3ToStream({ Key: path, VersionId: undefined }, stream);
            await new Promise((resolve) => stream.on('finish', resolve));

            const manuscript = JSON.parse(readFileSync(sourceFile, 'utf-8'));

            let scripts: { src?: string; content?: string; }[] = [];

            const virtualConsole = new VirtualConsole();
            virtualConsole.sendTo(console, { omitJSDOMErrors: true });

            switch (format) {
                case 'snapshot':
                    const zip = await exportService.createSnapshotZIP({ projectId, manuscript });
                    res.setHeader('Content-Type', 'application/zip; charset=utf-8');
                    return res.send(zip);
                case 'pagedjs':
                    {
                        scripts.push({
                            src: '/export/static/pagedjs.polyfill.js'
                        });
                        const file = await exportService.renderHTML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, runner: 'pagedjs', inline: true });
                        const zip = new JSZip();
                        const content = await zip.loadAsync(file);
                        const body = await content.file('manuscript.html')?.async('string');

                        const dom = new JSDOM(body, { virtualConsole });
                        const document = dom.window.document;

                        // we append the paged js styles for the in-browser view
                        const pagedjsStyles = document.createElement('link');
                        pagedjsStyles.setAttribute('rel', 'stylesheet');
                        pagedjsStyles.setAttribute('href', '/export/static/pagedjs.interface.css');
                        document.head.appendChild(pagedjsStyles);

                        res.setHeader('Content-Type', 'text/html; charset=utf-8');
                        return res.send(dom.window.document.documentElement.outerHTML);
                    }
                case 'html':
                    {
                        const file = await exportService.renderHTML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, runner: 'raw', inline: true });
                        const zip = new JSZip();
                        const content = await zip.loadAsync(file);
                        const body = await content.file('manuscript.html')?.async('string');
                        const dom = new JSDOM(body, { virtualConsole });
                        const document = dom.window.document;

                        scripts.push({ src: '/export/mathjax/es5/tex-mml-chtml.js' });
                        const pagedjsStyles = document.createElement('link');
                        pagedjsStyles.setAttribute('rel', 'stylesheet');
                        pagedjsStyles.setAttribute('href', '/export/static/html-basic.css');
                        document.head.appendChild(pagedjsStyles);

                        res.setHeader('Content-Type', 'text/html; charset=utf-8');
                        return res.send(dom.window.document.documentElement.outerHTML);
                    }
                case 'xml':
                    {
                        const body = await exportService.renderXML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name });
                        res.setHeader('Content-Type', 'application/xml; charset=utf-8');
                        return res.send(body);
                    }
                case 'princexml':
                    {
                        const file = await exportService.renderHTML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, runner: 'princexml', inline: false });
                        const zip = new JSZip();
                        const content = await zip.loadAsync(file);

                        for (let path of Object.keys(content.files)) {
                            const file = await content.file(path)?.async('array');
                            if (file) {
                                console.log('Writing ' + join(dirObj.name, path))
                                writeFileSync(join(dirObj.name, path), Buffer.from(file));
                            } else {
                                mkdirSync(join(dirObj.name, path), { recursive: true });
                            }
                        }

                        const inFilename = join(dirObj.name, 'manuscript.html');
                        const outFilename = join(dirObj.name, 'manuscript.pdf');
                        try {
                            let prince = Prince()
                                .inputs(inFilename)
                                .output(outFilename);

                            if (existsSync(process.env.PRINCE_LICENSE_PATH as string)) {
                                prince = prince.license(process.env.PRINCE_LICENSE_PATH);
                            }

                            await prince.execute();
                        } catch (err) {
                            logger.error('Could not convert file to pdf', { projectId, message: err.message || err.error });
                            console.log(err?.stdout);
                            console.warn(err?.stderr);
                            return res.status(500).json({ message: err.message, stack: err.stack });
                        }
                        res.setHeader('Content-Type', 'application/pdf');
                        const streamFile = createReadStream(outFilename).pipe(res);
                        await new Promise((resolve) => streamFile.on('finish', resolve));
                        return res.end();
                    }
                default:
                    return res.status(400).send('Unknown format');
            }

        } catch (err) {
            logger.error('Could not convert file', { projectId, message: err.message });
            return res.status(500).render('error', { message: err.message, stack: err.stack });
        }
    };

    app.get('/export/snapshot/:projectId/:filename.zip', handleExport)
    app.get('/export/:format/:templateId/:projectId/:filename', handleExport);

    app.use('/export/csl', express.static(join(__dirname, './csl/')));
    app.use('/export/lens', express.static(join(distPath, 'lens')));
    app.use('/export/mathjax', express.static(join(nodeModulesPath, 'mathjax')));
    console.log(join(nodeModulesPath, 'mathjax'))
}
