import { loadTemplate } from '@sciflow/tdk';
import { ExportService, injector } from '../services';

import template from './Template.graphql';
const exportService: ExportService = injector.get(ExportService);

export const typeDefs = [template];
export const resolvers = {
    Query: {
        templates: async () => {
            const templates = await exportService.getTemplates();
            return templates.map(({ slug }) => slug);
        },
        template: async (_parent, { projectId, templateId }) => {
            const template = await loadTemplate({ projectId, templateId });

            return {
                slug: templateId,
                metaData: template?.metaData,
                ...(template?.configuration || {}),
                projectId,
                runners: exportService.getRunners()
            };
        }
    }
}