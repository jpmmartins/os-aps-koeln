import GraphQLDateTime from 'graphql-type-datetime';
import { streamToS3, listObjects } from '@sciflow/support';
import { createId } from '@sciflow/schema';
import { GraphQLJSON, GraphQLJSONObject } from 'graphql-type-json';
import winston from 'winston';
import { extname } from 'path';

// @ts-ignore
import query from './Query.graphql';
import * as referenceDef from './reference';
import * as templateDef from './template';
// @ts-ignore
import relay from './Relay.graphql';
import { toGlobalId } from 'graphql-relay';
import { DocumentService, injector } from '../services';
import GraphQLUpload from 'graphql-upload/GraphQLUpload.js';

const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console() });

const documentService: DocumentService = injector.get(DocumentService);

const resolvers = [
    {
        Query: {
            project: async (_parent, { projectId }) => {
                const resources = await listObjects(projectId);
                const size = resources.reduce((s, resource) => resource.Size + s, 0);
                return { id: toGlobalId('project', projectId), projectId, resources, size };
            },
            document: async (_parent, { key, version }) => {
                return { id: toGlobalId('document', key + version), key, version };
            },
        },
        Subscription: {
        },
        Mutation: {
            /* deleteFile: async (_parent, { input }) => {
                // TODO
            }, */
            uploadFile: async (_parent, { input }) => {
                const projectId = input.projectId || `${createId()}-${createId()}-${createId()}`;
                let { filename, mimetype, createReadStream } = await input.file;
                const ext = extname(filename);
                filename = encodeURIComponent(filename.replaceAll(ext, '').replace(/[^A-Za-z0-9\-_]/g, '-')) + ext;
                logger.info('Processing upload', {
                    filename,
                    mimetype
                });
                const stream = createReadStream();
                const result = await streamToS3(projectId + '/' + filename, stream);
                logger.info('Receiving file', {
                    filename,
                    mimetype
                });

                return {
                    projectId,
                    etag: result?.ETag,
                    key: result?.Key,
                    version: result?.VersionId ? {
                        ETag: result?.ETag,
                        VersionId: (result as any).VersionId
                    } : null
                };
            }
        },
        Document: {
            manuscript: async (parent, args) => {
                const options = {
                    shiftHeadingsBy: args.shiftHeadingsBy,
                    processCitations: args.processCitations
                };
                return documentService.getManuscript(parent.key, parent.version);
            },
            preview: async (parent, args) => {
                const options = {
                    shiftHeadingsBy: args.shiftHeadingsBy,
                    processCitations: args.processCitations
                };
                return documentService.getManuscript(parent.key, parent.version, 'preview', options);
            }
        },
        Upload: GraphQLUpload,
        DateTime: GraphQLDateTime,
        JSON: GraphQLJSON,
        JSONObject: GraphQLJSONObject
    },
    referenceDef.resolvers,
    templateDef.resolvers
];

const typeDefs = [query, relay, ...referenceDef.typeDefs, ...templateDef.typeDefs];

export {
    typeDefs,
    resolvers
}
