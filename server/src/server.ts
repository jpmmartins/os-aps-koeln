import 'reflect-metadata';
import { json, urlencoded } from 'body-parser';
import express from 'express';

import { ApolloServer } from 'apollo-server-express';
import graphqlUploadExpress from 'graphql-upload/graphqlUploadExpress.js';

import { createServer } from 'http';
import { ApolloServerPluginDrainHttpServer, ApolloServerPluginLandingPageGraphQLPlayground } from "apollo-server-core";
import helmet, { contentSecurityPolicy } from 'helmet';

import winston from 'winston';
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console() });

import { registerExports } from './export';
import { ensureBucket } from '@sciflow/support';

import { typeDefs, resolvers } from './schema/index';

import { join } from 'path';
import { registerClient } from './client';
import { registerImport } from './import';
import e from 'express';

(async () => {
  const { PORT = 3000 } = process.env;

  const path = '/api/graphql';

  const httpServer = createServer((req, res) => app(req, res));
  const app = express();

  // security middleware
  app.use(
    helmet(),
    contentSecurityPolicy({
      useDefaults: true,
      directives: {
        // cdn.jsdelivr.net is needed for the GraphQL playground
        "script-src": ["'self'", "'unsafe-eval'", "'unsafe-inline'", "cdn.jsdelivr.net"],
        "style-src": ["'self'", "https://fonts.googleapis.com", "'unsafe-inline'", "cdn.jsdelivr.net"],
        "connect-src": ["'self'"],
        "img-src": ["'self'", "data:", "'unsafe-inline'", "cdn.jsdelivr.net"]
      }
    }));

  app.use(json({ limit: '150mb' }));
  app.use(urlencoded({ limit: '150mb', extended: true }));

  registerExports(app);
  registerImport(app);

  app.disable('x-powered-by');
  app.set('view engine', 'pug');
  app.set('views', join(process.cwd(), 'src/views'));

  const server = new ApolloServer({
    csrfPrevention: true,
    typeDefs,
    resolvers,
    context: ({ req }) => ({
      // add a viewer object for authentication if needed
    }),
    plugins: [
      ApolloServerPluginLandingPageGraphQLPlayground({
        cdnUrl: 'https://cdn.jsdelivr.net/npm',
        faviconUrl: ''
      }),
      // Proper shutdown for the HTTP server.
      ApolloServerPluginDrainHttpServer({ httpServer }),

      // Proper shutdown for the WebSocket server.
      {
        async serverWillStart() {
          return {
            async drainServer() {
              // cleanup where needed
            },
          };
        },
      },
    ]
  });

  await server.start();

  app.use(graphqlUploadExpress({ maxFileSize: 50000000, maxFiles: 10 })); // 50mb
  server.applyMiddleware({
    bodyParserConfig: { limit: '150mb' },
    cors: {
      credentials: true,
      origin: function (origin, callback) {
        callback(null, true);
        // FIXME
        /* if (!origin || allowedOrigins.some(allowed => origin.indexOf(allowed) === 0)) {
          callback(null, true);
        } else {
          callback(new Error('Not allowed by CORS'));
        } */
      }
    }, app, path,
  });

  // we run this last since it contains a catchall
  registerClient(app);

  httpServer.listen({ port: PORT }, () => {
    logger.info(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`);
    ensureBucket().catch(console.error);
  });

  process.on('unhandledRejection', (reason) => {
    logger.error('unhandled rejection', reason);
  });

  const shutdown = async () => {
    logger.info('Gracefully shutting down server');
    server.stop();
    httpServer.close();

    // start any cleanup jobs that are needed

    await new Promise((r) => setTimeout(r, 1000));
    logger.info('Shutting down http server ..');
    httpServer.close();
    await new Promise((r) => setTimeout(r, 1000));
    process.exit(0);
  }

  process.on('uncaughtException', async (err: any) => {
    logger.error('Caught exception', { message: err.message });
    await shutdown();
  });

  process.on('SIGTERM', async () => {
    logger.info('Received SIGTERM');
    await shutdown();
  });
})().catch((e) => {
  console.error('Error caught', e.message);
});
