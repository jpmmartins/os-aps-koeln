import { ImportError } from '@sciflow/import';
import { spawnSync } from 'child_process';
import * as mime from 'mime';
import { extname } from 'path';
import winston from 'winston';
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console() });

import { DocumentService, injector } from './services';

export const registerImport = (app) => {
    const documentService: DocumentService = injector.get(DocumentService);

    try {
        const { signal, status, stderr, stdout } = spawnSync(
            'pandoc',
            ['-v']
        );
        const errors = stderr.toString().split('\n').filter(s => s.length > 0);
        const logs = stdout.toString().split('\n').filter(s => s.length > 0);
        logger.info('Checking pandoc', { signal, status, errors, logs });
    } catch (e) {
        logger.error('Could not initialize pandoc', { message: e.message });
    }

    /**
     * Extracts an asset (e.g. media file) from a source document to display before the import
     */
    app.use('/import/asset/:projectId/:filename/media/:assetName', async (req, res) => {
        const assetParams = { projectId: req.params.projectId, filename: req.params.filename, assetName: req.params.assetName, version: undefined };
        try {
            // assets are currently not versioned
            const stream = await documentService.streamAsset(assetParams);
            const type = (mime as any).types[extname(req.params.assetName).replace('.', '')];
            res.setHeader('Content-Type', type);
            stream.pipe(res);
            await new Promise((resolve) => stream.on('finish', resolve));
            return res.end();
        } catch (e) {
            logger.error('Could not stream asset', { message: e.message, assetParams, payload: e.payload });
            if (e instanceof ImportError) {
                return res.status(e.payload.status || 500).send(e.payload.reason || 'Could not stream asset');
            }
            res.status(500).send('Could not stream asset');
        }
    });

};
