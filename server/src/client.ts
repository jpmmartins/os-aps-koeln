import { existsSync, readFileSync } from 'fs';
import { join } from 'path';
import { static as serveStatic } from 'express';
import { JSDOM, VirtualConsole } from 'jsdom';

export const registerClient = (app) => {

    const instanceData = {
        namespace: process.env.NAMESPACE,
        version: process.env.CI_COMMIT_SHA,
        instanceTitle: process.env.INSTANCE_TITLE || 'Importer'
    };

    const distPath = join(__dirname, '..', 'public');
    if (existsSync(distPath)) {
        const indexFile = readFileSync(join(distPath, 'index.html'), 'utf-8');

        const virtualConsole = new VirtualConsole();
        virtualConsole.sendTo(console, { omitJSDOMErrors: true });
        const dom = new JSDOM(indexFile, { virtualConsole });
        const document = dom.window.document;

        document.getElementById('instance-data')?.remove();
        const instanceDataEl = document.createElement('script');
        instanceDataEl.setAttribute('type', 'application/json');
        instanceDataEl.setAttribute('id', 'instance-data');
        instanceDataEl.innerHTML = JSON.stringify(instanceData);
        document.title = instanceData.instanceTitle;
        document.head.appendChild(instanceDataEl);

        app.get('/', (req, res) => res.redirect('/write'));
        app.use('/', serveStatic(distPath));
        app.get('/*', (_req, res) => res
            .setHeader('Content-Type', 'text/html')
            .send(dom.window.document.documentElement.outerHTML));
    } else {
        app.get('/', (_req, res) => res.status(404).send('No public files'));
    }
}
