import 'reflect-metadata';
import { ReflectiveInjector } from 'injection-js';

import { ReferenceService } from './references';
import { ExportService } from './export';
import { DocumentService } from './document';

const injector = ReflectiveInjector.resolveAndCreate([DocumentService, ExportService, ReferenceService]);

export {
    ReferenceService,
    DocumentService,
    ExportService,
    injector
};