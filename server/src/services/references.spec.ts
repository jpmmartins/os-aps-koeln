import { it, describe } from 'mocha';
import { expect } from 'chai';

import { ReferenceService } from './references';

const refs =
    [{ "DOI": "10.1371/journal.pone.0251513", "URL": "https://doi.org/10.1371/journal.pone.0251513", "abstract": "Introduction In the marathon, how runners pace and fuel their race can have a major impact on race outcome. The phenomenon known as hitting the wall (HTW) refers to the iconic hazard of the marathon distance, in which runners experience a significant slowing of pace late in the race, typically after the 20-mile mark, and usually because of a depletion of the body’s energy stores. Aim This work investigates the occurrence of significant late-race slowing among recreational marathoners, as a proxy for runners hitting the wall, to better understand the likelihood and nature of such slowdowns, and their effect on race performance. Methods Using pacing data from more than 4 million race records, we develop a pacing-based definition of hitting the wall, by identifying runners who experience a sustained period of slowing during the latter stages of the marathon. We calculate the cost of these slowdowns relative to estimates of the recent personal-best times of runners and compare slowdowns according to runner sex, age, and ability. Results We find male runners more likely to slow significantly (hit the wall) than female runners; 28", "author": [{ "family": "Smyth", "given": "Barry" }], "container-title": "PLOS ONE", "id": "10.1371/journal.pone.0251513", "issue": "5", "issued": { "date-parts": [[2021, 5]] }, "page": "1-25", "publisher": "Public Library of Science", "title": "How recreational marathon runners hit the wall: A large-scale data analysis of late-race pacing collapse in the marathon", "title-short": "How recreational marathon runners hit the wall", "type": "article-journal", "volume": "16" }];

describe('Reference service', async () => {
    const referenceService = new ReferenceService();
    const ref: any = refs.find(r => r.DOI === '10.1371/journal.pone.0251513');

    it('Translates item types and fields', async () => {
        expect(referenceService.getItemTypeTranslation('article-journal'))
            .to.equal('Journal Article');
        expect(referenceService.getItemTypeTranslation('article'))
            .to.equal('Document');
        expect(referenceService.getFieldTranslation('container-title')).to.equal('Publication');
        expect(referenceService.getFieldTranslation('author')).to.equal('Author');
    });

    it('Retrieves item types', async () => {
        const type = referenceService.getType('article-journal');
        for (let field of Object.keys(ref)) {
            const translation = referenceService.getFieldTranslation(field);
            expect(translation).not.to.be.null;
        }

        const missingFields = type?.fields.filter(field => {
            return !Object.keys(ref).map(cslFieldName => referenceService.getZoteroFieldName(cslFieldName)).includes(field.field)
        });
        expect(missingFields?.find(({ field }) => field === 'ISSN')).not.to.be.null;
    });
});
