import { generateListings } from '@sciflow/export';
import { ImportError, parsePandocAST, proseMirrorToHTML } from '@sciflow/import';
import { parse, SimplifiedManuscriptFile } from '@sciflow/schema';
import { listObjects, s3ToStream } from '@sciflow/support';
import { ApolloError } from 'apollo-server-express';
import { spawnSync } from 'child_process';
import fs, { createReadStream, existsSync, readdirSync, readFileSync, ReadStream } from 'fs';
import { Injectable } from 'injection-js';
import { PandocNode } from 'libs/import/src/pandoc-renderers';
import { extname, join } from 'path';
import 'reflect-metadata';
import sharp from 'sharp';
import { dirSync } from 'tmp';
import winston from 'winston';

const { OPENFAAS_URL, NAMESPACE } = process.env;
const functionNamespace = NAMESPACE && NAMESPACE !== 'default' ? '.' + NAMESPACE : '';
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console() });

@Injectable()
export class DocumentService {
    async getManuscript(key, version, mode?: 'preview' | undefined, options?: { processCitations: boolean, shiftHeadingsBy: number }) {
        try {
            logger.info('Requesting file (json)', { version, key });
            const file = await this.processFile({ key, version, mode, processCitations: options?.processCitations, shiftHeadingsBy: options?.shiftHeadingsBy });

            if (file.errors?.length > 0) {
                logger.error('Found errors', file.errors);
                throw new ApolloError('Could not convert document', 'Pandoc Conversion Error', {
                    errors: file.errors.map(error => {
                        if (error instanceof ImportError) {
                            if (error.payload?.pandocNode) {
                                try {
                                    const renderedNode = this.renderPandocNode(error.payload?.pandocNode);
                                    const prefix = `/import/asset/${key}/media`;
                                    error.payload.pandocNodeHTML = renderedNode?.replace(/<img([^>]*)\ssrc=(['"])(?:[^\2\/]*\/)*([^\2]+)\2/gi, '<img$1 src=$2' + prefix + '/$3$2');
                                    console.log(error.payload.pandocNodeHTML);
                                } catch (e) {
                                    console.error('Could not render preview', e, error.payload?.pandocNode);
                                }
                            }
                        }

                        return error;
                    })
                });
            }

            if (!file.document && !file.html) {
                logger.error('Did not find document in ' + Object.keys(file).join(', '));
                throw new ApolloError('File must contain document');
            }

            return file;

        } catch (e: any) {
            logger.error('Could not retrieve document', { type: e.type, message: e.message });
            if (e instanceof ApolloError) {
                throw e;
            }
            throw new Error(e.type ? `Could not retrieve document (${e.type})` : `Could not retrieve document`);
        }
    }

    /** Extracts an asset from a file (like docx) */
    public async streamAsset({ projectId, filename, version, assetName }): Promise<ReadStream> {
        logger.info('Processing asset', { projectId, filename, version, assetName });
        const dirObj = dirSync({ unsafeCleanup: true });
        const projectDir = join(dirObj.name, projectId);

        try {
            // extract the assets
            const sourceFile = join(projectDir, filename);
            fs.mkdirSync(projectDir, { recursive: true });
            const stream = fs.createWriteStream(sourceFile);
            await s3ToStream({
                Key: `${projectId}/${filename}`
            }, stream);
            await new Promise((resolve) => stream.on('finish', resolve));
            let commands = [
                '-s', sourceFile,
                '-o', join(dirObj.name, 'data.json'),
                '--extract-media', projectDir,
                '-t', 'json'
            ];

            const { signal, status, stderr, stdout } = spawnSync(
                'pandoc',
                commands,
                { cwd: projectDir }
            );

            if (signal === 'SIGKILL') {
                throw new ImportError('Pandoc received SIGKILL', { reason: 'The document could not be opened', status: 500 });
            }

            if (!existsSync(join(projectDir, 'media'))) {
                throw new ImportError('No embedded assets found in ' + filename, { reason: 'Asset file not found', status: 404 });
            }
            const files = readdirSync(join(projectDir, 'media'));
            if (files.indexOf(assetName) === -1) {
                throw new ImportError('File not found ' + assetName, { reason: 'Asset file not found', status: 404 });
            }

            return createReadStream(join(projectDir, 'media', assetName));
        } catch (e) {
            logger.error('Could not stream file', { projectId, filename, version, assetName, message: e.message });
            throw new ImportError('Asset streaming error', { reason: 'An error occured reading the asset', status: 500 });
        }
    }

    private renderPandocNode(node: PandocNode | string) {
        if (typeof node === 'string') { node = JSON.parse(node); }
        const input = {
            "pandoc-api-version": [1, 22, 2, 1],
            "meta": {},
            "blocks": [{
                "t": "Plain",
                "c": [
                    node
                ]
            }]
        };
        const { signal, status, stderr, stdout } = spawnSync('pandoc', ['--from', 'json', '--to', 'html'], { input: JSON.stringify(input) });
        if (signal === 'SIGKILL') {
            throw new Error('Pandoc quit unexpectedly');
        }

        const errors = stderr.toString().split('\n').filter(s => s.length > 0);
        if (errors?.length > 0) {
            console.error('Could not render node preview', errors);
        }
        const logs = stdout.toString().split('\n').filter(s => s.length > 0);

        return logs.join(' ');
    }

    private async processFile({ key, version, mode, processCitations = true, shiftHeadingsBy = 0 }): Promise<any> {

        logger.info('Processing file', { key, version });

        const projectId = key.split('/')[0];
        const fileName = key.replace(projectId + '/', '');
        const extension = extname(fileName).replace('.', '');
        const dirObj = dirSync({ unsafeCleanup: true });

        try {
            const directory = await listObjects(projectId);
            const file = directory.find(({ Key }) => Key === key);
            if (!file) {
                throw new ApolloError('File not found', '404');
            }

            const start = Date.now();

            const sourceFile = join(dirObj.name, fileName);
            const stream = fs.createWriteStream(sourceFile);
            const reqObj: any = { Key: key };
            if (version?.length > 0) {
                reqObj.VersionId = version;
            }
            try {
                await s3ToStream(reqObj, stream);
            } catch (err) {
                logger.error('Could not stream file', { projectId, message: err.message, err, reqObj });
                throw new ApolloError('Could not convert file', '500');
            }
            await new Promise((resolve) => stream.on('finish', resolve));

            const endStream = Date.now();

            logger.info('Converting file', { projectId, fileName, key, dir: readdirSync(dirObj.name) });

            if (!existsSync(sourceFile)) {
                logger.error('Source file not found', { projectId, sourceFile });
                throw new ApolloError('Source file not found', '404');
            }

            let commands = [
                '-s', sourceFile,
                '-o', join(dirObj.name, 'data.json'),
                '-t', 'json'
            ];

            const mediaDir = join(dirObj.name);

            switch (extension) {
                case 'json': {
                    const file: SimplifiedManuscriptFile = JSON.parse(fs.readFileSync(sourceFile, 'utf-8'));
                    try {
                        const pmDoc = parse(file.document);
                        pmDoc.check();
                    } catch (e) {
                        throw new ApolloError('Could not parse document: ' + e.message, '500');
                    }

                    dirObj.removeCallback();
                    return file;
                }
                case 'bib':
                    {
                        const { signal, status, stderr, stdout } = spawnSync(
                            'pandoc',
                            ['-s', sourceFile, '-o', join(dirObj.name, 'data.json'), '-f', 'biblatex', '-t', 'csljson'],
                            { cwd: dirObj.name }
                        );

                        const errors = stderr.toString().split('\n').filter(s => s.length > 0);
                        if (fs.existsSync(join(dirObj.name, 'data.json'))) {
                            const references = JSON.parse(fs.readFileSync(join(dirObj.name, 'data.json'), 'utf-8'));
                            dirObj.removeCallback();
                            return {
                                references,
                                errors
                            };
                        } else {
                            dirObj.removeCallback();
                            throw new ApolloError('Could not process bib file', '500');
                        }
                    }
                case 'docx':
                // fall through
                case 'odt':
                    commands.push(...[
                        '-f', `${extension}+styles${processCitations ? '+citations' : ''}`,
                        `--shift-heading-level-by=${shiftHeadingsBy}`,
                        '--extract-media', mediaDir
                    ]);
                default:
                    {
                        const { signal, status, stderr, stdout } = spawnSync(
                            'pandoc',
                            commands,
                            { cwd: dirObj.name }
                        );

                        if (signal === 'SIGKILL') {
                            throw new Error('Pandoc quit unexpectedly');
                        }

                        const output = existsSync(join(dirObj.name, 'data.json'));
                        let media = existsSync(join(mediaDir, 'media')) ? readdirSync(join(mediaDir, 'media')) : [];

                        /** Pandoc errors are thrown by Pandoc during conversion of the input format
                         *  (while render errors happen during interpreting the Pandoc AST) */
                        const pandocErrors = stderr.toString().split('\n').filter(s => s.length > 0);
                        const logs = stdout.toString().split('\n').filter(s => s.length > 0);
                        const dataFile = join(dirObj.name, 'data.json');
                        if (!existsSync(dataFile)) {
                            return {
                                errors: [new ImportError('Pandoc conversion error', { message: pandocErrors.join(' \n') })]
                            };
                        }

                        logger.info('Executed pandoc', { output, signal, status, errors: pandocErrors, logs });
                        const pandocAST = JSON.parse(fs.readFileSync(dataFile, 'utf-8'));

                        // handle conversion of files in media dir
                        for (let i = 0; i < media.length; i++) {
                            const imagePath = join(mediaDir, 'media', media[i]);
                            const extension = extname(imagePath);
                            switch (extension) {
                                case '.emf':
                                    {
                                        //use libemf2svg to create an svg
                                        const { stdout } = spawnSync(
                                            'sh', ['-c', `whereis emf2svg-conv`]
                                        );
                                        if (!stdout.includes('/emf2svg-conv')) {
                                            console.error('emf2svg-conv must be installed (package libemf2svg)');
                                        }
                                    }

                                    const svgPath = imagePath?.replace('.emf', '.svg');
                                    const { stderr } = spawnSync(
                                        'sh', ['-c', `emf2svg-conv -i ${imagePath} -o ${svgPath}`]
                                    );
                                    if (stderr.toString().length > 0) {
                                        console.error('Could not convert emf to svg', { stderr: stderr.toString() });
                                    } else {
                                        media[i] = media[i].replace('.emf', '.svg');
                                    }
                                    break;
                                case '.wmf':
                                // console.warn('wmf is currently not supported', imagePath);
                            }
                        }

                        const { doc, references, schema, errors: renderErrors } = await parsePandocAST(pandocAST, mediaDir);
                        const endConversion = Date.now();

                        if (renderErrors.length > 0 || pandocErrors.length > 0) {
                            logger.info('Failed to transform file', { transformErrors: pandocErrors, renderErrors });
                            return { errors: [...pandocErrors, ...renderErrors] };
                        }

                        logger.info('Converted file', { totalTimeMs: endConversion - start, timeReadingFilesMs: endStream - start });

                        const listings = await generateListings(doc?.toJSON());

                        const mediaStats = async (path) => {
                            try {
                                let image = await sharp(readFileSync(path)).withMetadata();
                                const metaData = await image.metadata();
                                return {
                                    width: metaData.width,
                                    height: metaData.height,
                                    format: metaData.format,
                                    dpi: metaData.density,
                                    size: metaData.size
                                };
                            } catch (e) {
                                console.error(e);
                                return null;
                            }
                        }

                        if (mode === 'preview') {
                            const html = await proseMirrorToHTML(doc, schema);
                            return {
                                html,
                                references,
                                listings,
                                assets: Promise.all(media.map(async (file) => ({
                                    key: join('media', file),
                                    stats: await mediaStats(join(mediaDir, 'media', file))
                                }))),
                                errors: [...pandocErrors, ...renderErrors]
                            };
                        } else {
                            return {
                                document: doc?.toJSON(),
                                authors: [],
                                listings: [],
                                lastModified: null,
                                references,
                                errors: [...pandocErrors, ...renderErrors]
                            };
                        }
                    }
            }
        } catch (err) {
            logger.error('Could not convert file', { projectId, message: err.message, err });
            if (err.code === 'ENOENT') {
                throw new ApolloError('File not found', '404');
            }
            throw new ApolloError('Could not convert file', '500');
        } finally {
            dirObj.removeCallback();
        }
    }
}