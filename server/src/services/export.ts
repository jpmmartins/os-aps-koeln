import { convertToSnapshot, extractTitle, SingleDocumentExportData } from '@sciflow/export';
import { SimplifiedManuscriptFile } from '@sciflow/schema';
import { loadTemplate, defaultHTMLTemplate, defaultXMLTemplate } from '@sciflow/tdk';
import { lstatSync, readdirSync, symlinkSync } from 'fs';
import { Injectable } from 'injection-js';
import { join } from 'path';
import 'reflect-metadata';
import { getDefaults } from '@sciflow/cite';
import JSZip from 'jszip';
import { s3ToBuffer } from '@sciflow/support';

const { styleXML, localeXML } = getDefaults();

@Injectable()
export class ExportService {
    getRunners(): string[] {
        const runners = ['pagedjs', 'html', 'xml'];
        // we only use prince if there is a license present
        // TODO activate this again when pagedjs is in a better state
        //if (existsSync(process.env.PRINCE_LICENSE_PATH as string)) {
        runners.push('princexml');
        //}
        return runners;
    }

    async getTemplates() {
        let templates: any[] = [];
        if (process.env.TEMPLATE_SOURCE) {
            const entries = readdirSync(process.env.TEMPLATE_SOURCE);
            for (let entry of entries.filter(slug => slug.indexOf('_') !== 0)) {
                const path = join(process.env.TEMPLATE_SOURCE, entry);
                const stat = lstatSync(path);
                if (stat.isDirectory()) {
                    templates.push({
                        slug: entry
                    });
                }
            }
            return templates;
        }

        return templates;
    }

    async createSnapshotZIP(data: {
        projectId: string;
        manuscript: SimplifiedManuscriptFile;
    }): Promise<Buffer> {
        const snapshot = convertToSnapshot({
            document: data.manuscript.document,
            title: extractTitle(data.manuscript.document),
            files: data.manuscript.files || [],
            templateOptions: null,
            citationStyleData: { styleXML, localeXML },
            authors: data.manuscript.authors ?? [],
            references: data.manuscript.references ?? [],
            metaData: data.manuscript.metaData ?? {}
        }, {
            assetUrl: 'assets'
        });

        snapshot.files = snapshot.files.map(file => ({
            ...file,
            url: `assets/${file.name}`
        }));

        const zip = new JSZip();
        zip.file('index.json', JSON.stringify(snapshot));
        if (snapshot.files?.length > 0) {
            const contentDir = zip.folder('assets');
            for (let asset of snapshot.files) {
                if (typeof asset.name === 'string') {
                    const buffer = await s3ToBuffer({ Key: `${data.projectId}/${asset.name}`, VersionId: undefined });
                    contentDir?.file(asset?.name, buffer);
                }
            }
        }

        return await zip.generateAsync({
            type: 'nodebuffer',
            mimeType: 'application/zip'
        });
    }

    async renderHTML(data: {
        projectId: string;
        templateId: string;
        runner?: string;
        manuscript: SimplifiedManuscriptFile;
        scripts: {
            src?: string;
            content?: string;
        }[];
        tmpPath: string;
        inline?: boolean;
    }): Promise<Buffer> {

        if (process.env.TEMPLATE_SOURCE) {
            symlinkSync(process.env.TEMPLATE_SOURCE, join(data.tmpPath, 'templates'));
        }

        const template = await loadTemplate({
            projectId: data.projectId,
            templateId: data.templateId
        }, {
            componentPath: undefined,
            fontPath: process.env.FONT_PATH,
            fontUrlPrefix: process.env.FONT_PATH,
            templateDir: process.env.TEMPLATE_SOURCE
        });

        const snapshot = convertToSnapshot({
            document: data.manuscript.document,
            title: extractTitle(data.manuscript.document),
            stylePaths: (template?.stylePaths || []).map(style => ({ inline: true, ...style })),
            files: data.manuscript.files || [],
            templateOptions: null,
            citationStyleData: { styleXML, localeXML },
            authors: data.manuscript.authors ?? [],
            configuration: (template?.configuration || []),
            configurations: (template?.configurations ?? []).map(c => JSON.parse(JSON.stringify(c))),
            references: data.manuscript.references ?? [],
            metaData: data.manuscript.metaData ?? {},
            metaDataSchema: template?.metaData
        }, {
            assetUrl: process.env.INSTANCE_URL || 'http://localhost:3000',
        });

        try {
            if (typeof template?.render === 'function') {
                return await template?.render(snapshot);
            }

            return await defaultHTMLTemplate(snapshot, template, { inline: data.inline === true, scripts: data.scripts || [] });
        } catch (e) {
            console.error('Could not render', e);
            throw new Error('Could not render template');
        }
    }

    async renderXML(data: { projectId: string, templateId: string, manuscript: SimplifiedManuscriptFile, scripts: { src?: string; content?: string; }[], tmpPath: string }): Promise<string> {

        if (process.env.TEMPLATE_SOURCE) {
            symlinkSync(process.env.TEMPLATE_SOURCE, join(data.tmpPath, 'templates'));
        }

        const template = await loadTemplate({ projectId: data.projectId, templateId: data.templateId });
        const input: SingleDocumentExportData = {
            document: data.manuscript.document,
            title: extractTitle(data.manuscript.document),
            stylePaths: template?.stylePaths || [],
            files: data.manuscript.files || [],
            templateOptions: null,
            citationStyleData: { styleXML, localeXML },
            authors: data.manuscript.authors ?? [],
            configuration: (template?.configuration || []),
            configurations: (template?.configurations ?? []).map(c => JSON.parse(JSON.stringify(c))),
            references: data.manuscript.references ?? [],
            metaData: data.manuscript.metaData ?? {},
            metaDataSchema: template?.metaData
        };

        try {
            if (typeof template?.render === 'function') {
                return await template?.render(input);
            }

            const content = await defaultXMLTemplate(input);
            return content.end({ prettyPrint: true });
        } catch (e) {
            console.error('Could not render', e);
            throw new Error('Could not render template');
        }
    }
}