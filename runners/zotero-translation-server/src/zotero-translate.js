// make sure all needed properties have been added
require('../translation-server/src/zotero');
require('../translation-server/src/debug');
require('../translation-server/modules/utilities/utilities');
require('../translation-server/src/translation/translate');
const { FORMATS } = require('../translation-server/src/formats');

const Translators = require('../translation-server/src/translators');

const toCSL = (item) => {
    var translate = new Zotero.Translate.Export();
    translate.setTranslator(FORMATS.csljson);
	translate.setItems([item]);
    
    return new Promise((resolve, reject) => {
        translate.translate();
        translate.setHandler("done", function (obj, status) {
            if (!status) { reject(); }
            resolve(JSON.parse(obj.string)[0]);
        });
    });
};

module.exports = {
    /** Parses a reference file and returns the Zotero item, the CSL representation and the itemID */
    parseReferenceFile: async (content) => {
        await Translators.init();

        const translate = new Zotero.Translate.Import();
        translate.setString(content);
        const translators = await translate.getTranslators();
        if (translators.length === 0) {
            throw new Error('No translators found');
        }
        translate.setTranslator(translators[0]);
    
        let items = await translate.translate({ libraryID: 1 });
        items = await Promise.all(items.map(async (original) => ({
            item: Zotero.Utilities.Item.itemToAPIJSON(original)[0],
            id: original.itemID,
            csl: {
                id: original.itemID,
                ...await toCSL(original)
            }
        })));
        
        return items;
    }
};
