FROM --platform=${TARGETPLATFORM:-linux/amd64} ghcr.io/openfaas/of-watchdog:0.9.5 as watchdog
FROM node:16-alpine3.16

RUN set -x \
    && apk update \
    && apk upgrade \
    && apk add --no-cache \
    git \
    && addgroup -S app && adduser -S -g app app

COPY --from=watchdog /fwatchdog /usr/bin/fwatchdog
RUN chmod +x /usr/bin/fwatchdog

RUN mkdir -p /home/app
WORKDIR /home/app

COPY . .
RUN git clone --recurse-submodules https://github.com/zotero/translation-server.git translation-server
RUN yarn

ENV cgi_headers="true"
ENV fprocess="node src/server.js"
ENV mode="http"
ENV upstream_url="http://127.0.0.1:3000"

ENV write_timeout="60s"
ENV read_timeout="60s"
ENV exec_timeout="60s"

ENV prefix_logs="false"

HEALTHCHECK --interval=5s CMD [ -e /tmp/.lock ] || exit 1

CMD ["fwatchdog"]
