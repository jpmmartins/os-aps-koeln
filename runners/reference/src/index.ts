import express from 'express';
import { spawn, spawnSync, exec } from 'child_process';
import { text } from 'body-parser';
import winston from 'winston';

const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console() });

(async () => {
    const app = express();
    app.use(text());

    app.post('/', (req, res) => {

        let inputs = req.body;
        inputs = inputs.split("\n");
        let results: any[] = [];
        let errors: string[] = [];

        // PROOF OF CONCEPT
        // This needs work. Does this allow injection of some kind?
        for (let input of inputs) {
            try {
                const { stderr, stdout } = spawnSync(
                    'sh', ['-c', `echo "${input}" | ./anystyle.rb`]
                );

                const err = stderr.toString();
                if (err.length > 0)  {
                    errors.push(err);
                }
                const out = stdout.toString();
                const x = JSON.parse(out);
                results.push(...JSON.parse(x));
            } catch (e) {
                console.error(e.message);
            }
        }

        res.json({results, errors });
    });

    app.listen(3000, () => {
        logger.info('Reference runner ready');
    });
})();
