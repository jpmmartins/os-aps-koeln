import { download } from 'google-fonts-helper';
import { join } from 'path';

export const downloadFont = async (url, { path, urlPrefix }) => {
    const downloader = download(url, {
        base64: false,
        overwriting: false,
        outputDir: path,
        stylePath: join(path, 'fonts.scss'),
        fontsDir: path,
        fontsPath: urlPrefix
    });
    await downloader.execute();
}
