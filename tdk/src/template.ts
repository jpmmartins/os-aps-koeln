import fs, { existsSync } from 'fs';
import { join, dirname, resolve } from 'path';
import winston from 'winston';
import { s3ToStream, listObjects } from '@sciflow/support';
import { readConfiguration, getComponentSchema } from '@sciflow/export';
import { dirSync } from 'tmp';
import { NodeVM } from 'vm2';
import * as ts from "typescript";
import { downloadFont } from './fonts';
import { createHash } from 'crypto';

const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console() });

/**
 * Loads a template from a provided path or S3 storage.
 */
export const loadTemplate = async ({
    projectId, templateId
}, opts: {
    /** The directory where templates are being stored */
    templateDir,
    /** The path to the template components */
    componentPath,
    /** The local disc location of the fonts */
    fontPath,
    /** The prefix to where fonts are served, e.g. /fonts/ or https://url/fonts/ */
    fontUrlPrefix
} = {
            templateDir: process.env.TEMPLATE_SOURCE,
            fontPath: process.env.FONT_PATH,
            componentPath: undefined,
            fontUrlPrefix: process.env.FONT_URL_PREFIX
        }) => {

    let customTemplateDirectory: any[] = [];
    if (projectId?.length > 0) {
        // read custom templates from the project directory
        customTemplateDirectory = await listObjects(projectId);
    }

    let assets: string[] = [];

    const dirObj = dirSync({ unsafeCleanup: true });
    if (templateId?.length > 0) {
        // try to find the template in the shared folder
        if (opts.templateDir) {
            logger.info('Loading template from file system', templateId);
            const readFiles = (path: string): string[] => {
                let assetPaths: string[] = [];
                if (!fs.existsSync(join(path))) { return assetPaths; }
                const assetFiles: fs.Dirent[] = fs.readdirSync(path, { withFileTypes: true });
                for (let assetFile of assetFiles) {
                    if (assetFile.isDirectory()) {
                        assetPaths = [...assetPaths, ...readFiles(join(path, assetFile.name))];
                    } else {
                        assetPaths.push(join(path, assetFile.name));
                    }
                }

                return assetPaths.flat();
            };

            if (fs.existsSync(join(opts.templateDir, templateId))) {
                assets = readFiles(join(opts.templateDir, templateId));
            }
        } else {
            logger.info('Loading template from s3', templateId);
            // TODO this logic should be moved to a shared lib to be used by all runners
            for (let asset of customTemplateDirectory
                .filter((a) => a.Key.startsWith(`${projectId}/exports/${templateId}`))) {
                try {
                    const assetFileName = asset.Key.replace(`${projectId}/exports/${templateId}/`, '');
                    const assetFile = join(dirObj.name, 'exports', templateId, assetFileName);
                    fs.mkdirSync(dirname(assetFile), { recursive: true });
                    const assetStream = fs.createWriteStream(assetFile);
                    await s3ToStream({ Key: asset.Key }, assetStream);
                    await new Promise((resolve) => assetStream.on('finish', resolve));
                    assets.push(assetFile);
                } catch (e) {
                    console.error('Could not access file', e.message, asset.Key);
                }
            }
        }

        let templateScript = assets.find(path => path.endsWith('template.tsx'));
        let render;
        let customRenderers = {};
        let stylePaths: any[] = [{ path: 'styles.scss', position: 'start' }];
        let customTemplateComponents = [];

        if (templateScript) {
            const script = fs.readFileSync(templateScript, 'utf-8');
            // TODO check for possible exploits / need of an additional sandbox
            // a better way might be to transpile using tsc before and then npm installing deps
            let result = ts.transpileModule(script, {
                compilerOptions: {
                    module: ts.ModuleKind.CommonJS,
                    target: ts.ScriptTarget.ES2020,
                    lib: ['DOM'],
                    jsx: ts.JsxEmit.React,
                    reactNamespace: 'TSX'
                }
            });
            // FIXME restrict this for security reasons
            // TODO limit max run time
            const root = [
                resolve(__dirname),
                resolve(__dirname, '../..', 'node_modules'),
                resolve(__dirname, '../../..', 'node_modules')
            ];
            const vm = new NodeVM({
                require: {
                    external: ['@sciflow/*'],
                    root,
                    resolve: (moduleName: string) => {
                        if (fs.existsSync(resolve(__dirname, '../../..', 'node_modules', moduleName))) {
                            return resolve(__dirname, '../../..', 'node_modules', moduleName);
                        } else if (fs.existsSync(resolve(__dirname, '../..', 'node_modules', moduleName))) {
                            return resolve(__dirname, '../..', 'node_modules', moduleName);
                        } else if (fs.existsSync(resolve(__dirname, 'node_modules', moduleName))) {
                            return resolve(__dirname, 'node_modules', moduleName);
                        }

                        console.error('Could not resolve ' + moduleName, {
                            paths: [
                                resolve(__dirname, 'node_modules', moduleName),
                                resolve(__dirname, '../..', 'node_modules', moduleName),
                                resolve(__dirname, '../../..', 'node_modules', moduleName)
                            ]
                        });

                        throw new Error('Could not resolve ' + moduleName);
                    }
                }
            });
            const scriptResult = vm.run(result.outputText, '/data/template.js');
            render = scriptResult.default;
            if (scriptResult.customRenderers) { customRenderers = scriptResult.customRenderers; }
            if (scriptResult.customTemplateComponents) { customTemplateComponents = scriptResult.customTemplateComponents; }
            if (scriptResult.stylePaths) { stylePaths = scriptResult.stylePaths; }

        }
        logger.info('Loaded template', { projectId, templateId });

        const configurationPath = assets.find(path => path.endsWith('.sfo.yml'));
        const metaDataSchemaPath = assets.find(path => path.endsWith('.schema.yml'));

        // transform local style paths into fully resolved paths
        stylePaths = stylePaths.map(({ path, position }) => {
            const asset = assets.find(assetPath => assetPath.endsWith(path));
            if (asset) {
                return { path: asset, position };
            }

            return null;
        }).filter(v => v != null);

        let css, configurations, metaData;
        if (configurationPath) {
            configurations = readConfiguration(configurationPath);
            metaData = metaDataSchemaPath ? readConfiguration(metaDataSchemaPath)[0] : {};
        }

        if (configurations?.length > 0) {
            const metaDataSchema = getComponentSchema(configurations.map(c => c.kind), configurations, opts.componentPath);
            if (metaData) {
                // shallow merge both schemas
                metaData = {
                    ...metaData,
                    properties: {
                        ...metaData.properties,
                        ...metaDataSchema.properties
                    }
                }
            } else {
                metaData = metaDataSchema;
            }
        }

        const googleFontConfiguration = configurations?.find(c => c.kind === 'GoogleFont');

        /** The absolute path to the font files on the disk */
        let fontPath;
        /** The prefix added to the css to load the font on the web (url or path) */
        let urlPrefix;
        if (googleFontConfiguration?.spec?.api?.url) {
            const hashValue = createHash('md5').update(googleFontConfiguration?.spec?.api?.url).digest('hex');
            fontPath = join(opts?.fontPath || join(opts?.templateDir, '..', 'fonts'), hashValue);
            // if we get a font path we use it as an absolute path in the font URL (e.g. for local production through the cli)
            urlPrefix = opts?.fontUrlPrefix ? join(opts?.fontUrlPrefix, hashValue) : '/' + join('fonts', hashValue);
            if (existsSync(fontPath)) {
                logger.info('Google Fonts already present', { fontPath, urlPrefix, spec: googleFontConfiguration?.spec });
            } else {
                logger.info('Downloading Google Fonts', { fontPath, urlPrefix, spec: googleFontConfiguration?.spec });
                await downloadFont(googleFontConfiguration?.spec?.api?.url, { path: fontPath, urlPrefix });
            }
            stylePaths.push({ path: join(fontPath, 'fonts.scss'), position: 'start' });
        }

        return {
            render,
            assets,
            customRenderers,
            customTemplateComponents,
            configuration: configurations?.find(conf => conf.kind === 'Configuration')?.spec,
            configurations,
            metaData,
            stylePaths
        };
    } else {
        throw new Error('Template must be provided');
    }
};
