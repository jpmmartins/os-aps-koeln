/**
 * Describes the structure of a template
 */
export interface Structure {
    parts: Part[];
}

interface Part {
    /** a unique identifier (optional) */
    id: string;
    type: 'chapter' | 'abstract' | 'bibliography';
    /** The part title */
    title: string;
    /** Body text for the part (accepts markdown) */
    body: string;
}