#!/usr/bin/env yarn run ts-node

import { createHTMLZip } from '@sciflow/export';
import { DocumentSnapshot, DocumentSnapshotPart } from '@sciflow/schema';
import { writeFileSync } from 'fs';
import { join } from 'path';
import { loadTemplate } from '../public-api';

const templateDir = join(__dirname, 'fixtures', 'templates');
const componentPath = join(__dirname, '..', '..', '..', 'libs', 'export', 'src', 'html');
const outPath = join(__dirname, 'outfiles');

/**
 * Generates a ZIP file of the HTML.
 */
const main = async (documentSnapshot: DocumentSnapshot, filename = documentSnapshot.documentId + '.zip') => {
    const template = await loadTemplate({ projectId: undefined, templateId: 'monograph' }, { templateDir, componentPath, fontPath: undefined, fontUrlPrefix: undefined });
    console.log('Read template', template.metaData);
    const document = await createHTMLZip(
        {
            ...documentSnapshot,
            parts: documentSnapshot.parts.map((part: DocumentSnapshotPart) => ({
                ...part,
                id: part.partId || part.id
            })),
            references: Object.keys(documentSnapshot.references || {}).map(id => documentSnapshot.references[id])
        },
        {
            configurations: template.configurations,
            metaData: documentSnapshot.metaData,
            metaDataSchema: template.metaData,
            customTemplateComponents: template.customTemplateComponents,
            customRenderers: template.customRenderers,
            stylePaths: template.stylePaths || [],
            scripts: []
        });

    writeFileSync(join(outPath, filename), document);
    console.log('Wrote file ' + join(outPath, filename));
};

export default main;