# Fixtures

These are demo documents used to test our export.

The fixtures ending on .snapshot.json are exported from the SciFlow platform.
sfo.snapshot.json are the currently used format at SFO / OS-APS.
This format does not have parts yet but consists of a linear document with heading 1.
