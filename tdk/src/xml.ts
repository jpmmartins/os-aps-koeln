import { SingleDocumentExportData, renderXML } from '@sciflow/export';

/**
 * Renderer function for the template (default export).
 */
export const defaultXMLTemplate = async (rendererInput: SingleDocumentExportData): Promise<any> => {
    return renderXML({ document: rendererInput.document }, {
        customRenderers: {},
        customTemplateComponents: [],
        authors: rendererInput.authors,
        files: rendererInput.files,
        configurations: rendererInput.configurations || [],
        references: rendererInput.references || [],
        metaData: rendererInput.metaData as any || {},
        metaDataSchema: rendererInput.metaDataSchema
    });
}
