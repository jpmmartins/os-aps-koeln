import { createHTMLZip } from '@sciflow/export';
import { DocumentSnapshot } from '@sciflow/schema';

/**
 * Renderer function for the template (default export).
 * Splits a document consisting of a single chapter into multiple parts for more advanced processing.
 * @returns a zip buffer with the manuscript.html and all files inside.
 */
export const defaultHTMLTemplate = async (documentSnapshot: DocumentSnapshot, template, opts: {
    inline: boolean;
    scripts: any[];
}): Promise<Buffer> => {
    documentSnapshot.parts = documentSnapshot.parts.map(part => ({
        ...part,
        id: part.partId || part.id
    }));

    const file = await createHTMLZip(
        documentSnapshot,
        {
            configurations: template.configurations,
            metaData: documentSnapshot.metaData,
            metaDataSchema: template.metaData,
            customTemplateComponents: template.customTemplateComponents,
            customRenderers: template.customRenderers,
            stylePaths: template.stylePaths || [],
            inline: opts?.inline,
            scripts: opts?.scripts || []
        });

    return file;
}
