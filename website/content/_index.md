---
title: "SciFlow Open Source"
date: 2021-02-01T18:58:57+01:00
draft: false
description: "SciFlow Open Source Community site"
---

<section class="col-12 lg-col-8 mt4">
    <h1 class="h00 m0"><b>Do more with your publications</b></h1>
    <h2>Open Tools for Single Source Publishing</h2>
</section>

<section class="flex flex-column col-12 lg-col-8">
    <p>Components and helper libraries that could help others to write, transform and publish their publications. We believe that open data formats will make it easier to use SciFlow products or build completely new apps on top.</p>
    <blockquote>“It is our mission to accelerate the research community’s transition towards open and sustainable scholarly publications.” (<a href="https://blog.sciflow.net/sciflow-2021-our-road-to-open-60eaed63b735">SciFlow Roadmap</a>)</blockquote>
    <div class="flex mt2 justify-end">
        <a class="mdc-button mdc-button--unelevated" href="https://docs.sciflow.org/">Read the docs</a>
    </div>
</section>
