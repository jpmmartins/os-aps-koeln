import { it, describe } from 'mocha';
import { expect } from 'chai';

import { Citation, CitationItem, SourceField } from '.';

describe('Reference utilities', async () => {
    it('Extracts source string', async () => {
        // expect.fail('not implemented');
        // FIXME #64
    });

     it('Encodes source string', async () => {
        const citation: Citation = {
            citationID: '1',
            citationItems: [
                {
                    id: 'test-book',
                    locator: '1',
                    label: 'page',
                    prefix: 'See',
                    suffix: 'See',
                    'author-only': true
                },
                {
                    id: 'test-book-2',
                }
            ],
            properties: {
                noteIndex: 0
            }
        };
        const encoded = encodeURI(SourceField.toString(citation));
        expect(encoded).to.equal('%5B%7B%22id%22:%22test-book%22,%22locator%22:%221%22,%22label%22:%22page%22,%22prefix%22:%22See%22,%22suffix%22:%22See%22,%22author-only%22:true%7D,%7B%22id%22:%22test-book-2%22%7D%5D');
    });
    it('Decodes source string singleReference', async () => {
        const source: string = '%5B%7B%22id%22:%2243579/ABCD2345%22%7D%5D';
        const decoded = SourceField.fromString(source);
        expect(decoded).to.deep.equal({
            citationItems: [
                {
                    id: '43579/ABCD2345'
                }
            ],
            properties: {
                noteIndex: 0
            }
        });
    });
    it('Decodes source string combinedRefences', async () => {
        const source: string = '[{\"id\":\"43579/NPHJT7TV\",\"locator\":\"2\",\"label\":\"page\"},{\"id\":\"43579/E2H4R9ZM\"}]';
        const decoded = SourceField.fromString(source);
        expect(decoded).to.deep.equal({
            citationItems: [
                {
                    id: '43579/NPHJT7TV',
                    locator: '2',
                    label: 'page'
                },
                {
                    id: '43579/E2H4R9ZM'
                }
            ],
            properties: {
                noteIndex: 0
            }
        });
    });
    it('Decodes source string', async () => {
        const source: string = '%5B%7B%22id%22:%22test-book%22,%22locator%22:%221%22,%22label%22:%22page%22,%22prefix%22:%22See%22,%22suffix%22:%22See%22,%22author-only%22:true%7D,%7B%22id%22:%22test-book-2%22%7D%5D';
        const decoded = SourceField.fromString(source);
        expect(decoded).to.deep.equal({
            citationItems: [
                {
                    id: 'test-book',
                    locator: '1',
                    label: 'page',
                    prefix: 'See',
                    suffix: 'See',
                    'author-only': true
                },
                {
                    id: 'test-book-2'
                }
            ],
            properties: {
                noteIndex: 0
            }
        });
    });
});
