import { CSLReference, getDefaults, ReferenceRenderer } from '@sciflow/cite';
import { DocumentNode, DocumentSnapshot, DocumentSnapshotImage, DocumentSnapshotResource, HeadingDocumentNode, SFNodeType } from '@sciflow/schema';
import { fetch } from 'cross-fetch';
import { JSDOM } from 'jsdom';
import JSZip from 'jszip';
import { extname } from 'path';
import { parse } from 'url';
import { create } from 'xmlbuilder2';
import { getValueOrDefault } from '../component';
import { createLogger, format, transports } from 'winston';
import { countryToLabel, createSectionHierarchy, extractFirstHeading, extractHeadingTitle, extractTitle, extractTitleFromHeading, slugify } from '../helpers';
import { generateListings } from '../html';
import { DocumentData, ExportOptions, SciFlowDocumentData } from "../interfaces";
import { RenderingEngine } from "../renderers";
import { XMLJSXFactory as TSX } from '../XMLJSXFactory';
import xmlRenderers from './renderers';
import { getAffiliationSlug, getAuthorObj, getAuthorSlug, getGroupObj } from './renderers/metaData';
import { getCitation, renderElementCitation, renderCitation, renderMixedCitationRef } from './renderers/references';
const { styleXML, localeXML } = getDefaults();

const dom = new JSDOM();
const virtualDocument = dom.window.document;

const logger = createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { service: 'main' },
    transports: [
        new transports.Console()
    ],
});

/**
 * Returns a renderer that can render a document.
 */
const getRenderer = ({ document, citationRenderer, templateOptions, metaData = {}, metaDataSchema = null, customRenderers = {}, configurations = [] as any[] }): RenderingEngine<HTMLElement, SciFlowDocumentData> => {
    return new RenderingEngine<HTMLElement, SciFlowDocumentData>(
        { ...xmlRenderers, ...customRenderers },
        { document, virtualDocument, citationRenderer, templateOptions, metaData, metaDataSchema, configurations },
        []
    );
}

/** Creates a ZIP file with the JATS XML and assets. */
export const createXMLZip = async (exportData: DocumentSnapshot, options?: ExportOptions) => {

    const citation = getCitation(exportData, options);
    const citationRenderer = new ReferenceRenderer(styleXML, localeXML, [...(exportData.references ?? []), citation], { format: 'html' });
    const configuration = options?.configurations?.find(c => c.kind === 'Configuration');
    const configurations = options?.configurations || [];
    const citationXMLFormat: string = getValueOrDefault('jats.citationFormat', exportData.metaData, options?.metaDataSchema) || 'elementCitation';
    const title = exportData.title || 'Untitled document';
    const altTitle = getValueOrDefault('article-meta.altTitle', exportData.metaData, options?.metaDataSchema);

    const articleType = getValueOrDefault('article-meta.articleType', exportData.metaData, options?.metaDataSchema);
    const doi = getValueOrDefault('article-meta.identifiers.doi', exportData.metaData, options?.metaDataSchema);
    const volume = getValueOrDefault('article-meta.volume', exportData.metaData, options?.metaDataSchema);
    const issue = getValueOrDefault('article-meta.issue', exportData.metaData, options?.metaDataSchema);

    const articleNumber = getValueOrDefault('article-meta.articleNumber', exportData.metaData, options?.metaDataSchema);
    // <article attributes>
    const articleTypeSlug = slugify(articleType || 'research-article');
    const locale = exportData.locale || configuration?.locale || 'en-US';

    // pub-date
    const epubDate = getValueOrDefault('article-meta.history.epubDate', exportData.metaData, options?.metaDataSchema);

    // <journal-meta> @see https://jats.nlm.nih.gov/publishing/tag-library/1.3/element/journal-meta.html
    const issn = getValueOrDefault('journal-meta.issn', exportData.metaData, options?.metaDataSchema);
    const publisherName = getValueOrDefault('journal-meta.publisherName', exportData.metaData, options?.metaDataSchema);
    const journalShortName = getValueOrDefault('journal-meta.shortName', exportData.metaData, options?.metaDataSchema);
    const journalTitle = getValueOrDefault('journal-meta.title', exportData.metaData, options?.metaDataSchema);
    const publisherLocation = getValueOrDefault('journal-meta.publisherLocation', exportData.metaData, options?.metaDataSchema);
    let journalMeta = <journal-meta>
        <journal-id journal-id-type="publisher-id">{journalShortName || '-'}</journal-id>
        {journalTitle ? <journal-title-group>
            <journal-title>{journalTitle}</journal-title>
            <abbrev-journal-title>{journalShortName}</abbrev-journal-title>
        </journal-title-group> : ''}
        {issn ? <issn publication-format="electronic">{issn}</issn> : ''}
        {publisherName ? <publisher>
            <publisher-name>{publisherName}</publisher-name>
            {publisherLocation ? <publisher-loc>{publisherLocation}</publisher-loc> : ''}
        </publisher> : ''}
    </journal-meta>;

    // history
    let history = '';
    const acceptedDate = getValueOrDefault('article-meta.history.acceptedDate', exportData.metaData, options?.metaDataSchema);
    const receivedDate = getValueOrDefault('article-meta.history.receivedDate', exportData.metaData, options?.metaDataSchema);
    if (acceptedDate || receivedDate) {
        history = <history>
            {receivedDate ? <date date-type="received">
                <day>{(new Date(receivedDate)).getUTCDate()}</day>
                <month>{(new Date(receivedDate)).getUTCMonth() + 1}</month>
                <year>{(new Date(receivedDate)).getUTCFullYear()}</year>
            </date> : ''}
            {acceptedDate ? <date date-type="accepted">
                <day>{(new Date(acceptedDate)).getUTCDate()}</day>
                <month>{(new Date(acceptedDate)).getUTCMonth() + 1}</month>
                <year>{(new Date(acceptedDate)).getUTCFullYear()}</year>
            </date> : ''}
        </history>
    }

    let permissions = '';
    const copyrightStatement = getValueOrDefault('article-meta.permissions.copyrightStatement', exportData.metaData, options?.metaDataSchema);
    const copyrightYear = getValueOrDefault('article-meta.permissions.copyrightYear', exportData.metaData, options?.metaDataSchema);
    const copyrightHolder = getValueOrDefault('article-meta.permissions.copyrightHolder', exportData.metaData, options?.metaDataSchema);
    const licenseType = getValueOrDefault('article-meta.permissions.license.type', exportData.metaData, options?.metaDataSchema);
    const licenseUrl = getValueOrDefault('article-meta.permissions.license.url', exportData.metaData, options?.metaDataSchema);
    const licenseText = getValueOrDefault('article-meta.permissions.license.text', exportData.metaData, options?.metaDataSchema);
    if (copyrightStatement || licenseText) {
        permissions = <permissions>
            {copyrightStatement ? <copyright-statement>{copyrightStatement}</copyright-statement> : ''}
            {copyrightYear ? <copyright-year>{copyrightYear}</copyright-year> : ''}
            {copyrightHolder ? <copyright-holder>{copyrightHolder}</copyright-holder> : ''}
            {licenseUrl ? <license license-type={licenseType || 'open-access'} xlink:href={licenseUrl}>
                {licenseText ? <license-p>{licenseText}</license-p> : ''}
            </license> : ''}
        </permissions>
    }

    const format = new Intl.DateTimeFormat(exportData.locale || 'en-US', { month: 'long', day: 'numeric', year: 'numeric', timeZone: 'UTC' });
    const formatDate = (date: Date) => date ? format.format(new Date(date)) : '';

    const runner = 'jatsxml';
    const styles: any[] = [];
    const scripts: any[] = [];
    let files: DocumentSnapshotResource[] = (exportData.files || [])
        .map(file => {
            if (file.type === 'image') {
                // set a max width for epub exports
                const url = new URL(file.url);
                url.searchParams.append('width', '1024');
                url.searchParams.append('format', 'jpeg');
                file.url = url.toString();
            }
            return {
                ...file,
                id: (file.fileId || file.id) as string
            };
        });

    const zip = new JSZip();
    const contentDir = zip.folder('assets');

    const customTemplateComponents = options?.customTemplateComponents || [];

    // download media
    if (files.some(file => file.type === 'image') && !options?.inline) {
        files = await Promise.all(files.map(async (file) => {
            if (file.type === 'image') {
                // make sure to remove any query strings that might still be attached
                const parsedUrl = parse(file.url);
                let ext = extname(parsedUrl.pathname || '');
                if (parsedUrl.query) {
                    const params = new URLSearchParams(parsedUrl.query);
                    if (params.get('format')) {
                        ext = '.' + params.get('format') as string;
                    }
                }
                const source = await fetch(file.url);
                if (source.status === 200) {
                    const content = await source.blob();
                    if (content) {
                        const buffer = await content.arrayBuffer();
                        const filePath = `${file.id}${ext}`;
                        // we save the images to the content dir because apple books showed issues with relative imports
                        if (contentDir?.file(`${file.id}${ext}`, Buffer.from(buffer), { binary: true })) {
                            file.url = `assets/${filePath}`;
                            file.mimeType = content.type;
                            return file;
                        }
                    }
                }
            }
            return file;
        }));
    } else {
        // we keep the files with their urls
        files = files.map(file => {
            if (file.url.startsWith('http')) {
                const url = new URL(file.url);
                file.url = url.toString().replace(url.search, '');
            }
            return file;
        })
    }

    const document: DocumentData = {
        documentId: exportData.documentId,
        title: exportData.title?.document ? extractFirstHeading(exportData.title?.document) : 'Untitled document',
        subtitle: undefined,
        titlePart: exportData.parts.filter(p => p.type === 'title'),
        parts: exportData.parts,
        index: exportData.parts.filter(p => p.type !== 'title').map(p => p.id),
        metaData: {},
        images: (files || []).filter(file => file.type === 'image') as DocumentSnapshotImage[],
        files,
        tables: await generateListings(exportData.parts.map(p => ({
            ...p.document,
            attrs: {
                ...p.document.attrs,
                id: p.document.attrs?.id || p.partId || p.id,
                numbering: p.numbering,
                role: p.role,
                schema: p.schema,
                type: p.type
            }
        }))),
        authors: exportData.authors || [],
        references: exportData.references || []
    }

    // the node renderer renders the document tree itself
    const htmlEngine: RenderingEngine<HTMLElement, SciFlowDocumentData> = getRenderer({
        document,
        configurations,
        citationRenderer,
        customRenderers: {},
        templateOptions: configuration,
        metaData: options?.metaData,
        metaDataSchema: options?.metaDataSchema
    });

    // we move the part information into the root of the part document itself
    const parts: DocumentNode<SFNodeType.document>[] = exportData.parts.map((part => ({
        ...part.document,
        attrs: {
            ...part.document.attrs,
            id: part.document.attrs?.id || part.partId || part.id,
            numbering: part.numbering,
            role: part.role,
            schema: part.schema,
            placement: part.placement || part.options?.placement,
            type: part.type
        }
    })));

    try {

        const renderedManuscriptSections: any[] = [];
        for (let part of parts) {
            if (part.attrs?.id === 'title') { continue; }
            // render the parts in order
            if (!part.content) { continue; }
            const sections = createSectionHierarchy(part.content);
            if (sections.length > 0) { sections[0].attrs = part.attrs; }
            renderedManuscriptSections.push({
                partId: part.attrs.id,
                role: part.attrs.role,
                type: part.attrs.type,
                placement: part.attrs.placement,
                title: extractFirstHeading(part),
                document: sections[0],
                dom: await htmlEngine.render(sections[0])
            });
        }

        const footnotes = await Promise.all(document.tables.footnotes.map(async (footnote) => {
            const content = await htmlEngine.renderContent(footnote);
            return {
                footnote,
                children: content
            };
        }));

        let { references, formatting } = await htmlEngine.data.citationRenderer.getBibliography();

        const citations: { [format: string]: any[] } = {
            elementCitation: document.references.map((ref) => renderElementCitation(ref)),
            mixedCitation: references
                .map((referenceString, i) => {
                    const entryIds: string[] = formatting?.entry_ids?.[i];
                    const cslReferences: CSLReference[] = entryIds.map(id => document.references.find(r => r.id === id));
                    if (cslReferences.length === 0) { return; }
                    return renderMixedCitationRef(cslReferences[0], referenceString);
                }).filter(r => r !== undefined)
        }

        let refList;
        try {
            refList = <ref-list>
                <title>References</title>
                {...citations[citationXMLFormat]}
            </ref-list>;
        } catch (e) {
            console.error(e.message);
            throw new Error('Could not create XML ref list');
        }

        const affiliations = document.authors.map((author) => author.positions).flat()
            .map(position => ({
                ...position,
                country: position.country ? countryToLabel(position.country, locale) : undefined,
                slug: getAffiliationSlug(position)
            }))
            .filter(position => position != null)
            .filter((position, index, list) => list.findIndex((pos) => pos.slug === position.slug) === index);

        const funders = document.authors.map((author) => author.funders || []).flat()
            .filter(funder => funder != null)
            .filter((funder, index, list) => list.findIndex((pos) => pos.id === funder.id) === index);

        // get keywords
        const keywordPart = renderedManuscriptSections?.find(({ role }) => role === 'keywords');
        let keywords;
        if (keywordPart) {
            const paragraph = keywordPart.document.content.find(c => c.type === 'paragraph');
            keywords = paragraph?.content?.find(({ type }) => type === 'text')?.text?.split(',').map(s => s.trim());
        }

        const abstracts = await Promise.all(parts.filter((part) => part.attrs.type === 'abstract').map(async (part) => {
            if (!part.content) { return; }
            const sections = createSectionHierarchy(part.content?.filter(c => c.attrs?.level !== 1));
            const abstractDoc = await htmlEngine.renderContent(sections[0]);
            return {
                partId: part.attrs.id,
                role: part.attrs.role,
                type: part.attrs.type,
                placement: part.attrs.placement,
                title: extractFirstHeading(part),
                document: part,
                dom: <abstract>{...abstractDoc}</abstract>
            }
        }));

        const textBody = renderedManuscriptSections.filter((part) => {
            return part.type !== 'title' && part.placement === 'body' && part.type !== 'abstract' && part.role !== 'keywords' && part.role !== 'acknowledgements';
        });

        const textBack = renderedManuscriptSections.filter((part) => part.placement === 'back' && part.role !== 'acknowledgements');
        // acknowledgements
        const authorNames = citationRenderer.renderCitationOutOfContext([{
            id: citation.id,
            "author-only": true,
            itemData: { [citation.id]: citation }
        }]);
        let citeAs;

        try {
            const completeBib = citationRenderer.getBibliographyFromAll();
            const selfCitationIndex = completeBib.formatting.entry_ids.findIndex(ids => ids.includes(citation.id))
            if (selfCitationIndex > -1) {
                citeAs = renderCitation(citation, completeBib.references[selfCitationIndex], 'p');
            }
        } catch (e) {
            console.error(e);
        }

        const acknowledgementContent = parts.filter((part) => part?.attrs?.role === 'acknowledgements').map(part => part.content).flat() as DocumentNode<any>[];
        let acknowledgementPart;
        if (acknowledgementContent.length > 0) {
            const ackDoc: DocumentNode<SFNodeType.document> = {
                type: SFNodeType.document,
                content: createSectionHierarchy(acknowledgementContent.map((child) => {
                    if (child.type === SFNodeType.heading) {
                        const headingTitle = extractTitleFromHeading(child as HeadingDocumentNode);
                        if (headingTitle) {
                            child.attrs.id = slugify(headingTitle);
                        }
                    }

                    return child;
                }))
            }

            const content = await htmlEngine.renderContent(ackDoc);
            if (citeAs) {
                // TODO this should probably be template specific
                const citeAsSec = <sec sec-type="cite-as">
                    <title>Cite as</title>
                    {citeAs}
                </sec>;
                content.push(citeAsSec);
            }

            acknowledgementPart = {
                partId: 'acknowledgements',
                role: 'acknowledgements',
                type: 'chapter',
                placement: 'back',
                title: 'Acknowledgements',
                document: null,
                dom: <ack>{...content}</ack>
            }
        }

        const hasCorrespondingAuthors = document.authors.some(author => author.correspondingAuthor && author.email?.length > 0);

        const xmlDocument = create({
            version: '1.0',
            encoding: 'UTF-8',
            standalone: true
        })
            .import(<article
                xml:lang={locale}
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                article-type={articleTypeSlug}
                dtd-version="1.0"
            >
                <front>
                    {journalMeta}
                    <article-meta>
                        {doi ? <article-id pub-id-type="doi">https://doi.org/{doi}</article-id> : ''}
                        <title-group>
                            <article-title>{document.title}</article-title>
                            {document.subtitle ? <subtitle>{document.subtitle}</subtitle> : ''}
                            {altTitle?.length > 0 ? <alt-title>{altTitle}</alt-title> : ''}
                        </title-group>
                        {document.authors.length > 0 ? <contrib-group>
                            {document.authors.map(author => author.roles?.includes('author') ? getAuthorObj(author) : getGroupObj(author, document.authors))}
                            {affiliations.map(affiliation =>
                                <aff id={affiliation.slug}>
                                    {affiliation.institution?.length > 0 ? <institution content-type="orgname">{affiliation.institution}</institution> : ''}
                                    {affiliation.department?.length > 0 ? <institution content-type="orgdiv1">{affiliation.department}</institution> : ''}
                                    <addr-line>{affiliation.street}</addr-line>
                                    <city>{affiliation.city}</city>
                                    {affiliation.country && affiliation.country?.length > 0 ? <country>{affiliation.country}</country> : ''}
                                </aff>)}
                        </contrib-group> : ''}
                        {hasCorrespondingAuthors ?
                            <author-notes>{...document.authors.filter(author => author.correspondingAuthor && author.email?.length > 0).map((author) =>
                                <corresp id={'corr-' + getAuthorSlug(author)}>
                                    Corresponding author: <email>{author.email}</email>
                                </corresp>
                            )}</author-notes>
                            : ''}
                        {epubDate ? <pub-date pub-type="epub">
                            <day>{(new Date(epubDate)).getUTCDate()}</day>
                            <month>{(new Date(epubDate)).getUTCMonth() + 1}</month>
                            <year>{(new Date(epubDate)).getUTCFullYear()}</year>
                        </pub-date> : ''}
                        {volume ? <volume>{volume}</volume> : ''}
                        {issue ? <issue>{issue}</issue> : ''}
                        {history}
                        {permissions}
                        {abstracts.map(s => s?.dom || '')}
                        {keywords?.length > 0 ? <kwd-group kwd-group-type="author">
                            {...keywords.map(s => <kwd>{s}</kwd>)}
                        </kwd-group> : ''}
                    </article-meta>
                </front>
                <body>
                    {...textBody.map(s => s.dom)}
                </body>
                <back>
                    {acknowledgementPart ? acknowledgementPart.dom : ''}
                    {...textBack.map(s => s.dom)}
                    {refList}
                    {footnotes.length > 0 ? <fn-group content-type="footnotes">
                        <title>Footnotes</title>
                        {footnotes.map(({ footnote, children }, index) => {
                            return <fn id={slugify(footnote.attrs?.id)}>
                                <label>{index + 1}</label>
                                <p>{...children}</p>
                            </fn>
                        })}
                    </fn-group> : ''}
                </back>
            </article>);

        zip.file('manuscript.xml', xmlDocument
            .dtd({
                pubID: '-//NLM//DTD JATS (Z39.96) Journal Publishing DTD v1.3 20210610//EN',
                sysID: 'https://jats.nlm.nih.gov/publishing/1.3/JATS-journalpublishing1-3.dtd'
            })
            .end({ prettyPrint: false })
            .replaceAll('<i>', '<italic>')
            .replaceAll('</i>', '</italic>')
            .replaceAll('<b>', '<bold>')
            .replaceAll('</b>', '</bold>')
            .replaceAll(/data-(\S+)=\"((?:\\.|[^"\\])*)"/g, '')
        );

        return await zip.generateAsync({
            type: 'nodebuffer',
            mimeType: 'application/zip'
        });
    } catch (e) {
        logger.error('Could not render xml', { message: e.message, documentId: exportData.documentId });
        throw new Error(e.message);
    }
};

/** Renders a manuscript to HTML using custom renderers
 * @deprecated use createXMLZip to receive the XML and all included assets.
 */
const renderXMLSinglePart = async (manuscript, {
    configurations, references, authors, metaData = {}, metaDataSchema = null, customRenderers = {}, customTemplateComponents = [], files
}) => {

    const document = manuscript.document;
    const citationRenderer = new ReferenceRenderer(styleXML, localeXML, references ?? []);
    const configuration = configurations?.find(c => c.kind === 'Configuration');

    const title = extractTitle(document);

    const data = {
        document: {
            title,
            images: [], // TODO store full resolution images
            ...document,
            files,
            tables: await generateListings(document),
            authors,
            references: references.map(reference => ({
                ...reference,
                id: slugify(reference.id)
            }))
        },
        configurations,
        citationRenderer,
        templateOptions: configuration,
        metaData,
        metaDataSchema
    };

    // the node renderer renders the document tree itself
    const documentNodeRenderer: RenderingEngine<HTMLElement, SciFlowDocumentData> = getRenderer({
        ...data,
        customRenderers
    });

    // adding Authors to root vars
    let authorList: any[] = [];
    authors.map(author => {
        authorList.push(author?.firstName + ' ' + author?.lastName);
    });
    let authorRootVariables: any[] = [];
    authorRootVariables.push("--authors:" + "'" + authorList + "'")

    // remove the title element and then create parts for each heading 1
    const sections = createSectionHierarchy(manuscript.document.content.filter(t => t.type !== 'header'));
    const lang = configuration?.locale ?? 'en-US';
    const renderedSections = await Promise.all(sections.map(section => {
        try {
            return documentNodeRenderer.render(section);
        }
        catch (e) {
            console.error('Could not render section', section, e);
            throw new Error('Could not render section ' + section?.attrs?.id);
        }
    }));

    const authorGroupCollaborators = authors.filter((author) => !author.groups || author.groups.length === 0);

    const footnotes = await Promise.all(data.document.tables.footnotes.map(async (footnote) => {
        const content = await documentNodeRenderer.renderContent(footnote);
        return {
            footnote,
            children: content
        };
    }));

    let refList;
    try {
        refList = <ref-list>
            <title>References</title>
            {references.map((ref) => renderElementCitation(ref))}
        </ref-list>;
    } catch (e) {
        console.error(e.message);
        throw new Error('Could not create XML ref list');
    }

    const result = create({ version: '1.0', encoding: 'UTF-8', standalone: true });
    result.import(<article xml:lang="EN" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" article-type="research-article">
        <front>
            <article-meta>
                <title-group>
                    <article-title>{data.document.title}</article-title>
                    {data.document.subtitle ? <subtitle>{data.document.subtitle}</subtitle> : ''}
                </title-group>
                {authors.length > 0 ? <contrib-group>
                    {authors.map(author => author.type === 'author' ? getAuthorObj(author) : getGroupObj(author, authors))}
                </contrib-group> : ''}
            </article-meta>
            <journal-meta>
                <publisher-name></publisher-name>
            </journal-meta>
        </front>
        <body>
            {...renderedSections}
        </body>
        <back>
            {refList}
            {footnotes.length > 0 ? <fn-group content-type="footnotes">
                <title>Footnotes</title>
                {footnotes.map(({ footnote, children }, index) => {
                    return <fn id={slugify(footnote.attrs?.id)}>
                        <label>{index + 1}</label>
                        <p>{...children}</p>
                    </fn>
                })}
            </fn-group> : ''}
        </back>
    </article>);

    return result;
}

export {
    renderXMLSinglePart as renderXML
};
