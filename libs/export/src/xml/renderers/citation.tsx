import { JSDOM } from 'jsdom';
import { XMLJSXFactory as TSX } from '../../XMLJSXFactory';
import { TSX as HTMLJSXFactory } from '../../TSX';

import { SFNodeType, DocumentNode } from '@sciflow/schema';

import { SciFlowDocumentData } from './../../interfaces';
import { NodeRenderer } from '../../renderers';
import { SourceField } from '@sciflow/cite';
import { escapeId, slugify } from '../../helpers';

const renderers: { [key: string]: any } = {
    [SFNodeType.citation]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.citation>) {
            const citation = SourceField.fromString(node.attrs.source);
            if (!citation) {
                return <bold>Unreadable reference</bold>;
            }
            try {
                const processed = this.engine.data.citationRenderer.renderCitation(citation);
                const id = citation.citationItems?.[0]?.id;
                if (processed.renderedCitation.includes('PRINT')) {
                    return <bold>{processed.renderedCitation}</bold>;
                }

                try {
                    // try to create the full element with italics
                    const c = HTMLJSXFactory.htmlStringToElement(`<xref ref-type="bibr" rid="${slugify(id)}">${processed.renderedCitation.replace('<i>', '<italic>')
                    .replace('</i>', '</italic>')
                    .replace('<b>', '<bold>')
                    .replace('</b>', '</bold>')}</xref>`);
                    if (!c) {
                        return <bold>NO PRINTED REFERENCE FORM</bold>;
                    }
                
                    return c;
                } catch (e) {
                    console.error(e);
                }

                return processed.renderedCitation;
            } catch (e) {
                if (e instanceof Error) {
                    this.engine.log('error', 'Could not render reference ' + citation?.citationItems?.map(i => i.id).join(), { message: e.message, reference: citation });
                }
                return <bold>Missing reference {citation?.citationItems?.map(i => i.id).join()}</bold>;
            }
        }
    }
};

export default renderers;
