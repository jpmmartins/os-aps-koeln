// this import is needed for TSX to work (look into tsconfig.json for configuration)
import { CSLReference } from '@sciflow/cite';
import { slugify } from '../../helpers';
import { XMLJSXFactory as TSX } from '../../XMLJSXFactory';
import { TSX as HTMLTSXFactory } from '../../TSX';
import { XMLBuilderImpl } from 'xmlbuilder2/lib/builder';
import { DocumentSnapshot } from '@sciflow/schema';
import { getValueOrDefault } from '../../component';
import { ExportOptions } from '../../interfaces';

/**
 * Creates a tag if a value is present or an empty string if not.
 */
const createTag = (tagName, value?, attrs?) => {
    if (!value) { return ''; }
    const el = TSX.createElement(tagName, attrs, value);
    return el;
}

/**
 * Renders a CSL citation as a jats element-citation element.
 */
export const renderElementCitation = (ref: CSLReference): string => {
    const citation = <ref id={slugify(ref.id)}>
        <element-citation publication-type={ref.type}>
            <person-group person-group-type="author">
                {ref.author?.map(author =>
                    <name>
                        <surname>{author?.family}</surname>
                        <given-names>{author?.given}</given-names>
                    </name>
                )}
            </person-group>
            {createTag('year', ref.issued?.['date-parts']?.[0])}
            {createTag('month', ref.issued?.['date-parts']?.[1])}
            {createTag('article-title', ref.title)}
            {createTag('source', ref['container-title'])}
            {createTag('volume', ref.volume)}
            {createTag('pub-id', ref.DOI, { 'pub-id-type': 'doi' })}
        </element-citation>
    </ref>;
    return citation;
}

export const renderCitation = (ref: CSLReference, referenceString: string, tagName = 'mixed-citation'): XMLBuilderImpl => {
    const referenceEl: HTMLDivElement = HTMLTSXFactory.htmlStringToElement(`<div>${referenceString
        ?.trim()
        .replace('<div class="csl-entry">', '')
        .replace('</div>', '')
        .replace(/(https?:\/\/[^\s]+)/g, '<a href="$1">$1</a>')}</div>`) as HTMLDivElement;

    const citation = TSX.createElement(tagName, {});

    for (let i = 0; i < referenceEl.childNodes.length; i++) {
        const n = referenceEl.childNodes[i];
        if (n.textContent) {
            switch (n.nodeName) {
                case '#text':
                    citation.txt(TSX.escapeString(n.textContent));
                    break;
                case 'B':
                    citation.import(TSX.createElement('bold', {}, [TSX.escapeString(n.textContent)]));
                    break;
                case 'I':
                    citation.import(TSX.createElement('italic', {}, [TSX.escapeString(n.textContent)]));
                    break;
                case 'A':
                    citation.import(TSX.createElement('ext-link', {
                        'ext-link-type': n.textContent.includes('doi') ? 'doi' : 'uri',
                        'xlink:href': n.textContent.replace('&', '#26')
                    }, [TSX.escapeString(n.textContent)]));
                    break;
                default:
                    citation.txt(TSX.escapeString(n.textContent));
            }
        }
    }
    return citation;
}

export const renderMixedCitationRef = (ref: CSLReference, referenceString: string): XMLBuilderImpl => {
    const citation = renderCitation(ref, referenceString);
    if (ref.type) { citation.att('publication-type', ref.type); }
    return <ref id={slugify(ref.id)}>{citation}</ref>;
}

export const getCitation = (document: DocumentSnapshot, options?: ExportOptions): CSLReference => {

    const get = (path: string) => getValueOrDefault(path, document.metaData, options?.metaDataSchema);
    const epubDate = get('article-meta.history.epubDate');

    const articleNumber = get('article-meta.articleNumber');
    const csl = {
        id: document.documentId,
        type: 'article-journal',
        title: document.title,
        'container-title': get('journal-meta.title'),
        volume: get('article-meta.volume'),
        issue: get('article-meta.issue'),
        DOI: get('article-meta.identifiers.doi'),
        page: articleNumber ? `Article ${articleNumber}` : undefined,
        author: document.authors?.map(author => ({
            family: author.lastName,
            given: author.firstName
        })),
        issued: epubDate ? {
            'date-parts': [
                (new Date(epubDate)).getUTCFullYear(),
                (new Date(epubDate)).getUTCMonth() + 1,
                (new Date(epubDate)).getUTCDate()
            ]
        } : {
            'date-parts': [
                [(new Date()).getFullYear()]
            ]
        }
    };

    for (let key of Object.keys(csl)) {
        if (csl[key] === undefined || csl[key]?.length === 0) {
            delete csl[key];
        }
    }

    return csl;
}
