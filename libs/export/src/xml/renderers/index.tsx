import { DocumentNode, SFNodeType } from '@sciflow/schema';
import { XMLJSXFactory as TSX, XMLJSXFactory } from '../../XMLJSXFactory';
import { SciFlowDocumentData } from '../../interfaces';
import { NodeRenderer } from '../../renderers';
import htmlRenderers from '../../html/renderers';
import tableRenderers from './table';
import citationRenderers from './citation';
import { SourceField } from '@sciflow/cite';
import { create, convert } from 'xmlbuilder2';
import { FigureRendererConfiguration } from '../../html/renderers/figure/figure.schema';
import { Figure } from '../../html/components/figure/figure.component';
import { slugify } from '../../helpers';
import { XMLBuilderImpl } from 'xmlbuilder2/lib/builder';

class RenderWarning extends NodeRenderer<any, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.heading>) {
        return convert(`${node.type}`).toString();
    }
}

const renderers: { [key: string]: any } = {
    ...htmlRenderers,
    ...tableRenderers,
    ...citationRenderers,
    [SFNodeType.section]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.section>) {
            try {
                const content = await Promise.all(node.content?.map(async (c) => {
                    const rc: any = await this.engine.render(c);
                    return rc;
                }) || '');
                return <sec id={slugify(node.attrs.id)} sec-type={node.attrs.type}>
                    {...content}
                </sec>;
            } catch (e) {
                debugger;
                console.error(e);
                throw new Error('Could not render section ' + node?.attrs?.id);
            }
        }
    },
    [SFNodeType.blockquote]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.blockquote>) {
            const children = await this.engine.renderContent(node);
            return <disp-quote>{...children}</disp-quote>;
        }
    },
    [SFNodeType.bullet_list]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.bullet_list>) {
            const children = await this.engine.renderContent(node);
            return <list list-type="bullet">{...children}</list>;
        }
    },
    [SFNodeType.list_item]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.list_item>) {
            const children = await this.engine.renderContent(node);
            return <list-item>{...children}</list-item>;
        }
    },
    [SFNodeType.ordered_list]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.ordered_list>) {
            const children = await this.engine.renderContent(node);
            return <list list-type="order">{...children}</list>;
        }
    },
    [SFNodeType.caption]: RenderWarning,
    [SFNodeType.code]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.code>) {
            const children = await this.engine.renderContent(node);
            return <code language={node.attrs.language ?? 'text'}>
                {...children}
            </code>;
        }
    },
    [SFNodeType.footnote]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.footnote>) {
            const index = this.engine.data.document.tables?.footnotes?.findIndex(fn => fn?.attrs?.id === node.attrs?.id);
            return <xref ref-type="fn" rid={slugify(node.attrs.id)}>{index > -1 ? (index + 1) : '*'}</xref>;
        }
    },
    [SFNodeType.hardBreak]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.hardBreak>) {
            // https://jats.nlm.nih.gov/publishing/tag-library/1.3/element/break.html
            // breaks are discourraged
            return '';
        }
    },
    [SFNodeType.header]: RenderWarning,
    [SFNodeType.horizontalRule]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.horizontalRule>) {
            return <hr />;
        }
    },
    [SFNodeType.hyperlink]: RenderWarning,
    [SFNodeType.image]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.image>) {
            const file = this.engine.data.document.files.find(f => f.id === node.attrs.id);
            return <graphic xmlns_xlink="http://www.w3.org/1999/xlink" xlink_href={file?.url} />;
        }
    },
    [SFNodeType.link]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.link>) {
            const linkId = node.attrs && node.attrs.href && node.attrs.href.replace('#', '');
            const children = await this.engine.renderContent(node);

            const figures = this.engine.data.document.tables.figures.filter(figure => figure.title && figure.title.length > 0);
            const tables = this.engine.data.document.tables.tables.filter(table => table.title && table.title.length > 0);
            const equations = this.engine.data.document.tables.equations.filter(equation => equation.attrs?.type === 'block');
            const secRef = this.engine.data.document.tables.headings.find(heading => heading.id === linkId);
            const equationRef = equations.find(figure => figure.id === linkId);
            const figRef = figures.find(figure => figure.id === linkId);
            const tableRef = tables.find(figure => figure.id === linkId);

            const figureComponent = new Figure(this.engine as any); // TODO this should work with XML as well
            const configuration: FigureRendererConfiguration = figureComponent.getValues();

            if (node.attrs.type !== 'xref') {
                return;
            }

            if (secRef) {
                return <xref ref-type="sec" rid={slugify(linkId)}>{secRef.title}</xref>;
            }

            if (equationRef) {
                return <xref ref-type="disp-formula" rid={slugify(linkId)}>{configuration?.equation?.referenceLabel || 'eq.'} {equations.findIndex(fig => fig.id === linkId) + 1}</xref>;
            }

            if (tableRef) {
                return <xref ref-type="table" rid={slugify(linkId)}>{configuration?.table?.referenceLabel || 'tbl.'} {tables.findIndex(fig => fig.id === linkId) + 1}</xref>;
            }

            if (figRef) {
                return <xref ref-type="fig" rid={slugify(linkId)}>{configuration?.image?.referenceLabel || 'fig.'} {figures.findIndex(fig => fig.id === linkId) + 1}</xref>;
            }
        }
    },
    [SFNodeType.math]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.math>) {
            if (node.attrs.style === 'block') {
                return <disp-formula id={slugify(node.attrs.id)}>
                    <tex-math>
                        {node.attrs.tex}
                    </tex-math>
                </disp-formula>;
            }

            return <inline-formula>
                <tex-math>
                    {node.attrs.tex}
                </tex-math>
            </inline-formula>;
        }
    },
    [SFNodeType.pageBreak]: RenderWarning,
    [SFNodeType.paragraph]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.paragraph>) {
            const paragraphContent = await this.engine.renderContent(node);
            return <p id={slugify(node.attrs.id)}>{...paragraphContent}</p>;
        }
    },
    [SFNodeType.quote]: RenderWarning,
    [SFNodeType.text]: class extends NodeRenderer<XMLBuilderImpl | string, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.text>) {
            const hyperlink = node.marks?.find(m => m.type === 'anchor');
            let text = node.text;

            if (hyperlink) {
                return <ext-link ext-link-type='uri' xlink_href={hyperlink.attrs.href.replace('&', '#26')}>{text}</ext-link>;
            }

            if (node.marks?.some(mark => mark.type === 'strong')) {
                text = <b>{text}</b>;
            }

            if (node.marks?.some(mark => mark.type === 'em')) {
                text = <italic>{text}</italic>;
            }

            if (node.marks?.some(mark => mark.type === 'sub')) {
                text = <sub>{text}</sub>;
            }

            if (node.marks?.some(mark => mark.type === 'sup')) {
                text = <sup>{text}</sup>;
            }

            return text;
        }
    },
    [SFNodeType.figure]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.figure>) {
            if (node.type !== 'figure') {
                throw new Error('Must start with a figure but found ' + node.type);
            }

            if (!node.content) {
                return <fig />;
            }

            const file = this.engine.data.document.files.find(f => f.id === node.attrs.id);
            const captionWrapper = node.content.find(node => node.type === 'caption');
            const caption = captionWrapper?.content && captionWrapper.content[0];
            const notes = captionWrapper?.content && captionWrapper.content.slice(1);
            const figures = this.engine.data.document.tables.figures.filter(table => table.title && table.title.length > 0);
            const tables = this.engine.data.document.tables.tables.filter(table => table.title && table.title.length > 0);

            const captionContent = caption ? (await this.engine.renderContent(caption)) : '';
            const captionNotesContent = notes ? await Promise.all(notes.map(async (note) => this.engine.renderContent(note))) : [];
            // if (captionNotesContent.length === 0) { captionNotesContent.push(''); }
            if (node.content?.some(c => c.type === 'table')) {
                const index = tables.findIndex(figure => figure.attrs.id === node.attrs.id || figure.figureId === node.attrs.id) + 1;
                const tableContent = await this.engine.renderContent({ ...node, content: node.content.filter(n => n.type === 'table') });
                return <table-wrap id={slugify(node.attrs?.id)}>
                    <label>Table {index > 0 ? index : ''}.</label>
                    <caption>
                        <title>{captionContent}</title>
                    </caption>
                    {...tableContent}
                    {captionNotesContent.length > 0 ? <table-wrap-foot>{...captionNotesContent.map(note => <p>{note}</p>)}</table-wrap-foot> : ''}
                </table-wrap>;
            }

            const index = figures.findIndex(figure => figure.attrs.id === node.attrs.id) + 1;
            return <fig id={slugify(node.attrs?.id)} fig-type="content-image">
                <label>Figure {index > 0 ? index : ''}.</label>
                <caption>
                    <title>{captionContent}</title>
                    {...captionNotesContent.map(note => <p>{note}</p>)}
                </caption>
                <graphic xmlns_xlink="http://www.w3.org/1999/xlink" xlink_href={file?.url} />
            </fig>;
        }
    },
    [SFNodeType.heading]: class extends NodeRenderer<Element, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.heading>) {
            const content = await this.engine.renderContent(node);
            return <title>
                {...content}
            </title>;
        }
    },
    [SFNodeType.document]: class extends NodeRenderer<Element, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.document>) {
            const content = await this.engine.renderContent(node);
            return <article xmlns_xlink="http://www.w3.org/1999/xlink" article-type="research-article">
                <front></front>
                <body>
                    {...content}
                </body>
                <back></back>
            </article>;
        }
    },
    [SFNodeType.placeholder]: RenderWarning
}

export default renderers;
