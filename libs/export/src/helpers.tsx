import { createId, DocumentNode, DocumentSnapshot, DocumentSnapshotPart, HeadingDocumentNode, SFNodeType } from "@sciflow/schema";
import { v4 as uuidv4 } from 'uuid';
import { SciFlowDocumentData, SingleDocumentExportData } from "./interfaces";
import { RenderingEngine } from "./renderers";
import { TSX } from './TSX';

const getTextContent = (node: DocumentNode<any>) => {
    return node.content?.map((n) => n.text).join('');
}

/**
 * Converts a document from the single chapter schema to a document split into parts.
 * This is needed to import into SciFlow or use advanced template processing.
 */
export const convertToSnapshot = (input: SingleDocumentExportData, opts: { assetUrl?: string; }): DocumentSnapshot => {

    // split the document
    if (!input.document?.content) {
        throw new Error('Document can\'t be empty');
    }

    const parts = createParts(input.document.content);
    const locale = input.configuration?.locale ?? 'en-US';
    const documentId = uuidv4();
    const data: DocumentSnapshot = {
        apiVersion: 1,
        id: documentId,
        documentId,
        authors: input.authors,
        createdAt: Date.now(),
        files: input.files?.map(file => {
            if (!file.url.startsWith('http')) {
                file.url = opts?.assetUrl + file.url;
            }
            return { type: 'image', ...file };
        }) || [],
        index: {
            index: parts.filter(p => p.partId !== 'title').map(p => p.partId)
        },
        metaData: input.metaData,
        parts: parts.filter(p => p.partId !== 'title'),
        title: parts.find(p => p.partId === 'title') as DocumentSnapshotPart,
        references: input.references,
        locale,
    };

    return data;
}

export const extractCaptionOrTitle = (node: DocumentNode<any>, parent?: DocumentNode<any>): string | undefined => {
    switch (node.type) {
        case SFNodeType.heading:
            return getTextContent(node);
        case SFNodeType.figure:
            {
                const caption = node?.content?.find(n => n.type === 'caption');
                if (caption?.content && caption.content?.length > 0) {
                    return getTextContent(caption.content[0]);
                }
            }
        case SFNodeType.table:
            {
                const caption = parent?.content?.find(n => n.type === 'caption');
                if (caption?.content && caption.content?.length > 0) {
                    return getTextContent(caption.content[0]);
                }
            }
    }

}

export const escapeId = (s: string) => s.replace(/[^a-zA-Z0-9]/g, '');

export const extractFirstHeading = (doc: DocumentNode<SFNodeType.document>, engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>): string | undefined => {
    const heading = doc?.content && doc?.content.length > 0 ? doc.content[0] : undefined;
    return extractTitleFromHeading(heading as HeadingDocumentNode);
}

export const extractTitleFromHeading = (heading: HeadingDocumentNode) => {
    if (heading?.type === SFNodeType.heading) {
        const titleNode = heading.content && heading.content[0];
        return titleNode?.text || titleNode?.content?.map((n) => n.text).join('');
    }
}

export const extractHeadingTitle = async (heading: HeadingDocumentNode, engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>): Promise<{ title?: string; titleHTML?: string; }> => {
    if (heading.type !== SFNodeType.heading) { throw new Error('Must provide heading for text extraction'); }
    const title = heading.content?.map((n) => n.text).join('');
    // if we have an engine, render the node to get a more complete string
    if (engine && heading.content) {
        const renderedHeading = await engine.renderContent(heading);
        return {
            title,
            titleHTML: (<span>{...renderedHeading}</span>).innerHTML
        }
    }

    return {
        title,
        titleHTML: undefined
    };
}

export const extractTitle = (doc: DocumentNode<SFNodeType.document>): string | undefined => {
    const header = doc?.content && doc?.content.length > 0 ? doc.content[0] : undefined;
    if (header?.type === 'header') {
        const titleNode = header.content && header.content[0];
        if (titleNode) {
            return titleNode.content?.map((n) => n.text).join('');
        }
    }
}

/**
 * Wraps the toc listing into a sectioned structure.
 */
export const wrapTOC = (headings: { type: 'heading', id: string; level: number; numberingString: string; title: string; partId?: string; }[], level = 1) => {

    let sections: any[] = [];
    let section: any = undefined;
    let i = 0;
    while (i < headings.length) {
        let heading = headings[i];

        if (heading.level === level) {
            section = {
                ...heading,
                content: []
            };
            sections.push(section);
        } else if (heading.level && heading.level > level) {
            let nextHeading;
            if (!section) {
                section = {
                    level,
                    content: []
                };
                sections.push(section);
            }
            nextHeading = headings.findIndex((n, index) => index > i && n.level <= heading.level);
            if (heading.level === level + 1) {
                section.content?.push(...wrapTOC(headings.slice(i, nextHeading === -1 ? headings.length : nextHeading), heading.level));
            } else {
                section.content?.push(...wrapTOC(headings.slice(i, nextHeading === -1 ? headings.length : nextHeading), level + 1));
            }
            i = nextHeading === -1 ? headings.length : nextHeading - 1;
        } else {
            //
        }
        i++;
    }

    return sections;
}

/**
 * Wraps the text into parts (one per h1) with an otherwise linear document inside (e.g. no sub-sections).
 * If the heading 1 node attributes contain meta data it will be transfered into the parts.
 * @param nodes
 */
export const createParts = (nodes: DocumentNode<any>[]): DocumentSnapshotPart[] => {
    const parts: DocumentSnapshotPart[] = [];
    let activePart;
    for (let node of nodes) {
        if (node.type === SFNodeType.header) {
            parts.push({
                partId: 'title',
                type: 'title',
                schema: 'title',
                document: {
                    type: SFNodeType.document,
                    attrs: { type: 'chapter' },
                    content: node.content
                }
            });
        } else if (node.type === SFNodeType.heading && node.attrs?.level === 1) {
            activePart = {
                partId: node.attrs.id || createId(),
                type: node.attrs.type || 'chapter',
                schema: node.attrs.schema || 'chapter',
                numbering: node.attrs.numbering,
                role: node.attrs.role,
                document: {
                    type: SFNodeType.document,
                    attrs: { type: 'chapter' },
                    content: [node] // add the heading
                }
            };
            parts.push(activePart);
        } else {
            // add to the current part
            if (!activePart) {
                // create an empty part if the document did not begin with a heading 1
                activePart = {
                    partId: 'chapter',
                    type: 'chapter',
                    schema: 'chapter',
                    document: {
                        type: SFNodeType.document,
                        attrs: { type: 'chapter' },
                        content: []
                    }
                };
                parts.push(activePart);
            }

            activePart.document.content.push(node);
        }

    }

    return parts;
};

/**
 * Wraps text into section nodes using headings.
 * This can be used to render documents that require a strict hierarchy like JATS XML.
 * @param nodes the node list
 * @param level the base level the sections start at
 */
export const createSectionHierarchy = (nodes: DocumentNode<any>[], level = 1) => {
    const sections: DocumentNode<SFNodeType.section>[] = [];

    // we will never modify the original array of nodes
    let nodeList = Object.freeze([...nodes.map(n => Object.freeze(n))]).map(node => {
        if (typeof node.attrs?.level === 'string') { node.attrs.level = Number.parseInt(node.attrs.level); }
        return node;
    });

    // a section at the level handed to the function
    let section: DocumentNode<SFNodeType.section> | undefined;

    let i = 0;
    while (i < nodeList.length) {
        let currentNode = nodeList[i];
        if (currentNode.type === SFNodeType.heading) {
            // we start a new base level section and wrap all children inside it
            if (currentNode.attrs.level === level) {
                // create a new wrapper section with the current heading node
                section = {
                    type: SFNodeType.section,
                    attrs: {
                        id: 's-' + (currentNode.attrs?.id || 'not-unique'),
                        level
                    },
                    content: [currentNode]
                }
                sections.push(section);
            } else {
                // a heading at a different level ()
                const difference = currentNode.attrs?.level - level;
                let nextSectionIndex, subsecs;

                if (!section) {
                    section = {
                        type: SFNodeType.section,
                        attrs: {
                            id: 's-v-' + nodes[0]?.attrs.id,
                            level
                        },
                        content: []
                    };
                    sections.push(section);
                }

                if (difference > 1) {
                    // we jumped heading levels (e.g. from 1 to 3). This does happen so we just wrap the heading onto a section
                    // instead  of calling wrap sections with the node's heading level we use level + 1 to nest sections until we reach the right level
                    nextSectionIndex = nodeList.findIndex((n, index) => index > i && n.type === SFNodeType.heading && n.attrs?.level <= currentNode.attrs?.level);
                    // set the index to the end if no other sections are following
                    if (nextSectionIndex === -1) { nextSectionIndex = nodeList.length; }
                    subsecs = createSectionHierarchy(nodeList.slice(i, nextSectionIndex), level + 1);
                } else {
                    // we found a sub-section at level currentNode.attrs?.level
                    // find the next higher up heading (excluding the current heading at i)
                    nextSectionIndex = nodeList.findIndex((n, index) => index > i && n.type === SFNodeType.heading && n.attrs?.level <= currentNode.attrs?.level);
                    // set the index to the end if no other sections are following
                    if (nextSectionIndex === -1) { nextSectionIndex = nodeList.length; }
                    // get all nodes that should be part of this sub-section
                    const no = nodeList.slice(i, nextSectionIndex);
                    // push the sub-sections
                    subsecs = createSectionHierarchy(no, currentNode.attrs?.level);
                }

                section?.content?.push(...subsecs);
                // move the index before the next heading node we should process (since i is incremented at the end)
                i = nextSectionIndex - 1;
            }
        } else {
            // process non-heading nodes
            // we found a node in the beginning that is not a heading, so we create an empty section for it
            if (!section) {
                section = {
                    type: SFNodeType.section,
                    attrs: {
                        id: 's-' + nodes[0]?.attrs.id,
                        level
                    },
                    content: []
                }
                sections.push(section);
            }

            section.content?.push(currentNode);
        }
        i++;
    }
    return sections;
}

/** Incomplete mapping from a character to their unicode superscript or subscript character */
const supSubMapping = {
    '0': { sup: '\u2070', sub: '\u2080' },
    '1': { sup: '\u00B9', sub: '\u2081' },
    '2': { sup: '\u00B2', sub: '\u2082' },
    '3': { sup: '\u00B3', sub: '\u2083' },
    '4': { sup: '\u2074', sub: '\u2084' },
    '5': { sup: '\u2075', sub: '\u2085' },
    '6': { sup: '\u2076', sub: '\u2086' },
    '7': { sup: '\u2077', sub: '\u2087' },
    '8': { sup: '\u2078', sub: '\u2088' },
    '9': { sup: '\u2079', sub: '\u2089' },
    'a': { sup: '\u1d43', sub: '\u2090' },
    'b': { sup: '\u1d47', sub: undefined },
    'c': { sup: '\u1d9c', sub: undefined },
    'd': { sup: '\u1d48', sub: undefined },
    'e': { sup: '\u1d49', sub: '\u2091' },
    'f': { sup: '\u1da0', sub: undefined },
    'g': { sup: '\u1d4d', sub: undefined },
    'h': { sup: '\u02b0', sub: '\u2095' },
    'i': { sup: '\u2071', sub: '\u1d62' },
    'j': { sup: '\u02b2', sub: '\u2c7c' },
    'k': { sup: '\u1d4f', sub: '\u2096' },
    'l': { sup: '\u02e1', sub: '\u2097' },
    'm': { sup: '\u1d50', sub: '\u2098' },
    'n': { sup: '\u207f', sub: '\u2099' },
    'o': { sup: '\u1d52', sub: '\u2092' },
    'p': { sup: '\u1d56', sub: '\u209a' },
    'r': { sup: '\u02b3', sub: '\u1d63' },
    's': { sup: '\u02e2', sub: '\u209b' },
    't': { sup: '\u1d57', sub: '\u209c' },
    'u': { sup: '\u1d58', sub: '\u1d64' },
    'v': { sup: '\u1d5b', sub: '\u1d65' },
    'w': { sup: '\u02b7', sub: undefined },
    'x': { sup: '\u02e3', sub: '\u2093' },
    'y': { sup: '\u02b8', sub: undefined },
    'A': { sup: '\u1d2c', sub: undefined },
    'B': { sup: '\u1d2e', sub: undefined },
    'D': { sup: '\u1d30', sub: undefined },
    'E': { sup: '\u1d31', sub: undefined },
    'G': { sup: '\u1d33', sub: undefined },
    'H': { sup: '\u1d34', sub: undefined },
    'I': { sup: '\u1d35', sub: undefined },
    'J': { sup: '\u1d36', sub: undefined },
    'K': { sup: '\u1d37', sub: undefined },
    'L': { sup: '\u1d38', sub: undefined },
    'M': { sup: '\u1d39', sub: undefined },
    'N': { sup: '\u1d3a', sub: undefined },
    'O': { sup: '\u1d3c', sub: undefined },
    'P': { sup: '\u1d3e', sub: undefined },
    'R': { sup: '\u1d3f', sub: undefined },
    'T': { sup: '\u1d40', sub: undefined },
    'U': { sup: '\u1d41', sub: undefined },
    'V': { sup: '\u2c7d', sub: undefined },
    'W': { sup: '\u1d42', sub: undefined },
    '+': { sup: '\u207A', sub: '\u208A' },
    '-': { sup: '\u207B', sub: '\u208B' },
    '=': { sup: '\u207C', sub: '\u208C' },
    '(': { sup: '\u207D', sub: '\u208D' },
    ')': { sup: '\u207E', sub: '\u208E' }
};

/** Takes a string and removes special characters
 * Never starts with a number to be a valid XML attribute.
 */
export const slugify = (title: string) => {
    // slug courtesy of https://gist.github.com/mathewbyrne/1280286
    return title.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')            // Trim - from end of text
        .replace(/^[0-9]+/, '_')
};

/**
 * Transforms a string to super/subscript characters.
 * Characters that do not have super/subscript remain the same.
 * @param s the string
 * @param type sup(erscript) or sub(script)
 * @returns the transformed string
 */
export const toSupSubScript = (s: string, type: 'sup' | 'sub'): string => {
    if (!s || typeof s !== 'string') { return s; }
    let superscripted = '';
    for (let i = 0; i < s.length; i++) {
        const char = s[i];
        superscripted = supSubMapping[char]?.[type] ? supSubMapping[char]?.[type] : char;
    }

    return superscripted;
}

/** Takes a country code and returns the localized name. */
export const countryToLabel = (countryCode: 'DE' | string, locale = 'en') => {
    try {
        // @ts-ignore TS2339: Property 'DisplayNames' does not exist on type 'typeof Intl'
        const regionNamesInEnglish = new Intl.DisplayNames([locale], { type: 'region' });
        return regionNamesInEnglish.of(countryCode) || countryCode;
    } catch (e) {
        console.error(e);
        return countryCode;
    }
}