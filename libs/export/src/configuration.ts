import { existsSync, readFileSync } from 'fs';
import yaml from 'js-yaml';

export const readConfiguration = (path: string): any[] => {
  if (!existsSync(path)) { return []; }

  try {
    const file = readFileSync(path, 'utf-8');
    const configurations = file.split('---').map(configuration => yaml.load(configuration));
    return configurations;
  } catch (e) {
    console.error(e);
    return [];
  }
}
