import { JSDOM } from 'jsdom';

const dom = new JSDOM();
const document = dom.window.document;

/**
 * JSX factory used to transpile to HTML by default.
 * Must be included on any pages using html markup directly within a tsx file.
 */
export namespace TSX {

    export function createTextNode(s: string) {
        return document.createTextNode(s);
    }

    /**
     * Takes a HTML string and returns a HTML element.
     */
    export function htmlStringToElement(s: string): HTMLElement | null {
        if (!s || s.length === 0) {
            return null;
        }

        try {
            const div = document.createElement('div');
            div.innerHTML = s;
            return div.firstChild as HTMLElement;
        } catch (e) {
            console.error('Could not transform HTML string', s, e.message);
        }
        return null;
    }

    interface AttributeCollection {
        [name: string]: string;
    }

    export function serialize(element) {
        return element.outerHTML;
    }

    export function createElement(tagName: string, attributes: AttributeCollection | null, ...children: any[]): Element {
        let element = document.createElement(tagName);

        if (attributes) {
            for (let key of Object.keys(attributes)) {
                // workaround to get xml tags working as well
                element.setAttribute(key.replace('_', ':'), attributes[key]);
            }
        }

        for (let child of children) {
            appendChild(element, child);
        }

        return element;
    }

    function appendChild(parent: any, child: any) {
        if (child == null) return;

        if (parent?.constructor?.name?.startsWith('XML')) {
            throw new Error('Parent element may not be XML but was ' + parent?.constructor?.name);
        }

        if (child?.constructor?.name?.startsWith('XML')) {
            // try to parse the XML as HTML
            child = htmlStringToElement(child.toString());
        }

        try {
            if (typeof child === "string") {
                parent.appendChild(document.createTextNode(child));
            } else if (typeof child === "number") {
                parent.appendChild(document.createTextNode(`${child}`));
            } else if (Array.isArray(child)) {
                child.filter(c => c != null).forEach(c => {
                    if (typeof c === 'string') {
                        return parent.appendChild(document.createTextNode(c));
                    } else if (typeof c === 'number') {
                        return parent.appendChild(document.createTextNode(`${c}`));
                    }

                    try {
                        parent.appendChild(c)
                    } catch (e) {
                        console.error('Could not append child: ' + e.message, c);
                        throw e;
                    }
                });
            } else {
                parent.appendChild(child);
            }
        } catch (e) {
            console.log(parent, child);
            console.error(e);
            debugger;
            throw e;
        }
    }
}