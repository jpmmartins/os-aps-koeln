import sass, { FileImporter, SassString } from 'sass';
import { existsSync } from 'fs';
import { pathToFileURL } from 'url';

/** Gets a value from an object if provided with a path (e.g. object.key.key2 ) */
const getValue = (path: string, object: any) => {
    if (!path) { return undefined; }
    if (object[path]) { return object[path]; }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object[segments[0]] === undefined) { return undefined; }
    return getValue(segments.slice(1, segments.length).join('.'), object[segments[0]]);
}

export class FindFiles implements FileImporter<'sync'> {
    findFileUrl(url: string, options: { fromImport: boolean; }): URL | null {
        if (url.indexOf('@sciflow/tdk') === 0) {
            const resourceUrl = pathToFileURL('node_modules/@sciflow/tdk' + url.replace('@sciflow/tdk', ''));
            const resourceUrl3 = pathToFileURL('../node_modules/@sciflow/tdk' + url.replace('@sciflow/tdk', ''));
            const resourceUrl2 = pathToFileURL('../../node_modules/@sciflow/tdk' + url.replace('@sciflow/tdk', ''));
            if (existsSync(resourceUrl)) {
                return resourceUrl;
            }

            if (existsSync(resourceUrl2)) {
                return resourceUrl2;
            }

            if (existsSync(resourceUrl3)) {
                return resourceUrl3;
            }

            console.error('Could not resolve scss file', url);

            return null;
        }
        return null;
    }
}

export const renderFragment = (styles: string) => {
    return sass.compileString(styles, { style: 'expanded' });
};

export const renderCss = async (file: string, { document, configurations, metaData }): Promise<{
    styles: sass.CompileResult,
    importers: any,
    functions: any
}> => {
    // FIXME #47 make meta data available
    
    const importers = [new FindFiles()];
    const functions: any = {
        'getConf($key, $defaultValue: null, $kind: Configuration)': ([key, defaultValue, kind]) => {
            const configuration = configurations?.find(c => c.kind === kind.text);
            if (!configuration) { return defaultValue; }
            const lookup = key.toString()?.replace(/"/gi, '',);
            const value = getValue(lookup, configuration);
            // TODO: we should make a difference between undefined and purposefully set to null
            if (!value) { return defaultValue; }
            // TODO we should respect the value type
            const retval = new SassString(value);
            return retval;
        },
        'getDocumentValue($key)': ([key]) => {
            const keyValue = (key as SassString).toString();
            if (!document[keyValue]) {
                debugger;
                throw new Error('No value for ' + keyValue);
            }
            const retval = new SassString(document[keyValue]);
            return retval;
        }
    };

    try {
        const styles = sass.compile(file, {
            importers,
            functions
        });

        return {
            styles,
            importers,
            functions
        }
    } catch (e) {
        console.error('Could not compile sass', { message: e.message, file });
        throw new Error('Could not compile sass');
    }
};
