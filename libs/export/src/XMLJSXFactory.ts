import { create } from 'xmlbuilder2';
import { XMLBuilderImpl } from 'xmlbuilder2/lib/builder';

export namespace XMLJSXFactory {
    interface AttributeCollection {
        [name: string]: string;
    }

    export function stringToElement(s: string) {
        return create(s);
    }

    export function escapeString(str) {
        str = str
            .replaceAll(new RegExp(String.fromCharCode(160), 'g'), ' ')
            .replaceAll(/ /g, ' ')
            .replaceAll(/"/g, '&quot;')
            .replaceAll(/'/g, '&apos;')
            .replaceAll(/</g, '&lt;')
            .replaceAll(/>/g, '&gt;')
            .replaceAll(/&/g, '&amp;')
            .replaceAll(/\f/g, '')
            .replaceAll(/\r/g, '')
            .replaceAll(/\n/g, '')
        return str;
    }

    export function createElement(tagName: string, attributes: AttributeCollection | null, ...children: any[]): XMLBuilderImpl {
        if (!attributes) { attributes = {}; }

        const element = create().ele(tagName.replace('_', ':'));

        if (attributes) {
            for (let attr of Object.keys(attributes)) {
                element.att(attr.replace('_', ':'), attributes[attr]);
            }
        }

        for (let child of children) {
            appendChild(element, child);
        }

        return element as XMLBuilderImpl;
    }

    function appendChild(parent: any, child: any) {
        if (child == null) return;

        if (parent?.constructor?.name?.startsWith('HTML')) {
            throw new Error('Parent element may not be HTML but was ' + parent?.constructor?.name);
        }

        if (child?.constructor?.name?.startsWith('HTML')) {
            // try to parse the HTML as XML
            child = stringToElement(child.outerHTML);
        }

        try {
            if (typeof child === 'string' || typeof child === 'number') {
                parent.txt(`${child}`);
            } else if (Array.isArray(child)) {
                child.filter(c => c != null).forEach(c => appendChild(parent, c));
            } else {
                parent.import(child);
            }
        } catch (e) {
            console.error('Could not append child', e.message, { parent, child });
            debugger;
            throw e;
        }
    }
}
