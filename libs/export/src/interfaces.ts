import { DocumentNode, Author, SFNodeType, DocumentSnapshotPart, DocumentSnapshotResource, DocumentSnapshotImage, counterStyle } from '@sciflow/schema';
import { ReferenceRenderer, CSLReference } from "@sciflow/cite";
import { TemplateComponent } from './component';
import { NodeRenderer } from './public-api';

export interface DocumentPartMeta {
    id: string;
    title?: string;
    type?: string;
    role?: string;
    numbering?: string;
}

export interface TocHeading {
    /** The title as a plain text */
    title?: string;
    /** A HTML reprentation fot he title, possibly including sup, sub, i and b elements next to spans. */
    titleHTML?: string;
    id: string;
    level: number;
    counterStyle: counterStyle;
    /** The string that contains the prefix and numbering of
     *  the heading: e.g. 1.1.1 where 1.1 is the prefix and 
     *  the heading is the first sub-sub-chapter, so 1*/
    numberingString: string;
    /** The document part id for the heading */
    partId?: string;
}

export interface Toc {
    headings: TocHeading[];
}

export interface DocumentTables {
    toc?: any;
    headings: DocumentElement<SFNodeType.heading>[];
    /** A list of all equations in the document  */
    equations: DocumentElement<SFNodeType.math>[];
    /** A list of image figures */
    figures: DocumentElement<SFNodeType.figure>[];
    footnotes: DocumentElement<SFNodeType.footnote>[];
    /** A list of all tables in the document  */
    tables: DocumentElement<SFNodeType.table>[];
     /** A list of all code blocks in the document  */
    codeBlocks: DocumentElement<SFNodeType.code>[];
    /** A list of all document parts */
    parts: DocumentPartMeta[];
    /** A list of all citations */
    citations: DocumentElement<SFNodeType.citation>[];
    ids: any[];
    idMap: {};
}

export interface SciFlowDocumentData {
    citationRenderer: ReferenceRenderer,
    templateOptions?: any;
    document: DocumentData;
    virtualDocument: Document;
    metaData?: any;
    metaDataSchema?: any;
    configurations?: any[];
}

export interface DocumentData {
    documentId: string;
    index: any;
    titlePart: any;
    title: string | undefined;
    metaData: any | undefined;
    subtitle: string | undefined;
    files: DocumentSnapshotResource[];
    parts: any[];
    images: DocumentSnapshotImage[];
    authors: Author[];
    /** Listings of document elements like headings, equations, footnotes, .. */
    tables: DocumentTables;
    /** The document annotations (only if specifically requested) */
    annotations?: any[];
    references: any[];
    wordCount?: { total?: number; };
}

export interface DocumentElement<T> {
    id: string;
    type: T,
    title?: string;
    attrs?: any;
    /** A document part this element may be part of */
    partId?: string;
    /** A figure this element may be part of */
    figureId?: string;
    content?: DocumentNode<any>[];
}

/**
 * The data handed to a template for rendering for documents with one text body (e.g. as created with OS-APS).
 * For more complex multi-part documents @see DocumentSnapshot
 */
export interface SingleDocumentExportData {
    title?: string;
    document: DocumentNode<SFNodeType.document>,
    authors: Author[],
    /** A list of styles to include with the position where they should be rendered (e.g. as a last style or as a first one) */
    stylePaths?: { path: string; position?: 'start' | 'end' }[];
    references: CSLReference[],
    configuration?: any,
    configurations?: any[],
    templateOptions: any,
    /** A JSON meta data object (created by a user modifying settings) */
    metaData?: unknown;
    /** A JSON meta data schema (possibly including default values) */
    metaDataSchema?: any;
    files?: { id: string; url: string; }[];
    citationStyleData: { styleXML: string; localeXML: string; }
}

export interface ExportOptions {
    /** The configuration YAML objects for each kind (also @see customTemplateComponents) */
    configurations: any[];
    /** The meta data set in the document */
    metaData: any;
    /** The JSON meta data schema (including defaults) */
    metaDataSchema: any;
    /** An object containing custom renderers */
    customRenderers: { [nodeType: string]: NodeRenderer<any, SciFlowDocumentData> };
    /** An array containing custom components to render a kind in order (last overwrites previous) */
    customTemplateComponents: TemplateComponent<any, SciFlowDocumentData>[];
    /** Paths to additional scss files */
    stylePaths: {
        path: string;
        /** Start puts the style to the very start of the template, end overwrites previous styles */
        position?: 'start' | 'end'
    }[];
    /** Whether to render inline where possible (i.e. styles, scripts, etc.). Images may be linking to an external URL. */
    inline?: boolean;
    scripts: { src?: string; content?: string; }[];
    /** An indicator as to what the output will be processed as (e.g. raw, princexml, pagedjs, ..) */
    runner?: string;
    /** A host to set as a base href for inlined image/font/resource paths that are not absolute */
    baseHref?: string;
}
