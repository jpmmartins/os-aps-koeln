import { join, resolve } from 'path';
import { sync } from 'glob';
import * as TJS from 'typescript-json-schema';

/**
 * Extracts the JSON schema from interfaces used to configure components. Files must be named *.schema.ts(x)
 * @param kinds 
 * @param componentPath the path to the component implementations
 * @returns 
 */
export const getComponentSchema = (kinds = [], configurations: any[], componentPath = join(__dirname, `libs/export/src/html`)) => {
  const files = sync(`${componentPath}/@(components|renderers)/**/*.schema.@(ts|tsx)`);
  if (files.length === 0) {
    console.log('No component schemas found', componentPath);
  } else {
    console.log('Found schemas for ' + files.length + ' components');
  }

  // optionally pass ts compiler options
  const compilerOptions: TJS.CompilerOptions = {
    strictNullChecks: true,
    lib: ['es2020', 'ES2021', 'dom'],
    skipLibCheck: true,
    exclude: [
      './node_modules/*',
      '**/node_modules/*'
    ]
  };

  try {
    console.time('generate configuration schema');
    const program = TJS.getProgramFromFiles(
      files.map(path => resolve(path)),
      compilerOptions
    );
    
    // We can either get the schema for one file and one type...
    const schema = TJS.generateSchema(program, "*", { tsNodeRegister: true });
    if (!schema && files.length > 0) {
      console.warn('Did not generate a schema even though ' + files.length + ' schema files were present.');
    }

    const definitions: any = {};
    for (let kind of kinds) {
      try {
        const configuration = configurations.find(config => config.kind === kind);
        const definition = (schema?.definitions || {})[`${kind}Configuration`];
        if (!definition) { continue; }
        definitions[`${kind}Configuration`] = setDefaults(definition, configuration.spec);
      } catch (e) {
        throw new Error('Could not read component schema for ' + kind + ': ' + e.message);
      }
    }
    
    const metaData = {
      type: 'object',
      title: 'Configuration settings',
      properties: {
        ...definitions
      }
    };
    
    console.timeEnd('generate configuration schema');
    return metaData;

  } catch (e) {
    throw new Error('Could not generate schema at ' + componentPath + ': ' + e.message);
  }
}

const setDefaults = (schema, values) => {
  if (!values) { return schema; }

  if (schema?.properties) {
    for (let prop of Object.keys(schema.properties)) {
      schema.properties[prop] = setDefaults(schema.properties[prop], values[prop]);
    }
  }

  if (schema.type === 'string' || schema.type === 'number' && values) {
    schema.default = values;
  }

  return schema;
}