import { JSDOM } from 'jsdom';
import { TSX } from './TSX';

const dom = new JSDOM();
const virtualDocument = dom.window.document;

import { CitationRenderer } from './html/citation-renderer';
import { DocumentNode } from '@sciflow/schema';

export interface Renderer<OutputFormat> {
    engine?: RenderingEngine<OutputFormat, any>;
    render(node: DocumentNode<any>, parent?: DocumentNode<any>): Promise<OutputFormat>;
}

/**
 * An abstract renderer that takes a document node and returns the results as OutputFormat.
 */
export abstract class NodeRenderer<OutputFormat, DataFormat> implements Renderer<OutputFormat> {
    _engine: RenderingEngine<OutputFormat, DataFormat>;

    get engine(): RenderingEngine<OutputFormat, DataFormat> {
        return this._engine;
    }

    constructor(engine: RenderingEngine<OutputFormat, DataFormat>) {
        this._engine = engine;
    }

    public abstract render(node: DocumentNode<any>, parent?: DocumentNode<any>): Promise<OutputFormat>;
}

export type RendererPlugin<OutputFormat, DataFormat> = ((node: DocumentNode<any>, element: OutputFormat, engine: RenderingEngine<OutputFormat, DataFormat>) => Promise<OutputFormat>)

/**
 * Rendering engine that returns as OutputFormat.
 */
export class RenderingEngine<OutputFormat, DataFormat> {
    renderers: { [key: string]: Renderer<OutputFormat> } = {};
    readonly data: DataFormat;
    plugins: RendererPlugin<OutputFormat, DataFormat>[];
    engineLog: { type: string; message: string; data?: any }[] = [];

    get logs() {
        return this.engineLog;
    }

    constructor(
        renderers: { [key: string /** stay open to undefined types from plugins */]: any },
        data?: DataFormat,
        plugins = []
    ) {
        this.data = data || {} as any;
        this.plugins = plugins;

        for (let key of Object.keys(renderers)) {
            if (typeof renderers[key] !== 'number') {
                if (!renderers[key]) {
                    throw new Error('Renderer must be defined or set to a default (' + key + ')');
                }
                try {
                    this.renderers[key] = new renderers[key](this);
                } catch (e) {
                    throw new Error(key + ':' + e.message);
                }
            } else {
                this.renderers[key] = renderers[key];
            }
        }
    }

    /** Logs an error and retains it. */
    log(type: 'info' | 'warning' | 'error' = 'warning', message: string, data?: any) {
        this.engineLog.push({ type, message, data });
    }

    /** Renders a node using the supplied renderers. */
    async render(node: DocumentNode<any>, parent?: DocumentNode<any>): Promise<OutputFormat> {
        if (node == null) { throw new Error('Node may not be undefined'); }
        if (!node?.type) { throw new Error('Node type must be defined'); }
        if (this.renderers[node.type] == null) { throw new Error('No renderer for ' + node.type); }
        
        // FIXME check this.renderers[node.type] instanceof NodeRenderer
        if (typeof this.renderers[node.type]?.render === 'function') {
            const renderer = this.renderers[node.type] as Renderer<OutputFormat>;
            let renderedElement = await renderer.render(node, parent);
         
            for (let plugin of this.plugins) {
                try {
                    renderedElement = await plugin(node, renderedElement, this);
                } catch (e: unknown) {
                    if (e instanceof Error) {
                        throw new Error(`Plugin failed: ${e?.message}`);
                    }
                }
            }

            return renderedElement;
        } else {
            throw new Error('Renderer for ' + node.type + ' was not a valid renderer');
        }
    }

    /**
     * Renders the node content only.
     */
    async renderContent(node: DocumentNode<any>): Promise<OutputFormat[]> {
        if (node == null) { throw new Error('Node may not be undefined'); }
        let content: OutputFormat[] = [];
        if (node.content) {
            for (let child of node.content) {
                const renderResult = await this.render(child, node);
                if (Array.isArray(renderResult)) {
                    content = [...content, ...renderResult.flat()];
                } else {
                    content.push(renderResult);
                }
            }
        }

        return content;
    }
}
