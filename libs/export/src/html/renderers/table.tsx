import { TSX } from '../../TSX';

import { SciFlowDocumentData } from "../../interfaces";
import { DocumentNode, SFNodeType } from '@sciflow/schema';
import { NodeRenderer } from "../../renderers";

class TableRenderer extends NodeRenderer<HTMLTableElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, parent?: DocumentNode<SFNodeType.figure>) {
        if (!node.content) {
            return <table></table>;
        }

        const colcount = node.content.length > 0 && node.content[0].content && node.content[0].content.length || 0;

        const indexOfFirstRow = node.content.findIndex(row => row.content?.some(n => n.type !== SFNodeType.table_header));
        const headers = node.content.slice(0, indexOfFirstRow);
        const rows = node.content.slice(indexOfFirstRow, node.content.length);
        const headersRendered = await this.engine.renderContent({ ...node, content: headers });
        const rowsRendered = await this.engine.renderContent({ ...node, content: rows });

        const captionWrapper = parent?.content?.find(node => node?.type === SFNodeType.caption);
        let captionText: HTMLElement[] = [];
        if (captionWrapper) {
            const caption = captionWrapper.content && captionWrapper.content[0];
            if (caption) {
                captionText = await this.engine.renderContent(caption);
            }
        }
        
        // only get tables that have a caption
        const tables = this.engine.data.document?.tables?.tables.filter(table => table?.title?.length != undefined && table?.title?.length > 0);
        const index = tables?.findIndex(t => t.id === node?.attrs?.id);
        const table = <table id={'tbl-' + node?.attrs?.id}>
            {(index > -1 && captionText?.length > 0) ? <caption data-table-number={index > -1 ? index + 1 : null}>{...captionText}</caption> : ''}
            {headers.length > 0 ? <thead>
                {...headersRendered}
            </thead> : ''}
            <tbody>
                {...rowsRendered}
            </tbody>
        </table> as unknown as HTMLTableElement;

        return table;
    }
}

class TableCellRenderer extends NodeRenderer<HTMLTableCellElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table_cell | SFNodeType.table_header>) {

        const children = await this.engine.renderContent(node);
        const align = node.content?.find(({ attrs }) => attrs && attrs['text-align'])?.attrs['text-align'];
        let htmlEl = <td>{...children}</td> as unknown as HTMLTableCellElement;
        if (node.type === SFNodeType.table_header) {
            htmlEl = <th>{...children}</th> as unknown as HTMLTableCellElement;
            // htmlEl.setAttribute('custom-style', 'Table Heading');
        }
        if (align) {
            htmlEl.setAttribute('style', `text-align: ${align}`);
        }

        if (node.attrs && node.attrs.colwidth) {
            htmlEl.setAttribute('width', node.attrs.colwidth);
        }

        if (node.attrs && node.attrs.colspan) {
            htmlEl.setAttribute('colspan', node.attrs.colspan);
        }

        if (node.attrs && node.attrs.rowspan) {
            htmlEl.setAttribute('rowspan', node.attrs.rowspan);
        }

        return htmlEl;
    }
}

class TableRowRenderer extends NodeRenderer<HTMLTableRowElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table_row>, _parent?: DocumentNode<any>) {
        const children = await this.engine.renderContent(node);

        return <tr>
            {...children}
        </tr>;
    }
}

const renderers: { [key: string]: any } = {
    [SFNodeType.table]: TableRenderer,
    [SFNodeType.table_row]: TableRowRenderer,
    [SFNodeType.table_cell]: TableCellRenderer,
    [SFNodeType.table_header]: TableCellRenderer
};

export default renderers;