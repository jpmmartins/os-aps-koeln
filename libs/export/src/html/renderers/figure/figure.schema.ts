/**
 * Figure renderer schema with the basic shape of configurations handed to the figure renderer.
 */

/**
* @title General figure settings
*/
export interface FigureRendererConfiguration {
    /**
     * @title Caption font size
     * @default 9pt
     */
    captionFontSize: string,
    /**
     * @title Table options
     */
    table: {
         /**
         * @title Table caption position
         * @default above
         */
        captionSide: 'above' | 'below';
        /**
         * @title Table label
         * @default Table
         */
        label: string;
        /**
         * @title Table reference label
         * @default tbl.
         */
        referenceLabel: string;
    };
    /**
     * @title Image options
     */
    image: {
        /**
         * @title Image caption position
         * @default above
         */
        captionSide: 'above' | 'below';
        /**
         * @title Image label
         * @default Figure
         */
        label: string;
        /**
         * @title Image reference label
         * @default fig.
         */
        referenceLabel: string;
    };
    /**
     * @title Code options
     */
    code: {
       /**
         * @title Code caption position
         * @default above
         */
       captionSide: 'above' | 'below';
       /**
        * @title Code label
        * @default Code
        */
       label: string;
       /**
        * @title Code reference label
        * @default cod.
        */
       referenceLabel: string;
    };
    /**
     * @title Equation options
     */
    equation: {
        /**
         * @title Equation number position
         * @default right
         */
        captionSide: 'none' | 'right';
        /**
         * @title Equation label
         * @default Figure
         */
        label: string;
        /**
         * @title Equation reference label
         * @default eq.
         */
        referenceLabel: string;
        /**
         * @title Render MathML instead of a Mathjax svg
         * @default false
         */
        renderMathML: boolean;
    };
    /**
     * @title Inline captions
     * @description Whether captions should be included in the output (and not styling). (needed for EPUB)
     * @default false
     */
    renderCaptionsInline: boolean;
}