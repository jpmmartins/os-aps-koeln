import { TSX } from '../../../TSX';

import { SciFlowDocumentData } from "../../../interfaces";
import { SFNodeType, DocumentNode } from '@sciflow/schema';
import { NodeRenderer } from "../../../renderers";

import { Figure } from '../../components/figure/figure.component';
import { FigureRendererConfiguration } from './figure.schema';

class FigureRenderer extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, parent: DocumentNode<SFNodeType.figure>) {

        // we are importing the default implementation for a figure component here. Be aware that this might be different,
        // for some templates. So we cannot rely on the shape of the data provided.
        const figureComponent = new Figure(this.engine);
        const configuration: FigureRendererConfiguration = figureComponent.getValues();

        const data = this.engine.data.document;
        const extractTitleFromCaption = async (captionNode): Promise<string | null> => {
            try {
                if (!captionNode?.content) { return ''; }
                if (captionNode.content && captionNode.content.length > 0) {
                    const label = await this.engine.render(captionNode.content[0]);
                    if (!label) { return null; }
                    return label.innerHTML;
                }
            } catch (e) {
                console.error(e);
            }
            return null;
        };

        let image = this.engine.data.virtualDocument.createTextNode('');
        let figcaption = this.engine.data.virtualDocument.createTextNode('');

        const captionNode = node.content?.find(n => n.type === 'caption');

        let label, labelNode;
        let notes = this.engine.data.virtualDocument.createTextNode('');
        const noteList: HTMLElement[] = [];

        if (captionNode !== undefined) {
            label = await extractTitleFromCaption(captionNode);
            if (captionNode.content) {
                labelNode = captionNode?.content?.length > 0 ? captionNode.content[0] : null;
                if (captionNode?.content?.length > 1) {
                    for (let child of captionNode.content.slice(1, captionNode.content.length)) {
                        noteList.push(await this.engine.render(child, captionNode));
                    }
                    notes = <div data-role="notes">{...noteList}</div>;
                }
            }
        }

        let content: HTMLElement[] = [];
        // filter out caption
        if (node.content) {
            for (let c of node?.content) {
                if (c.type === SFNodeType.caption) { continue; }
                const r = await this.engine.render(c, node);
                content.push(r);
            }
        }

        const images = data.images;
        let hasImage = false;
        // tables can have an image as well
        if (node.attrs && node.attrs.src) {
            const file = this.engine.data.document.files?.find(f => f.id === node.attrs.id);
            if (file?.url) {
                image = <img title={label ?? ''} src={file.url}></img>;
                hasImage = true;
            } else if (node.attrs.src?.length > 0) {
                image = <img title={label ?? ''} src={node.attrs.src} />;
            }
        }

        let htmlEl, figure;
        switch (node.attrs.type) {
            case 'image':
            case 'figure':
                const captionSide = configuration?.image?.captionSide || 'below';
                const figureIndex = data.tables.figures.findIndex(f => f.id === node.attrs.id);
                if (labelNode) {
                    figcaption = <figcaption data-figure-number={figureIndex > -1 ? figureIndex + 1 : null}>{...(await this.engine.renderContent(labelNode))}</figcaption>;
                }
                figure = <figure>
                    {captionSide === 'above' ? figcaption : ''}
                    {image}
                    {...content}
                    {captionSide === 'below' ? figcaption : ''}
                </figure>;
                htmlEl =
                    <div class="figure-wrapper">
                        {figure}
                        {notes}
                    </div>;
                break;
            case 'native-table':
                figure = <figure data-role={hasImage ? 'image-table' : 'native-table'}>
                    {...content}
                    {figcaption}
                </figure>;
                htmlEl =
                    <div class="figure-wrapper">
                        {figure}
                        {notes}
                    </div>;
                break;
            case 'table':
                const tableIndex = data.tables.tables.findIndex(f => f.id === node.attrs.id);
                if (labelNode) {
                    figcaption = <figcaption data-table-number={tableIndex > -1 ? tableIndex + 1 : null}>{...(await this.engine.renderContent(labelNode))}</figcaption>;
                }
                figure = <figure data-role={hasImage ? 'image-table' : 'native-table'}>
                    {figcaption}
                    {image}
                    {...content}
                </figure>;
                htmlEl =
                    <div class="figure-wrapper">
                        {figure}
                        {notes}
                    </div>;
                break;
            case 'code':
                const codeNode = node.content?.find(n => n.type === SFNodeType.code);
                const codeIndex = data.tables.codeBlocks.findIndex(f => f.id === codeNode?.attrs?.id);
                if (labelNode) {
                    figcaption = <figcaption data-code-block-number={codeIndex > -1 ? codeIndex + 1 : null}>{...(await this.engine.renderContent(labelNode))}</figcaption>;
                }
                figure = <figure>
                    {...content}
                    {figcaption}
                </figure>;
                htmlEl =
                    <div class="figure-wrapper">
                        {figure}
                        {notes}
                    </div>;
                break;
            default:
                htmlEl = <div>Unknown figure type {node.attrs.type}</div>
                break;
        }

        if (node.attrs && figure) {
            for (let att in node.attrs) {
                if (att != null && att !== 'src') {
                    figure.setAttribute(att, node.attrs[att]);
                }
            }
            if (node.attrs?.orientation?.length > 0) {
                htmlEl.setAttribute('orientation', node.attrs.orientation);
            }
        }

        return htmlEl;
    }
}

class CaptionRenderer extends NodeRenderer<HTMLTableCaptionElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, parent: DocumentNode<SFNodeType.figure>) {
        if (!node.content || node.content?.length === 0) {
            return <caption></caption>;
        }

        const label = await this.engine.renderContent(node.content[0]);
        return <caption>{...label}</caption>;
    }
}

const renderers: { [key: string]: any } = {
    [SFNodeType.figure]: FigureRenderer,
    [SFNodeType.caption]: CaptionRenderer
};

export default renderers;