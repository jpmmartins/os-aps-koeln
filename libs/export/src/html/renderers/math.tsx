import { TSX } from '../../TSX';

import { SciFlowDocumentData } from "../../interfaces";
import { SFNodeType, DocumentNode } from '@sciflow/schema';
import { NodeRenderer } from "../../renderers";
import { JSDOM } from 'jsdom';

const { TeX } = require('mathjax-full/js/input/tex.js');
import { HTMLDocument } from 'mathjax-full/js/handlers/html/HTMLDocument.js';
import { liteAdaptor } from 'mathjax-full/js/adaptors/liteAdaptor.js';
import { AllPackages } from 'mathjax-full/js/input/tex/AllPackages.js';
import { SerializedMmlVisitor } from 'mathjax-full/js/core/MmlTree/SerializedMmlVisitor.js';
import { SVG } from 'mathjax-full/js/output/svg.js';
import { FigureRendererConfiguration } from './figure/figure.schema';
import { Figure } from '../components/figure/figure.component';

const packages = AllPackages.filter((name) => name !== 'bussproofs');

const tex2svg = (s: string, inline = true) => {
    const tex = new TeX({ packages });
    const adaptor = liteAdaptor();
    const svg = new SVG({ fontCache: 'local' });
    const html = new HTMLDocument('', adaptor, { InputJax: tex, OutputJax: svg });
    const node = html.convert(s, {
        display: !inline,
        em: 16,
        ex: 8,
        containerWidth: 80 * 16
    });
    let output = adaptor.innerHTML(node);
    return output;
}

const tex2mml = (s: string) => {
    const tex = new TeX({ packages });
    const html = new HTMLDocument('', liteAdaptor(), { InputJax: tex });
    const visitor: any = new SerializedMmlVisitor();
    const toMathML = (node => visitor.visitTree(node, html));
    return toMathML(html.convert(s));
}

const renderers: { [key: string]: any } = {
    [SFNodeType.math]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.math>) {
            const equations = this.engine.data.document
                .tables.equations
                .filter((e) => e.attrs?.style === 'block');

            // we are importing the default implementation for a figure component here. Be aware that this might be different,
            // for some templates. So we cannot rely on the shape of the data provided.
            const figureComponent = new Figure(this.engine);
            const figureConfiguration: FigureRendererConfiguration = figureComponent.getValues();

            const index = equations.findIndex(t => t.id === node.attrs?.id);
            const inline = node.attrs.style !== 'block';

            const renderSvg = !figureConfiguration?.equation?.renderMathML === true;
            if (renderSvg) {
                const ret = tex2svg(node.attrs.tex, inline);
                const fragment = JSDOM.fragment(ret);
                if (inline) { return <span class="inline-math">{fragment}</span>; }

                return <figure id={node.attrs.id} data-type='equation' data-format='svg' data-label={`${index !== -1 ? index + 1 : ''}`} data-caption-side={figureConfiguration.equation?.captionSide}>
                    {fragment}
                </figure>
            } else {
                const ret = tex2mml(node.attrs.tex);
                const mathML = JSDOM.fragment(ret);

                if (!inline) {
                    return <figure id={node.attrs.id} data-type='equation' data-format='mathml' data-label={`${index !== -1 ? index + 1 : ''}`} data-caption-side={figureConfiguration.equation?.captionSide}>
                        {mathML}
                    </figure>;
                }

                const equation = mathML.firstChild as HTMLElement;
                if (!equation) { return mathML };
                equation.setAttribute('display', 'inline');

                return mathML;
            }


        }
    }
};

export default renderers;