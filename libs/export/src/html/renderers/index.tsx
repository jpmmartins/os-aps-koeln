import { TSX } from '../../TSX';

import { SciFlowDocumentData } from "../../interfaces";
import { SFNodeType, DocumentNode } from '@sciflow/schema';
import { NodeRenderer } from "../../renderers";

import inlineRenderers from './inline';
import tableRenderers from './table';
import figureRenderers from './figure';
import citationRenderers from './citation';
import mathRenderers from './math';
import { XMLJSXFactory } from '../../XMLJSXFactory';
import { escapeId } from '../../helpers';

const renderers: { [key: string]: any } = {
    [SFNodeType.document]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.document>) {

            let content = await this.engine.renderContent(node);
            if (node.attrs.type === 'bibliography') {
                let { references, formatting } = await this.engine.data.citationRenderer.getBibliography();
                for (const [i, reference] of references.entries()) {
                    const source = formatting.entry_ids[i];
                    const el = TSX.htmlStringToElement(reference?.trim());
                    if (source?.[0]) {
                        el?.setAttribute('id', 'ref-' + escapeId(source[0]));
                    }
                    if (el) { content.push(el); }
                }
            }

            return <section data-id={node.attrs.id} data-type={node.attrs.type} data-role={node.attrs.role}>
                {...content}
            </section>;
        }
    },
    [SFNodeType.section]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.section>) {
            return <section id={node.attrs.id} data-type={node.attrs.type} data-level={node.attrs.level} data-role={node.attrs.role} data-numbering={node.attrs.numbering}>
                {await Promise.all(node.content?.map(async (c) => {
                    const rc: any = await this.engine.render(c);
                    return rc;
                }) || '')}
            </section>;
        }
    },
    ...inlineRenderers,
    [SFNodeType.header]: class extends NodeRenderer<HTMLHeadElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.header>) {
            const content = await this.engine.renderContent(node);
            return <header>
                {...content}
            </header>
        }
    },
    [SFNodeType.paragraph]: class extends NodeRenderer<HTMLParagraphElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.paragraph>) {
            const htmlEl: HTMLParagraphElement = await (<p />);
            try {
                if (node.attrs) {
                    for (let att in node.attrs) {
                        if (node.attrs[att] !== null) {
                            htmlEl.setAttribute(att, node.attrs[att]);
                        }
                    }
                }

                if (node.text) { htmlEl.textContent = node.text; }
                if (node.content) {
                    for (let children of await this.engine.renderContent(node)) {
                        try {
                            if (Array.isArray(children)) {
                                for (let child of children) {
                                    htmlEl.appendChild(child);
                                }
                            } else {
                                if (typeof children === 'string') {
                                    htmlEl.appendChild(TSX.createTextNode(children));
                                } else {
                                    htmlEl.appendChild(children);
                                }
                            }
                        } catch (e) {
                            if (e instanceof Error) {
                                console.error('Error when adding child', e.message, children);
                            }
                            throw e;
                        }
                    }

                    // if we have a paragraph containing only a text node that is strong, we mark it as a heading
                    if (node.content.length === 1 && node.content[0].type === 'text') {
                        if (node.content[0].marks && node.content[0].marks.length === 1 && node.content[0].marks[0].type === 'strong') {
                            htmlEl.setAttribute('data-type', 'heading');
                        }
                    }
                }
                return htmlEl;
            } catch (e) {
                if (e instanceof Error) {
                    console.error('Could not render simple element', htmlEl.tagName, e.message);
                }
                throw e;
            }
        };
    },
    [SFNodeType.code]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.code>) {
            return <pre>{...(await this.engine.renderContent(node))}</pre>
        }
    },
    [SFNodeType.footnote]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.footnote>) {
            return <footnote>{...(await this.engine.renderContent(node))}</footnote>
        }
    },
    ...citationRenderers,
    ...mathRenderers,
    ...tableRenderers,
    ...figureRenderers,
    [SFNodeType.placeholder]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.placeholder>) {
            const assets = this.engine.data.templateOptions?.assets ?? [];
            const placeHolderAsset = assets.find((asset) => asset.type === node.attrs.type);
            if (placeHolderAsset) {
                return <span data-type={node.attrs?.type}>
                    <img src={placeHolderAsset.path} />
                </span>;
            }
            return <span data-type={node.attrs?.type} />;
        }
    }
};

export default renderers;
