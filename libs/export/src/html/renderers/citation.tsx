import { JSDOM } from 'jsdom';
import { TSX } from '../../TSX';

import { SFNodeType,  DocumentNode } from '@sciflow/schema';

import { SciFlowDocumentData } from './../../interfaces';
import { NodeRenderer } from '../../renderers';
import { SourceField } from '@sciflow/cite';
import { escapeId } from '../../helpers';

const renderers: { [key: string]: any } = {
    [SFNodeType.citation]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.citation>) {
            const citation = SourceField.fromString(node.attrs.source);
            if (!citation) {
                return <citation style="color: red;">Unreadable reference</citation>
            }
            try {
                const renderedCitation = this.engine.data.citationRenderer.renderCitation(citation);
                const id = citation.citationItems?.[0]?.id;
                return <a href={'#ref-' + escapeId(id)} data-type={this.engine.data.citationRenderer.footnotes === true ? 'footnote' : 'citation'}>{JSDOM.fragment(renderedCitation.renderedCitation)}</a>;
            } catch (e) {
                if (e instanceof Error) {
                    this.engine.log('error', 'Could not render reference ' + citation?.citationItems?.map(i => i.id).join(), { message: e.message, reference: citation });
                }
                return <citation style="color: red;">Missing reference {citation?.citationItems?.map(i => i.id).join()}</citation>
            }
        }
    }
};

export default renderers;