import { TSX } from '../../TSX';

import { SciFlowDocumentData } from "../../interfaces";
import { SFNodeType, DocumentNode } from '@sciflow/schema';
import { NodeRenderer } from "../../renderers";

export const getXRefLabel = (node, templateOptions, document) => {
    const idMap = document.tables.idMap;
    const referencedElement = node.attrs.href.replace('#', '');
    const link = idMap[referencedElement];
    if (!link) {
        return `Unknown reference!!!`;
    }
    let label = '', index, counter;
    switch (link.type) {
        case 'math':
            index = document.tables.equations?.findIndex(f => f.id === referencedElement);
            counter  = index + 1;
            label = templateOptions?.layout && templateOptions?.layout.equations && templateOptions?.layout.equations.counterLabel || 'equation';
            break;
        case 'figure':
        case 'table':
        case 'code':
            if (link.attrs.type === 'native-table') {
                const tables = document.tables.figures.filter((e) => e.attrs.type === 'native-table');
                index = tables?.findIndex(f => f.id === referencedElement);
                counter  = index + 1;
                label = templateOptions?.layout && templateOptions?.layout.tables && templateOptions?.layout.tables.counterLabel || 'table';
                break;
            }
            if (link.attrs.type === 'code') {
                const codes = document.tables.figures.filter((e) => e.attrs.type === 'code');
                index = codes?.findIndex(f => f.id === referencedElement);
                counter  = index + 1;
                label = templateOptions?.layout && templateOptions?.layout.codeBlocks && templateOptions?.layout.codeBlocks.counterLabel || 'code';
                break;
            }
            const figures = document.tables.figures.filter((e) => e.attrs.type === 'figure');
            index = figures?.findIndex(f => f.id === referencedElement);
            counter  = index + 1;
            label = templateOptions?.layout && templateOptions?.layout.figures && templateOptions?.layout.figures.counterLabel || 'figure';
            break;
        default:
            node.attrs.title = link.title;
            break;
    }
    return `${label ? label + ' ' : ''}${counter || link.title}`;
}

class TextRenderer extends NodeRenderer<HTMLElement | string, SciFlowDocumentData> {
    async render(node: DocumentNode <SFNodeType.text>) {
        const hyperlink = node.marks?.find(m => m.type === 'anchor');
        let plain = true;
        let text = node.text;

        if (hyperlink) {
            return <a href={hyperlink.attrs.href}>{node.text}</a>;
        }

        if (node.marks?.some(mark => mark.type === 'strong')) {
            plain = false;
            text = <b>{text}</b>;
        }

        if (node.marks?.some(mark => mark.type === 'em')) {
            plain = false;
            text = <i>{text}</i>;
        }

        if (node.marks?.some(mark => mark.type === 'sub')) {
            plain = false;
            text = <sub>{text}</sub>;
        }

        if (node.marks?.some(mark => mark.type === 'sup')) {
            plain = false;
            text = <sup>{text}</sup>;
        }

        return plain && text ? TSX.createTextNode(text) : text;
    }
}

class LinkRenderer extends NodeRenderer<HTMLLinkElement, SciFlowDocumentData> {
    async render(node: DocumentNode<any>) {
        const label = getXRefLabel(node, this.engine.data.templateOptions, this.engine.data.document);
        const htmlEl = <a>{label}</a>;
        for (let att in node.attrs) {
            htmlEl.setAttribute(att, node.attrs[att]);
        }
        return htmlEl;
    }
}

class HeadingRenderer extends NodeRenderer<HTMLHeadingElement, SciFlowDocumentData> {
    async render(node: DocumentNode<any>) {

        const heading = this.engine.data.document.tables?.toc.headings.find(h => h.id === node.attrs.id);
        const content = await this.engine.renderContent(node);
        const htmlEl = this.engine.data.virtualDocument.createElement(`h${node.attrs.level ?? 1}`) as HTMLHeadingElement;
        
        if (node.attrs.numbering?.length > 0 && node.attrs.numbering !== 'none') {
            node.attrs['data-numbering'] = node.attrs.numbering;
            delete node.attrs.numbering;
        }
        const span = <span>{...content}</span>
        for (let att in node.attrs) {
            span.setAttribute(att, node.attrs[att]);
        }

        if (heading?.numberingString?.length > 0) {
            span.setAttribute('data-numbering', heading.numberingString);
        }

        if (heading?.numberingType?.length > 0) {
            span.setAttribute('data-numbering-type', heading.numberingString);
        }

        htmlEl.append(span);
        return htmlEl;
    }
}


const basicNode = (tagName: string, attrs?) => {
    return class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {

        async render(node: DocumentNode<any>) {
            let htmlEl = this.engine.data.virtualDocument.createElement(tagName);
            if (node.type === SFNodeType.ordered_list && node.attrs.order) {
                node.attrs.start = node.attrs.order;
                delete node.attrs.order;
            }
            for (let att in node.attrs) {
                htmlEl.setAttribute(att, node.attrs[att]);
            }

            if (node.text) {
                htmlEl.textContent = node.text;
            }

            if (node.content) {
                for (let children of await this.engine.renderContent(node)) {
                    try {
                        if (Array.isArray(children)) {
                            for (let child of children) {
                                htmlEl.appendChild(child);
                            }
                        } else {
                            htmlEl.appendChild(children);
                        }
                    } catch (e) {
                        console.error('Error when adding child', e.message, children);
                        throw e;
                    }
                }
            }

            return htmlEl;
        }
    }
}

class PageBreakRenderer extends NodeRenderer<HTMLSpanElement, SciFlowDocumentData> {
    async render(node: DocumentNode<any>) {
        return <div data-page-break="after"></div>;
    }
}

basicNode('span', { 'data-page-break': 'after' })

const renderers: { [key: string]: any } = {
    [SFNodeType.image]: basicNode('img'),
    [SFNodeType.text]: TextRenderer,
    [SFNodeType.quote]: basicNode('q'),
    [SFNodeType.hardBreak]: basicNode('br'),
    [SFNodeType.pageBreak]: PageBreakRenderer,
    [SFNodeType.blockquote]: basicNode('blockquote'),
    [SFNodeType.ordered_list]: basicNode('ol'),
    [SFNodeType.bullet_list]: basicNode('ul'),
    [SFNodeType.link]: LinkRenderer,
    [SFNodeType.hyperlink]: basicNode('a'),
    [SFNodeType.list_item]: basicNode('li'),
    [SFNodeType.horizontalRule]: basicNode('hr'),
    [SFNodeType.subtitle]: basicNode('h2'),
    [SFNodeType.heading]: HeadingRenderer,
    'heading2': basicNode('h1')
};

export default renderers;