import { it, describe } from 'mocha';
import { assert, expect, should } from 'chai';
import { createPrefix, generateListings, getRenderer } from '.';

import data from './fixtures/numbered-document.json';

describe('Numbering helpers', async () => {

    it('Creates prefixes from numbering arrays', async () => {
        expect(createPrefix([1,1,2,0,0,0], 'lower-alpha', 'none')).to.equal('a.1.2');
        expect(createPrefix([1,1,2,0,0,0], 'decimal', 'none')).to.equal('1.1.2');
        expect(createPrefix([1,1,2,99,0,0], 'decimal', 'none')).to.equal('1.1.2.99');
    });

    it('Chapters with numbering none are not numbered', async () => {
        const { headings, toc } = await generateListings(data.document);

        const references = toc.headings.find(heading => heading.counterStyle === 'decimal');
        expect(references).to.exist;
        expect(references?.numberingString).to.equal('1');
    });

    it('Chapters with numbering decimal are numbered 1,2,3 and 1.1.1, etc.', async () => {
        const { headings, toc } = await generateListings(data.document);

        // new chapter with decimal numbering format
        const introduction = toc.headings.find(heading => heading.id === 'nlh9a8lkzeuz');
        expect(introduction).to.exist;
        expect(introduction?.numberingString).to.equal('1');

        const introductionLevel2 = toc.headings.find(heading => heading.id === 'rl74n0ypd72z5');
        expect(introductionLevel2).to.exist;
        expect(introductionLevel2?.numberingString).to.equal('1.1');

        const introductionSubLevel2 = toc.headings.find(heading => heading.id === 'ltpvih85erg4j');
        expect(introductionSubLevel2).to.exist;
        expect(introductionSubLevel2?.numberingString).to.equal('1.2');

        const introductionLevel3 = toc.headings.find(heading => heading.id === 'sSzgegyytn1');
        expect(introductionLevel3).to.exist;
        expect(introductionLevel3?.numberingString).to.equal('1.2.1');

        const introductionLevel4 = toc.headings.find(heading => heading.id === 'ocmn3meif2k8r');
        expect(introductionLevel4).to.exist;
        expect(introductionLevel4?.numberingString).to.equal('1.2.1.1');

        const introductionLevel5 = toc.headings.find(heading => heading.id === 'cy0mykqr7vwk1');
        expect(introductionLevel5).to.exist;
        expect(introductionLevel5?.numberingString).to.equal('1.2.1.1.1');

        const introductionSubLevel2_3 = toc.headings.find(heading => heading.id === 'pA8192cbi80va');
        expect(introductionSubLevel2_3).to.exist;
        expect(introductionSubLevel2_3?.numberingString).to.equal('1.3');

        // chapter 2 with decimal numbering format
        const methods = toc.headings.find(heading => heading.id === 'zydwss409oyzr');
        expect(methods).to.exist;
        expect(methods?.numberingString).to.equal('2');

        const methodsLevel2 = toc.headings.find(heading => heading.id === 'sbve6vlveysg');
        expect(methodsLevel2).to.exist;
        expect(methodsLevel2?.numberingString).to.equal('2.1');

        const methodsLevel3 = toc.headings.find(heading => heading.id === 'pdbonb58ejejp');
        expect(methodsLevel3).to.exist;
        expect(methodsLevel3?.numberingString).to.equal('2.2');
    });

    it('Chapters with numbering upper alphabetic are numbered A,B,C, A.1, A.1.1', async () => {
        const { headings, toc } = await generateListings(data.document);

        // new chapter with upper-alpha numbering format
        const appendix = toc.headings.find(heading => heading.id === 'bojwsu840nl6');
        expect(appendix).to.exist;
        expect(appendix?.numberingString).to.equal('A');

        const appendixLevel2 = toc.headings.find(heading => heading.id === 'bynj6i0a7tz5r');
        expect(appendixLevel2).to.exist;
        expect(appendixLevel2?.numberingString).to.equal('A.1');

        const appendixLevel3 = toc.headings.find(heading => heading.id === 'aZaygjtwq3l4m');
        expect(appendixLevel3).to.exist;
        expect(appendixLevel3?.numberingString).to.equal('A.2.1');

        const appendixLevel4 = toc.headings.find(heading => heading.id === 'wzb1ggu8qgzjb');
        expect(appendixLevel4).to.exist;
        expect(appendixLevel4?.numberingString).to.equal('A.2.1.1');

        const appendixLevel5 = toc.headings.find(heading => heading.id === 'soawl1p1ql5s5');
        expect(appendixLevel5).to.exist;
        expect(appendixLevel5?.numberingString).to.equal('A.2.1.1.1');

        // new chapter with upper-alpha numbering format
        const appendix2 = toc.headings.find(heading => heading.id === 'ihf1fac5sxxxb');
        expect(appendix2).to.exist;
        expect(appendix2?.numberingString).to.equal('B');

        const appendi2Level2 = toc.headings.find(heading => heading.id === 'eMos0n7pj2f9n');
        expect(appendi2Level2).to.exist;
        expect(appendi2Level2?.numberingString).to.equal('B.1');

    });

    it('Chapters with numbering lower alphabetic are numbered a,b,c, a.1, a.1.1', async () => {
        const { headings, toc } = await generateListings(data.document);

        // new chapter with lower-alpha numbering format
        const lowerAlpha = toc.headings.find(heading => heading.id === 'ujt7n6jubh0v');
        expect(lowerAlpha).to.exist;
        expect(lowerAlpha?.numberingString).to.equal('a');

        const lowerAlphaLevel3 = toc.headings.find(heading => heading.id === 'irm9wa6fe5iu');
        expect(lowerAlphaLevel3).to.exist;
        expect(lowerAlphaLevel3?.numberingString).to.equal('a.1.1');

    });

    it('All headings numbering formats are formated decimal, upper-alpha, lower-alpha, none', async () => {
        const { headings, toc } = await generateListings(data.document);

        const introduction = toc.headings.find(heading => heading.id === 'nlh9a8lkzeuz');
        expect(introduction).to.exist;
        expect(introduction?.counterStyle).to.equal('decimal');

        const appendix = toc.headings.find(heading => heading.id === 'bojwsu840nl6');
        expect(appendix).to.exist;
        expect(appendix?.counterStyle).to.equal('upper-alpha');

        const lowerAlpha = toc.headings.find(heading => heading.id === 'ujt7n6jubh0v');
        expect(lowerAlpha).to.exist;
        expect(lowerAlpha?.counterStyle).to.equal('lower-alpha');

        const references = toc.headings.find(heading => heading.id === 'ckdufzqqw9nz4');
        expect(references).to.exist;
        // default is decimal
        expect(references?.counterStyle).to.equal('decimal');
    });

});
