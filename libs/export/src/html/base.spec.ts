import { it, describe } from 'mocha';
import { expect } from 'chai';
import { generateListings, getRenderer } from '.';
import { NoStyleCitationRenderer } from './citation-renderer';

import data from './fixtures/data.json';
import { SFNodeType, DocumentNode } from '@sciflow/schema';

describe('HTML Exporter', async () => {
    it('Generates listings', async () => {
        const listings = await generateListings(data.document as unknown as DocumentNode<SFNodeType.document>);
        expect(listings.equations.length).to.equal(1);
    })
    it('Exports all parts', async () => {
        const renderer = getRenderer({
            document: { document: data.document, tables: await generateListings(data.document as unknown as DocumentNode<SFNodeType.document>) },
            citationRenderer: new NoStyleCitationRenderer(),
            templateOptions: null
        });

        const result = await renderer.render(data.document as unknown as DocumentNode<SFNodeType.document>);
        console.log(result.outerHTML);
        expect(true); // FIXME #29
    });
});
