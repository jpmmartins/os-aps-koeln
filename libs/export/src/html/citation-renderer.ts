import { CitationItem } from "@sciflow/cite";

export interface CitationRenderer {
    footnotes: boolean;
    getBibliography: any;
    getCitation: any; // TODO
}

export class NoStyleCitationRenderer implements CitationRenderer {
    footnotes = false;
    
    constructor() {}

    public getBibliography(args?): { errors: any[], entries: any[] } {
        return { errors: [], entries: [] };
    }

    public getCitation(ids: string | any[] | ReadonlyArray<any>, additionalInfo?: { [key: string]: CitationItem }): string {
        return Array.isArray(ids) ? ids.join(', ') : ids as string;
    }
}