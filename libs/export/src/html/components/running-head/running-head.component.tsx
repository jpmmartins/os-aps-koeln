import { Component, TemplateComponent } from '../../../component';
import { RunningHeadConfiguration } from './running-head.schema';
import { PageConfiguration } from '../page/page.schema';

@Component({
    styles: `
    @if #{isRunner('pagedjs')} == 'false' {
        @debug "Styling Running head";
        h1 span {
            string-set: h1-numbering attr(data-numbering) h1-numbering-and-content attr(data-numbering) " " content() hx-numbering-and-content attr(data-numbering) " " content() h2-numbering-and-content;
        }
        h2 span {
            string-set: h2-numbering attr(data-numbering) h2-numbering-and-content attr(data-numbering) " " content() hx-numbering-and-content attr(data-numbering) " " content();
        }

        @debug "RunningHead.state - #{get('state')}";
        @if #{get('state')} == 'on'   {
            @debug "RunningHead font size -  #{get('font.size', null, 'Numbering')}";
            @debug "RunningHead font style - #{get('fontStyle')}";
            @debug "RunningHead style - #{get('style')}";
            @debug "RunningHead left content - #{get('content.left')}";
            @debug "RunningHead right content - #{get('content.right')}";
            @debug "RunningHead padding top - #{get('paddingTop')};"; 
            @page {
                @top-left {
                    font-size: #{get('font.size', null, 'Numbering')};
                    font-style: #{get('fontStyle')};
                }
                @top-right {
                    font-size: #{get('font.size', null, 'Numbering')};
                    font-style: #{get('fontStyle')};
                    text-align: right;
                    @if #{get('content.right')} == 'headings' {
                        content: string(h1-numbering-and-content, first-except);
                    } @else if #{get('content.right')} == 'authors' {
                        content: var(--authors);
                    } @else if #{get('content.right')} == 'title' {
                        content: var(--title);
                    }
                    
                }
            }
            
            @if #{get('page.handling', null, 'Page')} == 'double-sided' {
                @page:left {
                    @top-left {
                        font-size: #{get('font.size', null, 'Numbering')};
                        font-style: #{get('fontStyle')};
                        text-align: left;
                        @if #{get('content.left')} == 'headings' {
                            content: string(h1-numbering-and-content, first-except);
                        } @else if #{get('content.left')} == 'authors' {
                            content: var(--authors);
                        } @else if #{get('content.left')} == 'title' {
                            content: var(--title);
                        }
                        
                    }
                    @top-right {
                        content: '';
                    }
                }
                @page:right {
                    @top-right {
                        font-size: #{get('font.size', null, 'Numbering')};
                        font-style: #{get('fontStyle')};
                        // vertical-align: top;
                        @if #{get('content.right')} == 'headings' {
                            content: string(hx-numbering-and-content, last);
                        } @else if #{get('content.right')} == 'authors' {
                            content: var(--authors);
                        } @else if #{get('content.right')} == 'title' {
                            content: var(--title);
                        }
                    }
                }
            }

            // This is needed if page breaks before chapters are enabled to work within journals
            @if #{get('page.pageBreaks.beforeChapter', null, 'Page')} != 'auto' { 
                @debug "Setting page group start as there are breaks before chapters";
                h1 { 
                    prince-page-group: start;
                    // TODO check if it works in pagedjs
                    page-group: start;
                } 
            }

            // running bounding box and style
            @page {
                @top-left {
                    padding-top: #{get('paddingTop')};
                    /// vertical alignment should be different when there is a border
                    vertical-align: top;
                    @if #{get('style')} == 'border-bottom'  {
                        padding-top: 0;
                        vertical-align: bottom;
                        border-bottom: thin black solid;
                        margin-bottom: 1rem;
                    }
                }
                @top-right {   
                    padding-top: #{get('paddingTop')};
                    vertical-align: top;
                    @if #{get('style')} == 'border-bottom'  {
                        padding-top: 0;
                        vertical-align: bottom;
                        border-bottom: thin black solid;
                        margin-bottom: 1rem;
                    }
                }
            }

            
            // remove running head from pages it should not exist
            @page:first { 
                @top-left {
                    border: none;
                    content: none;
                }
                @top-right {
                    border: none;
                    content: none;
                }
            }

            @page coverpage {
                @top-left {
                    border: none;
                    content: none;
                }
                @top-right {
                    border: none;
                    content: none;
                }
            }

            @page body:blank {
                @top-left {
                    border: none;
                    content: none;
                }
                @top-right {
                    border: none;
                    content: none;
                }
            }
        }
    }
    `
})
export class RunningHead extends TemplateComponent<RunningHeadConfiguration, any> {}
