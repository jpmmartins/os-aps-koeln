export interface RunningHeadConfiguration {
    /**
     * @title State
     * @default 'on'
     */
    state: 'on' | 'off';
    /**
     * @title Style
     * @default 'border-bottom'
     */
    style: 'none' | 'border-bottom';
    /**
     * @title Style
     * @default 'normal'
     */
    fontStyle: 'normal' | 'italic';
    /**
     * @title Padding top
     * @default 1cm
     */
    paddingTop: string;
    /**
     * @title Content 
     * @description Which content should appear on the running head
     */
    content: {
        /**
         * @title Left page 
         * @description Content for the left page
         * @default 'headings'
         */
        left: 'headings' | 'authors' | 'title';
        /**
         * @title Right page (default page)
         * @description Content for the right page
         * @default 'headings'
         */
        right: 'headings' | 'authors' | 'title';
    }

}
