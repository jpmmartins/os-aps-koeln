/**
 * @title Page layout
 * @description Set up page margins, fonts and page sizes.
 */
export interface PageConfiguration {
    page: {
        /**
         * @title Size
         * @description The page size.
         * @default A4
         */
        size: string;
        margins: {
            /**
             * @title The inner margin (for two-sided printing)
             * @description The inner margin is usually larger to account for binding a printed document
             * @default 25mm
             */
            inner: string;
            /**
             * @title The outer margin
             * @default 20mm
             */
            outer: string;
            /**
             * @title The top margin
             * @default 20mm
             */
            top: string;
            /**
             * @title The bottom margin
             * @default 20mm
             */
            bottom: string;

        };
        /**
         * @title Handling
         * @description One sided or two sided
         * @default double-sided
         */
        handling: 'one-sided' | 'double-sided';
        pageBreaks: {
            /**
             * @title Break before chapters
             * @description determines whether to break before a chapter. Recto will automatically put all new chapters on recto (i.e. right for rtl languages) pages.
             * @default auto
             */
            beforeChapter: 'auto' | 'recto' | 'avoid';
        }
        font: {
            /**
             * @title The font family
             * @default Arial
             */
            family: string;
            /**
             * @title The font size
             * @default 12pt
             */
            size: string;
        }
    }
    
    /**
     * @title Typesetting
     * @description General options for typesetting.
     */
    typesetting: {
        /** 
         * @title Widows
         * @default 3
         */
        widows: number;
        /**
         * @title Orphans
         * @default 3
         */
        orphans: number;
         /**
         * @title Indentation
         * @default 0mm
         */
         indentation: string;
    };
    /**
     * @title The locale (e.g. de-DE, en-GB, ..)
     * @default en-US
     */
    locale?: string;
}
