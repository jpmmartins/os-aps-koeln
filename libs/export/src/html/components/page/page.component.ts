import { Component, TemplateComponent } from '../../../component';
import { PageConfiguration } from './page.schema';

@Component({
    readme: `
    # Page setup
    General layout of the page like paper size, margins and typography.
    `,
    styles: `@import '@sciflow/tdk/styles/base.scss';
    @import '@sciflow/tdk/styles/pagedjs.scss';

    @if #{isRunner('pagedjs')} == 'true' {
        @debug "Runner - pagedjs";
        @include pagedjs;
    }
    
    /** Page */
    @debug "Page handling - #{get('page.handling')}";
    @debug "Page margin inner - #{get('page.margins.inner')}";
    @debug "Page margin outer - #{get('page.margins.outer')}";
    @debug "Page margin top - #{get('page.margins.top')}";
    @debug "Page margin bottom - #{get('page.margins.bottom')}";
    @page {
        size: #{get('page.size')};
        @debug "Page handling - #{get('page.handling')}";
        @if #{get('page.handling')} == 'double-sided' {
            margin-inside: #{get('page.margins.inner')};
        } @else {
            margin-inside: #{get('page.margins.outer')};;
        }
        margin-outside: #{get('page.margins.outer')};
        margin-top:  #{get('page.margins.top')};
        margin-bottom: #{get('page.margins.bottom')};
        
    }

    @debug "Page using font - #{get('page.font.family')} #{get('page.font.size')}";
    html, body {
        @if #{isRunner('princexml')} == 'true' {
            font-family: #{get('page.font.family')}, prince-no-fallback;
        } @else {
            font-family: #{get('page.font.family')};    
        }
        font-size: #{get('page.font.size')};
    }

    @debug "Page sing widows - #{get('typesetting.widows')}";
    @debug "Page using orphans - #{get('typesetting.orphans')}";
    @debug "Page using indentation - #{get('typesetting.indentation')}";
    p { 
        widows: #{get('typesetting.widows')};
        orphans: #{get('typesetting.orphans')};
        text-indent: #{get('typesetting.indentation')};
    }

    @debug "Page with breaking pages on - #{get('page.pageBreaks.beforeChapter')}";
    .front > section, .front .TableOfContents, .body > section[data-level="1"] {
        page-break-before: #{get('page.pageBreaks.beforeChapter')};
    }

    // All these running head resets should go into the base css?
    @page:blank {
        @top-left {
            content: none;
        }
        @top-right {
            content: none;
        }
        @bottom-left {
            content: none;
        }
        @bottom-right {
            content: none;
        }
    }
    
    @page front:blank {
        @top-left {
            content: none;
        }
        @top-right {
            content: none;
        }
        @bottom-left {
            content: none;
        }
        @bottom-right {
            content: none;
        }
    }

    @page body:blank {
        @top-left {
            content: none;
        }
        @top-right {
            content: none;
        }
        @bottom-left {
            content: none;
        }
        @bottom-right {
            content: none;
        }
    }

    @page wide {
        @top-left {
            content: none;
        }
        @top-right {
            content: none;
        }
        @bottom-left {
            content: none;
        }
        @bottom-right {
            content: none;
        }
    }
    
    @if #{isRunner('pagedjs')} == 'true' {
        @page {
            margin-left: #{get('page.margins.outer')};
            margin-right: #{get('page.margins.outer')};
        }
        @if #{get('page.handling')} == 'double-sided' {
            @page :right {
                margin-left: #{get('page.margins.inner')};
                margin-right: #{get('page.margins.outer')};
            }
            @page :left {
                margin-left: #{get('page.margins.outer')};
                margin-right: #{get('page.margins.inner')};
            }
        }
    }
    `
})
export class Page extends TemplateComponent<PageConfiguration, any> {}
