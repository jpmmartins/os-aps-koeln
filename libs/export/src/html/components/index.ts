import { Figure } from './figure/figure.component';
import { Footnote } from './footnote/footnote.component';
import { Numbering } from './numbering/numbering.component';
import { Page } from './page/page.component';
import { TableOfContents } from './table-of-contents/table-of-contents.component';
import { Title } from './title/title.component';
import { RunningHead } from './running-head/running-head.component';
import { JournalTitle } from './journal-title/journal-title.component';
import { EpubPage } from './epub-page/epub-page.component';

export { Numbering } from './numbering/numbering.component';
export { Page } from './page/page.component';
export { TableOfContents } from './table-of-contents/table-of-contents.component';

const components = [
    Page,
    Numbering,
    TableOfContents,
    JournalTitle,
    Title,
    EpubPage,
    Footnote,
    Figure,
    RunningHead
];

export const htmlComponents = components;
export default components;