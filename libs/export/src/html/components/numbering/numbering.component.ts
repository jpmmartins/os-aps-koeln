import { TemplateComponent, Component } from '../../../component';
import { NumberingConfiguration } from './numbering.schema';
import { PageConfiguration } from '../page/page.schema';

@Component({
    styles: `
    //@debug "styling for double sided printing";
    @debug "Checking numbering placement - #{get('placement.region')}";
    @debug "Handling numbering for - #{get('page.handling', null, 'Page')}";
    @debug "Numbering vertical alignment - #{get('align.vertical')}";
    @debug "Numbering vertical alignment - #{get('align.paddingBottom')}";
    // // FIXME: this should be whatever is set in numbering.font.size in config
    @debug "Numbering font size - #{get('font.size')}";

    @page front {
        @#{get('placement.region', 'bottom')}-right  {
            vertical-align: #{get('align.vertical')};
            padding-bottom: #{get('align.paddingBottom')};
            font-size: #{get('font.size')};
            content: counter(page, upper-roman);
        }
    }

    @page body {
        @#{get('placement.region', 'bottom')}-right {
            vertical-align: #{get('align.vertical')};
            padding-bottom: #{get('align.paddingBottom')};
            font-size: #{get('font.size')};
            content: counter(page);
        }
    }
    
    @if #{get('page.handling', null, 'Page')} == 'double-sided' {
        @page front:left {
            @#{get('placement.region', 'bottom')}-left  {
                vertical-align: #{get('align.vertical')};
                padding-bottom: #{get('align.paddingBottom')};
                font-size: #{get('font.size')};
                content: counter(page, upper-roman);
            }
            @#{get('placement.region', 'bottom')}-right {
                content: none;
            }
        }

        // @page body:right {
        //     @#{get('placement.region', 'bottom')}-right {
        //         font-size: #{get('font.size')};
        //         content: counter(page, upper-roman);
        //     }
        // }

        @page body:left {
            @#{get('placement.region', 'bottom')}-left {
                vertical-align: #{get('align.vertical')};
                padding-bottom: #{get('align.paddingBottom')};
                font-size: #{get('font.size')};
                content: counter(page);
            }
            // Remove numbering from the right region on the left page if double-sided
            @#{get('placement.region', 'bottom')}-right {
                content: none;
            }
        }
        
        // @page body:right {
        //     @#{get('placement.region', 'bottom')}-right {
        //         font-size: #{get('font.size')};
        //         content: counter(page);
        //     }
        // }
    }
    `
})
export class Numbering extends TemplateComponent<NumberingConfiguration, any> {}
