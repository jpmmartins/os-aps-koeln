/**
 * @title Numbering
 * @description Settings for page numbering.
 */
export interface NumberingConfiguration {
    placement: {
        /**
         * @title The region on the page where page numbers appear
         * @default bottom
         */
        region: 'bottom' | 'top';
    },
    font: {
        /**
         * @title The font size for numbering
         * @default 10pt
         */
        size: string;
    }
    align: {
        /**
         * @title Vertical alignment
         * @default middle
         */
        vertical: 'bottom' | 'middle' | 'top';
        /**
        * @title Padding bottom
        * @default 0mm
        */
        paddingBottom: string;
    }
}
