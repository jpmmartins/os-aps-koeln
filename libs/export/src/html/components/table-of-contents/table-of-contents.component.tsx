import { wrapTOC } from '../../../helpers';
import { TSX } from '../../..';
import { TemplateComponent, Component } from '../../../component';
import { TableOfContentsConfiguration } from './table-of-contents.schema';

@Component({
  styles: `
    @debug "Rendering table of contents";

    .TableOfContents {
      #table-of-contents {page-break-before: always;}
      ul, li {
        margin: 0;
        padding: 0;
        list-style: none;
        -webkit-hyphens: none;
        hyphens: none;
        text-align: left;
        ul {
          page-break-before: avoid;
        }
      }
      
      a {
          color: inherit;
          text-decoration: none;
          hyphens: none;
          text-align: left;
      }

      @if #{isRunner('pagedjs')} == 'false' {
        .link-with-target:after {
            padding-left: 1rem;
            content: leader("#{get('leader')}") target-counter(attr(href), page);
        }
      }
    
      li[data-numbering]:not([data-numbering="none"]) {
        a {
          display: block;
          position: relative;
          /* repositioning chapter counter */
          span.toc-numbering {
            position: absolute;
            top: 0;
            left: 0;
          }
        }
      
        &[data-toc-level="1"] {
          margin-top: 0.5rem;
          break-inside: avoid;
          > a {
            font-weight: bold;
            margin-top: 0;
            padding-left: 1.25rem;
          }
        }
        
        &[data-toc-level="2"] {
          margin-top: 0;
          margin-left: 1.25rem;
          a {
            margin-top: 0;
            padding-left: 2.4rem;
          }
        }
        
        &[data-toc-level="3"] {
          margin-top: 0;
          margin-left: 2.4rem;
          a {
            margin-top: 0;
            padding-left: 3.3rem;
          }
        }

        &[data-toc-level="4"] {
          margin-top: 0;
          margin-left: 3.3rem;
          a {
            margin-top: 0;
            padding-left: 3.6rem;
          }
        }

        &[data-toc-level="5"] {
          margin-top: 0;
          margin-left: 3.6rem;
          a {
            margin-top: 0;
            padding-left: 3.9rem;
          }
        }
      }
    }
    `
})
export class TableOfContents extends TemplateComponent<TableOfContentsConfiguration, any> {
  public renderDOM() {
    const headings = this.engine.data.document.tables?.toc.headings;
    const wrappedTOC = wrapTOC(headings);

    const depth = this.getValue('depth');
    if (headings.length === 0) {
      return {
        id: 'table-of-contents',
        placement: this.page || 'front',
        dom: <span />
      };
    }

    const renderHeadings = (headings, level) => level <= depth ? <ul>
      {headings.map(heading => <li data-toc-level={heading.level} data-numbering={heading.numberingString}>
        {heading.title ? <a href={`#${heading.id}`} class="link-with-target">
          {heading.numberingString ? <span class="toc-numbering">{heading.numberingString}</span> : ''}
          {heading.title && !heading.titleHTML ? <span class="toc-title">{heading.title}</span> : ''}
          {heading.titleHTML ? TSX.htmlStringToElement(`<span class="toc-title">${heading.titleHTML}</span>`) : ''}
        </a> : ''}
        {heading.content?.length > 0 ? renderHeadings(heading.content, level + 1) : ''}
      </li>)}
    </ul> : '';

    return {
      id: 'table-of-contents',
      placement: this.page || 'front',
      dom: <div class={this.kind}>
        <section id="table-of-contents">
          <h1><span>{this.getValue('label') || 'Contents'}</span></h1>
          {renderHeadings(wrappedTOC, 1)}
        </section>
      </div>
    };
  }
}
