/**
 * @title Table of contents
 */
export interface TableOfContentsConfiguration {
    /** @title The leader symbol between the heading title and the page number */
    leader: '.' | undefined,
    /** @title The text displayed above the table of contents */
    label: string;
    /**
     * @title TOC levels
     * @description The number of heading levels to show in the table of contents (e.g. 3 means 1.1.1)
     * @default 3
     */
    depth: number;
}
