import { TSX } from '../../..';
import { TemplateComponent, Component } from '../../../component';
import { FigureConfiguration } from './figure.schema';
// import { FigureRendererConfiguration } from '../../renderers/figure/figure.schema';
@Component({
    styles: `
    @debug "Figure with captionFontSize - #{get('captionFontSize')}";
    // We do not use images as tables anymore, but just uncomment here for a switch
    // figure[type="figure"], figure[type="image"] {
    //     display: table;
    //     figcaption {
    //         display: table-caption;
    //         @if #{get('image.captionSide')} == 'above' {
    //             caption-side: top;
    //         }
    //         @if #{get('image.captionSide')} == 'below' {
    //             caption-side: bottom;
    //         }
    //     }   
    // }
    figure {
        margin: 1rem auto;
        break-inside: avoid;
        page-break-inside: avoid;
        img {
            display: block;
            margin: 0 auto;
            max-width: 100%;
            max-height: calc(100vh - 5cm);
        }
        figcaption {
            font-size: #{get('captionFontSize')};
            margin: 0.25rem auto;
            &:before {
                font-weight: bold;
                display: block;
                @debug "Figure image label - #{get('image.label')}";
                content: "#{get('image.label')} " attr(data-figure-number) " ";
            }

        }
        +[data-role="notes"] {
            font-size: #{get('captionFontSize')};
        }
        table {
            display: table;
            font-size: 90%;
            border-collapse: collapse;
            table-layout: fixed;
            min-width: 16rem;
            @debug "Figure table captionSide - #{get('table.captionSide')}";
            caption {
                text-align: left;
                prince-caption-page: first;
                font-size: #{get('captionFontSize')};
                margin-top: 0.25rem;
                @if #{get('table.captionSide')} == 'above' {
                    caption-side: top;
                }
                @if #{get('table.captionSide')} == 'below' {
                    caption-side: bottom;
                }
                &:before {
                    font-weight: bold;
                    display: block;
                    @debug "Figure table label - #{get('table.label')}";
                    content: "#{get('table.label')} " attr(data-figure-number) " ";
                }
            }
            th, td {
                vertical-align: top;
                padding: 0.3rem;
            }
            tbody {
                border-top: 1px solid rgb(132, 140, 149);
                border-bottom: 1px solid rgb(132, 140, 149);
            }
    
        }
    }

    figure[data-type="equation"][data-caption-side="right"] {
        position: relative;
    }
    figure[data-type="equation"][data-caption-side="right"]::after {
        content: "[" attr(data-label) "]";
        position: absolute;
        right: 0.5rem;
        top: 30%;
    }
    `
})
export class Figure extends TemplateComponent<FigureConfiguration, any> {}
