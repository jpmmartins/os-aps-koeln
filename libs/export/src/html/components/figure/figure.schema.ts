


















/**
 * Figure component schema (extending the base schema needed by the figure renderer)
 */

import { FigureRendererConfiguration } from "../../renderers/figure/figure.schema";

/**
 * @title Test figure configuration
 */
export interface FigureConfiguration extends FigureRendererConfiguration {}
