import { TSX } from '../../../TSX';
import { TemplateComponent, Component } from '../../../component';

@Component({
    readme: `Creates the title section`,
    styles: `
    .Title {
        break-before: never;
    }
    .authors {
        margin-bottom: 4rem;
    }
    `
})
export class JournalTitle extends TemplateComponent<{}, any> {
    public renderDOM(): { id: string; dom: HTMLElement; placement?: string | undefined; } | undefined {

        const authors = this.engine.data.document.authors;
        const getName = (author) => {
            if (!author) { return 'Unknown author'; }
            if (author.firstName || author.lastName) {
                return [author.firstName, author.lastName].filter(n => n !== null).join(' ');
            }
            if (author.name) { return author.name; }
            return 'Unknown author';
        }
        const createInstitutionId = (institution) => institution.slug || [institution.department, institution.institution, institution.city].join('-');
        let affiliations: any = authors.map((author) => author.positions?.map((position) => {
            position.slug = position.slug ?? createInstitutionId(position);
            return position;
        }));
        affiliations = [].concat.apply([], affiliations);
        const mapAuthor = (position) => {
            const pos = affiliations.findIndex((affiliation: any) => affiliation?.slug === position.slug)
            return pos + 1;
        };
        const authorSection = <section class="authors">{
            ...authors.map((author, index) => <span class="author">{getName(author)}
                <sup>{author.positions?.map(mapAuthor).filter(l => l != null).join(' ')}
                    {author.orcid ? (<a href={`https://orcid.org/${author.orcid}`} target="_blank">
                        <img class="orcid-icon" src="/export/static/orcid.png" alt={author.orcid} /></a>) : ''}
                    {author.correspondingAuthor ? '*' : ''}</sup>
                {index + 1 < authors.length ? ', ' : ''}
            </span>)
        }
            <div class="affiliations small my2">
                {...affiliations.map((affiliation, index) => (<address>
                    <sup>{`${index + 1}`}</sup>&nbsp;
                    <span>{[
                        affiliation.department,
                        affiliation.institution,
                        affiliation.city,
                        affiliation.country
                    ].filter(l => l?.length > 0).join(', ')}
                    </span>
                </address>))}
            </div></section>;

        return {
            id: 'journal-title',
            placement: this.page || 'body',
            dom: <section class="Title">
                <h1>{this.engine.data?.document?.title}</h1>
                {authorSection}
            </section>
        }
    }
}
