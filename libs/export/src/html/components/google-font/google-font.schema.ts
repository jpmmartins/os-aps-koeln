export interface GoogleFontConfiguration {
    /**
     * @title The font name
     * @description e.g. EB Garamond', serif
     * 
     */
    fontFamily: string;
    /**
    * @title Google Fonts
    */
    api?: {
        /**
         * @title URL
         * @description Full URL to a Google font (e.g. https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@0,400;0,700;1,400;1,700)
         */
        url: string;
    }
}