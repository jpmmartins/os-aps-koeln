# Components

Components are responsible for rendering styles and DOM that can be injected into the document. The order of components depends on the configuration.yml provided in the template.

All components must be exported from *libs/export/src/html/components/index.ts*
