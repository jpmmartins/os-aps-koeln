export interface FootnoteConfiguration {
    /**
     * @title Font size
     * @default 9pt
     */
    fontSize: string;
    /**
     * @title Separator style
     * @default none
     */
    separatorStyle: 'none' | 'borderTop' | 'borderLeft';
}
