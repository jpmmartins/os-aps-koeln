import { Component, TemplateComponent } from '../../../component';
import { FootnoteConfiguration } from './footnote.schema';

@Component({
    styles: `
    @page {
        @footnote {
            margin-top: 20px;
            padding-top: 0.6rem;
            margin-bottom: 0;
            padding-bottom: 0.5rem;
            text-indent: 4rem hanging;
            break-inside: avoid;
            max-height: 55%;
            @if #{get('separatorStyle')} == 'borderTop' {
                border-top: 0.5px solid black;
                @debug "FootnoteConfiguration.seperatorStyle: rendering footnote with border top";
            } @else if #{get('separatorStyle')} == 'borderLeft'  {
                @debug "FootnoteConfiguration.seperatorStyle: rendering footnote with border left";
                border-top: 0.5px solid black;
                border-clip: 5em;
            }
        }
    }

    footnote {
        // FIXME: fontSize on footnote component should come with pt
        font-size: #{get('fontSize')};
        float: footnote;
        line-height: 1.2rem;
        padding-left: 0.075rem;
        margin-bottom: 0;
        margin-left: 1.25rem;
        footnote-display: initial;
        footnote-style-position: initial;
        prince-footnote-policy: initial;
        text-indent: initial;
        a {
            color: inherit;
        }
    }
    

    *::footnote-call {
        content: counter(footnote);
        font-size: 83%;
        vertical-align: super;
        line-height: none;
    }

    footnote::footnote-marker, a[data-type="footnote"]::footnote-marker {
        transform: translateY(-20%);
        font-size: 6.5pt;
    }
    `
})
export class Footnote extends TemplateComponent<FootnoteConfiguration, any> { }
