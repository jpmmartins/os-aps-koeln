import { Component, TemplateComponent } from '../../../component';
import { EpubPageConfiguration } from './epub-page.schema';

@Component({
    readme: `
    # EPUB Page setup
    General layout of the page like paper size, margins and typography.
    `,
    styles: `
    html, body {
        @debug "Using font: #{get('page.font.family')} #{get('page.font.size')}";
        font-family: #{get('page.font.family')};
        font-size: #{get('page.font.size')};
        line-height: 1.3; // Maybe this should be a typesetting option
    }

    p {
        margin: 0.1rem 0 0.35rem 0;
    }
    
    `
})
export class EpubPage extends TemplateComponent<EpubPageConfiguration, any> {}
