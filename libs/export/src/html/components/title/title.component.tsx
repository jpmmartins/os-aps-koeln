import { TSX } from '../../../TSX';
import { TemplateComponent, Component } from '../../../component';

@Component({
    readme: `Creates the title page`,
    styles: `
    .coverpage {
        page: coverpage;
        
        h1 {
            display: block;
            margin: auto;
            text-align: center;
        }
    }

    @page coverpage {
        prince-shrink-to-fit: auto;
        counter-reset: page -1;
    }
    `
})
export class Title extends TemplateComponent<{}, any> {
    public renderDOM(): { id: string; dom: HTMLElement; placement?: string | undefined; } | undefined {
        return {
            id: 'title',
            placement: this.page || 'front',
            dom: <section class="coverpage">
                <h1>{this.engine.data?.document?.title}</h1>
            </section>
        }
    }
}
