import { JSDOM } from 'jsdom';
import { TSX } from '../TSX';

import { createLogger, format, transports } from 'winston';
import { DocumentData, DocumentElement, DocumentTables, ExportOptions, SciFlowDocumentData, Toc, TocHeading } from "../interfaces";
import { RenderingEngine } from "../renderers";

import { getDefaults, ReferenceRenderer } from '@sciflow/cite';
import { counterStyle, counterStyleValues, DocumentNode, DocumentSnapshot, DocumentSnapshotImage, DocumentSnapshotResource, HeadingDocumentNode, SFNodeType } from '@sciflow/schema';
import { fetch } from 'cross-fetch';
import { createSectionHierarchy, extractCaptionOrTitle, extractFirstHeading, extractHeadingTitle, extractTitle } from '../helpers';

import { existsSync } from 'fs';
import JSZip from 'jszip';
import { extname } from 'path';
import { parse } from 'url';
import { TemplateComponent } from '../component';
import { renderCss } from '../css';
import defaultTemplateComponents from './components';
import htmlRenderers from './renderers';

const { styleXML, localeXML } = getDefaults();

const dom = new JSDOM();
const virtualDocument = dom.window.document;

const logger = createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { service: 'main' },
    transports: [
        new transports.Console()
    ],
});

/**
 * Returns a renderer that can render a document.
 */
const getRenderer = ({ document, citationRenderer, templateOptions, metaData = {}, metaDataSchema = null, customRenderers = {}, configurations = [] as any[] }): RenderingEngine<HTMLElement, SciFlowDocumentData> => {
    return new RenderingEngine<HTMLElement, SciFlowDocumentData>(
        { ...htmlRenderers, ...customRenderers },
        { document, virtualDocument, citationRenderer, templateOptions, metaData, metaDataSchema, configurations },
        []
    );
}

/**
* Renders a document node (mainly for testing)
* @param documentNode 
* @param parentNode 
*/
const renderNode = (documentNode, parentNode?, opts?: { renderers?}) => {
    const renderer = new RenderingEngine<HTMLElement, any>(
        { ...htmlRenderers, ...(opts?.renderers ?? {}) },
        { document: documentNode, virtualDocument },
        []
    );
    return renderer.render(documentNode, parentNode);
}

/**
* Takes an array with numbering e.g. [1,1,2] for a h3 and returns a string with the values concatenated. (e.g. I.1.1 for upper-roman counter styles)
* Exported for testability. Not an external API.
*/
export const createPrefix = (levels: number[], style: counterStyle, fallback: counterStyle): string => {
    if (style === 'none') { return ''; }
    if (style === 'alpha') { style = 'upper-alpha'; }
    if (style === 'numeric') { style = 'decimal'; }

    return levels.filter(v => v > 0).map((v, i) => {
        if (i > 0) {
            return counterStyleValues['decimal'](v);
        }
        // only number the first level with the style and use decimal below that
        // @ts-ignore TS2538
        return counterStyleValues[style || fallback] ? counterStyleValues[style || fallback](v) : v;

    }).join('.');
}

/**
 * Renders a table of contents.
 */
const generateToc = async (parts: DocumentNode<SFNodeType.document>[], partId?: string, defaultNumbering?: counterStyle, engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>): Promise<Toc> => {

    const numberingFallback = defaultNumbering || 'decimal';

    /** Initializes the counter array with 10 levels */
    const initCounter = (): Array<number> => '0'.repeat(10).split('').map(_ => 0);

    const createNumberedHeadings = async (headingNodes: HeadingDocumentNode[]): Promise<TocHeading[]> => {
        let headings: TocHeading[] = [];
        let activeCounterStyle: counterStyle;
        /** Counters for heading levels counters[0] is the value for h1 */
        let counters = initCounter();

        for (let heading of headingNodes) {
            const headingLevel = heading.attrs?.level || 1;
            const counterStyle = heading.attrs.numbering || numberingFallback;

            if (heading.attrs.level === 1) {
                // when numbering changes, reset the h1 counter
                if (activeCounterStyle !== counterStyle || counterStyle === 'none') { counters = initCounter(); }
                activeCounterStyle = counterStyle;
            }

            // increase the counter for the current heading level
            counters[headingLevel - 1]++;
            // and reset the level for all counters smaller than the current level
            counters = counters.map((value, index) => index >= headingLevel ? 0 : value); // [1,2,4] on level 2 becomes [1,3,1] after increasing h2 to 3 and resetting the rest

            const numberingString = createPrefix(counters, activeCounterStyle, numberingFallback);
            headings.push({
                ...(await extractHeadingTitle(heading, engine)),
                id: heading.attrs.id,
                numberingString,
                level: heading.attrs.level || 1,
                counterStyle,
                partId
            });
        }

        return headings;
    };

    return {
        headings: await createNumberedHeadings(parts.map(part => part?.content?.filter(d => d.type === SFNodeType.heading) as HeadingDocumentNode[] || []).flat())
    };
}

function extractType<SFNodeType>(type: SFNodeType, node: DocumentNode<any>, parent?: DocumentNode<any>, partId?: string): DocumentElement<SFNodeType>[] {
    if (!partId && node.type === SFNodeType.document) { partId = node.attrs.id; }
    let c: DocumentElement<any>[] = [];
    if (node.content) {
        c = [...c, ...node.content.map(n => extractType(type, n, node, partId)).flat()];
    }
    if (node.type === type) {
        let title = extractCaptionOrTitle(node, parent);
        if (parent?.type === SFNodeType.figure) {
            c.push({ type, id: node.attrs?.id, attrs: node.attrs, title, content: node.content, figureId: parent.attrs.id, partId });
        } else {
            c.push({ type, id: node.attrs?.id, attrs: node.attrs, title, content: node.content, partId });
        }
    }

    return c.flat().filter(n => n != null);
}

/** Generates listings of all figures (equations, ..), headings, and 
 * footnotes with their respective numbering
 * @param partId optional partId to be added to elements
 */
export const generateListings = async (content: DocumentNode<any> | DocumentNode<any>[], partId?: string, engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>): Promise<DocumentTables> => {

    if (content instanceof Array) {
        // create a virtual root document with all documents
        content = {
            type: null,
            content: content
        };
    }

    const equations = extractType<SFNodeType.math>(SFNodeType.math, content, undefined, partId);
    const tables = extractType<SFNodeType.table>(SFNodeType.table, content, undefined, partId);
    const codeBlocks = extractType<SFNodeType.code>(SFNodeType.code, content, undefined, partId);
    const figures = extractType<SFNodeType.figure>(SFNodeType.figure, content, undefined, partId);
    const headings = extractType<SFNodeType.heading>(SFNodeType.heading, content, undefined, partId);
    const footnotes = extractType<SFNodeType.footnote>(SFNodeType.footnote, content, undefined, partId);
    const citations = extractType<SFNodeType.citation>(SFNodeType.citation, content, undefined, partId);

    const getAttr = (attrs, key): string | undefined => attrs[key] ? attrs[key] as string : undefined;
    const parts = content.content?.filter(c => c.type === SFNodeType.document).map(d => ({
        id: getAttr(d.attrs, 'id') || 'UNKNOWN_PART',
        title: extractFirstHeading(d),
        type: getAttr(d.attrs, 'type'),
        role: getAttr(d.attrs, 'role'),
        numbering: getAttr(d.attrs, 'numbering')
    })) || [];

    const ids = [...equations, ...tables, ...figures, ...headings, ...footnotes, ...codeBlocks];
    const idMap = ids.reduce((prev, element) => ({ ...prev, [element.id]: element }), {});

    const toc = await generateToc(content?.type === SFNodeType.document ? [content] : content.content?.filter(c => c.type === SFNodeType.document) || [], partId, 'decimal', engine);

    return {
        toc,
        equations,
        tables,
        codeBlocks,
        figures,
        headings,
        footnotes,
        citations,
        parts,
        ids,
        idMap
    };
};

const flattenMetaData = (metaData: any) => {
    const retval = {};
    for (const key in metaData) {
        if (!metaData.hasOwnProperty(key)) { continue; }
        if ((typeof metaData[key]) === 'object' && metaData[key] !== null) {
            const flatObject = flattenMetaData(metaData[key]);
            for (var childKey in flatObject) {
                if (!flatObject.hasOwnProperty(childKey)) { continue; }
                retval[key + '.' + childKey] = flatObject[childKey];
            }
        } else {
            retval[key] = metaData[key];
        }
    }

    return retval;
}

export const generateRootVarsFromMetaData = (metaData) => {
    let values = flattenMetaData(metaData);
    let rootVars = {};
    for (let key in values) {
        if (key === 'id' || key === '_id' || key === '_rev') { continue; }
        rootVars['--' + key.replaceAll('.', '-')] = `"${values[key]}"`;
    }
    return rootVars;
}

/**
 * Renders a document as a ZIP
 * @param exportData 
 * @param options 
 * @returns the ZIP
 */
export const createHTMLZip = async (exportData: DocumentSnapshot, options?: ExportOptions) => {
    const citationRenderer = new ReferenceRenderer(styleXML, localeXML, exportData.references ?? []);
    const configuration = options?.configurations?.find(c => c.kind === 'Configuration');
    let configurations = options?.configurations || [];
    const title = (exportData.title?.document ? extractFirstHeading(exportData.title.document) : undefined) || 'Untitled document';

    const runner = options?.runner || 'html';
    const styles: any[] = [];
    const scripts: any[] = options?.scripts || [];
    let files: DocumentSnapshotResource[] = (exportData.files || [])
        .map(file => {
            if (file.type === 'image') {
                // set a max width for epub exports
                const url = new URL(file.url);
                url.searchParams.append('width', '1024');
                url.searchParams.append('format', 'jpeg');
                file.url = url.toString();
            }
            return { ...file, id: (file.fileId || file.id) as string };
        });

    const customTemplateComponents = options?.customTemplateComponents || [];

    const zip = new JSZip();
    // zip.file('mimetype', 'application/zip');
    const contentDir = zip.folder('assets');

    // download media
    if (!options?.inline && files.some(file => file.type === 'image')) {
        files = await Promise.all(files.map(async (file) => {
            if (file.type === 'image') {
                // make sure to remove any query strings that might still be attached
                const parsedUrl = parse(file.url);
                let ext = extname(parsedUrl.pathname || '');
                if (parsedUrl.query) {
                    const params = new URLSearchParams(parsedUrl.query);
                    if (params.get('format')) {
                        ext = '.' + params.get('format') as string;
                    }
                }
                const source = await fetch(file.url);
                if (source.status === 200) {
                    const content = await source.blob();
                    if (content) {
                        const buffer = await content.arrayBuffer();
                        const filePath = `assets/${file.id}${ext}`;
                        // we save the images to the content dir because apple books showed issues with relative imports
                        if (contentDir?.file(`${file.id}${ext}`, Buffer.from(buffer), { binary: true })) {
                            file.url = filePath;
                            file.mimeType = content.type;
                            return file;
                        }
                    }
                }
            }
            return file;
        }));
    }

    const document: DocumentData = {
        documentId: exportData.documentId,
        title: exportData.title?.document ? extractFirstHeading(exportData.title?.document) : 'Untitled document',
        subtitle: undefined,
        titlePart: exportData.parts.filter(p => p.type === 'title'),
        parts: exportData.parts,
        index: exportData.parts.filter(p => p.type !== 'title').map(p => p.id),
        metaData: {},
        images: (files || []).filter(file => file.type === 'image') as DocumentSnapshotImage[],
        files,
        tables: await generateListings(exportData.parts.map(p => ({
            ...p.document,
            attrs: {
                ...p.document.attrs,
                id: p.document.attrs?.id || p.partId || p.id,
                numbering: p.numbering,
                role: p.role,
                schema: p.schema,
                type: p.type
            }
        }))),
        authors: exportData.authors || [],
        references: exportData.references || []
    }

    // make sure we only process configurations meant for this export
    configurations = configurations?.filter(configuration => {
        if (Array.isArray(configuration.runners)) {
            return configuration.runners.includes(options?.runner);
        }
        return true;
    });

    // the node renderer renders the document tree itself
    const engine: RenderingEngine<HTMLElement, SciFlowDocumentData> = getRenderer({
        document,
        configurations,
        citationRenderer,
        customRenderers: {},
        templateOptions: configuration,
        metaData: options?.metaData,
        metaDataSchema: options?.metaDataSchema
    });

    // we re-generate the tables to have access to titleHTMl elements and other rendered captions/titles
    document.tables = await generateListings(exportData.parts.map(p => ({
        ...p.document,
        attrs: {
            ...p.document.attrs,
            id: p.document.attrs?.id || p.partId || p.id,
            numbering: p.numbering,
            role: p.role,
            schema: p.schema,
            type: p.type
        }
    })), undefined, engine);

    // components can be used to inject DOM into the document (e.g. for the table of contents)
    // or just render CSS (e.g. to style the @page)
    const componentRenderers: any[] = [
        ...defaultTemplateComponents,
        ...customTemplateComponents
    ]
        .reverse()
        .filter(r => typeof r === 'function')
        .map((r: any) => typeof r?.prototype?.constructor !== 'function' ? r(engine) : new r(engine));

    let renderedComponents: { id: string; dom: HTMLElement; placement?: string; }[] = [];

    // go through the template configuration values and create the styles and additional DOM elements
    if (configurations) {
        for (let configuration of configurations) {
            let rendererInstance: TemplateComponent<any, SciFlowDocumentData> = componentRenderers.find(templateComponent => templateComponent.kind === configuration.kind);
            try {
                const style = rendererInstance?.renderStyles({ runner });
                if (style) {
                    styles.push({
                        css: style
                    });
                }
                const dom = rendererInstance?.renderDOM();
                if (dom) { renderedComponents.push(dom); }
            } catch (e) {
                logger.warn('Could not render configuration styles', { message: e.message, configuration });
                debugger;
            }
        }
    }

    // we move the part information into the root of the part document itself
    const parts = exportData.parts.map(part => ({
        ...part.document,
        attrs: {
            ...part.document.attrs,
            id: part.document.attrs?.id || part.partId || part.id,
            numbering: part.numbering,
            role: part.role,
            schema: part.schema,
            type: part.type
        }
    }));

    let rootStyles = generateRootVarsFromMetaData(options?.metaData || {});

    const renderedManuscriptSections: any[] = [];
    for (let part of parts) {
        try {
            // render the parts in order
            renderedManuscriptSections.push({
                partId: part.attrs.id,
                title: extractFirstHeading(part),
                document: part,
                dom: await engine.render(part)
            });
        } catch (e) {
            console.error('Could not render part: ' + e.message, part.attrs);
        }
    }

    // using metaData as root vars
    let rootVariables: any[] = [];
    for (let key in rootStyles) {
        if (rootStyles.hasOwnProperty(key)) {
            rootVariables.push(key + ":" + rootStyles[key])
        }
    }

    if (title && title?.length > 0 && !rootVariables.find(v => v.startsWith('--title'))) {
        rootVariables.push(`--title:"${title}";`);
    }

    rootVariables = rootVariables.toString().replaceAll(/,/g, ";\n") as any;

    const lang = exportData.locale ?? configuration?.spec?.lang ?? 'en-US';

    if (options?.stylePaths && options?.stylePaths?.length > 0) {
        const templateStyles = (await Promise.all((options?.stylePaths || []).map(async (stylePath) => {
            try {
                if (!existsSync(stylePath.path)) {
                    throw new Error('Style file did not exist: ' + stylePath.path);
                }
                const { styles } = await renderCss(stylePath.path, {
                    document: { title: title?.length > 60 ? title?.substring(0, 60) + '...' : title, ...(options?.metaData ?? {}) },
                    configurations: options?.configurations,
                    metaData: options?.metaData ?? {}
                });
                return {
                    css: styles.css,
                    position: stylePath.position || 'start'
                };
            } catch (e) {
                console.error('Failed to render styles', { message: e.message, stylePath: stylePath.path });
                debugger;
                return null;
            }
        }))).filter(v => v && v?.css?.length > 0);

        styles.push(...templateStyles);
    }

    const renderedFrontSections = renderedComponents.filter(c => c.placement === 'front').map(c => c.dom);
    const renderedBodySections = renderedComponents.filter(c => c.placement === 'body').map(c => c.dom);
    const renderedBackSections = renderedComponents.filter(c => c.placement === 'back').map(c => c.dom);

    const firstStyles = styles.filter(style => style.position === 'start');
    const middleStyles = styles.filter(style => !style.position);
    const endStyles = styles.filter(style => style.position === 'end');

    let inlineStyles: any[] = [];
    let cssStyles: string = '';
    if (options?.inline) {
        inlineStyles.push(...firstStyles, ...middleStyles, ...endStyles);
    } else {
        cssStyles = [
            ...firstStyles.filter(style => !style.inline),
            ...middleStyles.filter(style => !style.inline),
            ...endStyles.filter(style => !style.inline)
        ].map(s => s?.css).join('\n\n');
        await zip.file('styles.css', cssStyles);
    }

    const manuscriptHTML = (<html lang={lang}>
        <head>
            <title>{title}</title>
            {options?.baseHref ? <base href={options?.baseHref} target="_blank"></base> : ''}
            {inlineStyles?.length > 0 ? <style>
                {inlineStyles.map(s => s?.css).join('\n\n')}
            </style> : ''}
            {cssStyles?.length > 0 ? <link rel="stylesheet" href="styles.css" /> : ''}
            {scripts.map((script: any) => script.src ? <script src={script.src}></script> : <script>{script.content}</script>)}
        </head>
        <body>
            {renderedFrontSections.length > 0 ? <section class="front">
                {...renderedFrontSections}
            </section> : ''}
            <section class="body">
                {...renderedBodySections}
                {...renderedManuscriptSections.map(s => s.dom)}
            </section>
            {renderedBackSections.length > 0 ? <section class="back">
                {...renderedBackSections}
            </section> : ''}
        </body>
    </html>).outerHTML;

    await zip.file('manuscript.html', manuscriptHTML);

    return await zip.generateAsync({
        type: 'nodebuffer',
        mimeType: 'application/epub+zip'
    });
}

/** Renders a manuscript to HTML using custom renderers
 * @deprecated use createHTMLZip directly to retain the document and the needed assets.
*/
const renderHTMLSinglePart = async (manuscript, {
    styles, scripts, configurations, references, authors, metaData = {}, metaDataSchema = null, customRenderers = {}, customTemplateComponents = [], files, runner = undefined
}) => {

    const document = manuscript.document;
    const citationRenderer = new ReferenceRenderer(styleXML, localeXML, references ?? []);
    const configuration = configurations?.find(c => c.kind === 'Configuration');

    const title = extractTitle(document);

    const data = {
        document: {
            title,
            images: [], // TODO store full resolution images
            ...document,
            files,
            tables: await generateListings(document),
            authors,
            references
        },
        configurations,
        citationRenderer,
        templateOptions: configuration,
        metaData,
        metaDataSchema
    };

    // the node renderer renders the document tree itself
    const engine: RenderingEngine<HTMLElement, SciFlowDocumentData> = getRenderer({
        ...data,
        customRenderers
    });

    // components can be used to inject DOM into the document (e.g. for the table of contents)
    // or just render CSS (e.g. to style the @page)
    const componentRenderers: any[] = [
        ...defaultTemplateComponents,
        ...customTemplateComponents
    ]
        .reverse()
        .filter(r => typeof r === 'function')
        .map((r: any) => typeof r?.prototype?.constructor !== 'function' ? r(engine) : new r(engine));

    let renderedComponents: { id: string; dom: HTMLElement; placement?: string; }[] = [];

    // go through the template configuration values and create the styles and additional DOM elements
    for (let configuration of configurations) {
        let rendererInstance: TemplateComponent<any, SciFlowDocumentData> = componentRenderers.find(templateComponent => templateComponent.kind === configuration.kind);
        const style = rendererInstance?.renderStyles({ runner });
        if (style) {
            styles.push({
                css: style
            });
        }
        const dom = rendererInstance?.renderDOM();
        if (dom) { renderedComponents.push(dom); }
    }

    const sections = createSectionHierarchy(manuscript.document.content.filter(t => t.type !== 'header'));
    let rootStyles = generateRootVarsFromMetaData(metaData);
    const renderedManuscriptSections = await Promise.all(sections.map(section => engine.render(section)));

    // using metaData as root vars
    let rootVariables: any[] = [];
    for (let key in rootStyles) {
        if (rootStyles.hasOwnProperty(key)) {
            rootVariables.push(key + ":" + rootStyles[key])
        }
    }

    if (title && title?.length > 0 && !rootVariables.find(v => v.startsWith('--title'))) {
        rootVariables.push(`--title:"${title}";`);
    }

    rootVariables = rootVariables.toString().replaceAll(/,/g, ";\n") as any;

    const lang = configuration?.locale ?? 'en-US';

    const renderedFrontSections = renderedComponents.filter(c => c.placement === 'front').map(c => c.dom);
    const renderedBodySections = renderedComponents.filter(c => c.placement === 'body').map(c => c.dom);
    const renderedBackSections = renderedComponents.filter(c => c.placement === 'back').map(c => c.dom);

    const firstStyles = styles.filter(style => style.position === 'start');
    const middleStyles = styles.filter(style => !style.position);
    const endStyles = styles.filter(style => style.position === 'end');

    return (<html lang={lang}>
        <head>
            <title>{title}</title>
            <style>
                /* start styles */
                {'\n' + firstStyles.map((s) => s.css).join('\n')}
                /* styles */
                {'\n' + middleStyles.map((s) => s.css).join('\n')}
                /* end styles */
                {'\n' + endStyles.map((s) => s.css).join('\n')}

            </style>
            {scripts.map((script: any) => script.src ? <script src={script.src}></script> : <script>{script.content}</script>)}
        </head>
        <body>
            {renderedFrontSections.length > 0 ? <section class="front">
                {...renderedFrontSections}
            </section> : ''}
            <section class="body">
                {...renderedBodySections}
                {...renderedManuscriptSections}
            </section>
            {renderedBackSections.length > 0 ? <section class="back">
                {...renderedBackSections}
            </section> : ''}
        </body>
    </html>).outerHTML;
}

export {
    renderHTMLSinglePart as renderHTML,
    getRenderer
};
