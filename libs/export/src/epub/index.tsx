import { XMLJSXFactory as TSX, XMLJSXFactory } from '../XMLJSXFactory';
import { TSX as HTMLFactory } from '../TSX';

import { DocumentData, ExportOptions, SciFlowDocumentData } from "../interfaces";
import { NodeRenderer, RenderingEngine } from "../renderers";
import { fetch } from 'cross-fetch';
import { parse } from 'url';
import { createLogger, format, transports } from 'winston';

import { getDefaults, ReferenceRenderer, SourceField } from '@sciflow/cite';

import JSZip from 'jszip';

import { createComponentInstance, TemplateComponent } from '../component';
import { generateListings, generateRootVarsFromMetaData, getRenderer } from '../html';
import defaultTemplateComponents from '../html/components';
import { Author, DocumentNode, DocumentSnapshot, DocumentSnapshotImage, DocumentSnapshotResource, SFNodeType } from '@sciflow/schema';
import { escapeId, extractFirstHeading, wrapTOC } from '../helpers';
import { extname } from 'path';
import { getXRefLabel } from '../html/renderers/inline';
import { renderCss } from '../css';
import { XMLBuilderImpl } from 'xmlbuilder2/lib/builder';

const { styleXML, localeXML } = getDefaults();

const logger = createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { service: 'main' },
    transports: [
        new transports.Console()
    ],
});

/** Renders a manuscript to a EPUB 3 ZIP blob
 * (!) Expects document to be split into parts/chapters already
 */
export const renderEPUB = async (exportData: DocumentSnapshot, options?: ExportOptions) => {

    const citationRenderer = new ReferenceRenderer(styleXML, localeXML, exportData.references ?? []);
    const configuration = options?.configurations?.find(c => c.kind === 'Configuration');
    const configurations = options?.configurations || [];
    const title = (exportData.title?.document ?  extractFirstHeading(exportData.title.document) : undefined) || 'Untitled document';

    const runner = 'epub';
    const styles: any[] = [];
    const scripts: any[] = [];
    let files: DocumentSnapshotResource[] = (exportData.files || [])
        .map(file => {
            if (file.type === 'image') {
                // set a max width for epub exports
                const url = new URL(file.url);
                url.searchParams.append('width', '1024');
                url.searchParams.append('format', 'jpeg');
                file.url = url.toString();
            }
            return { ...file, id: (file.fileId || file.id) as string } as DocumentSnapshotImage;
        });

    const zip = new JSZip();
    zip.file('mimetype', 'application/epub+zip');
    const contentDir = zip.folder('content');
    const customTemplateComponents = options?.customTemplateComponents || [];

    // download media
    if (files.some(file => file.type === 'image')) {
        files = await Promise.all(files.map(async (file) => {
            if (file.type === 'image') {
                // make sure to remove any query strings that might still be attached
                const parsedUrl = parse(file.url);
                let ext = extname(parsedUrl.pathname || '');
                if (parsedUrl.query) {
                    const params = new URLSearchParams(parsedUrl.query);
                    if (params.get('format')) {
                        ext = '.' + params.get('format') as string;
                    }
                }
                const source = await fetch(file.url);
                if (source.status === 200) {
                    const content = await source.blob();
                    if (content) {
                        const buffer = await content.arrayBuffer();
                        const filePath = `${file.id}${ext}`;
                        // we save the images to the content dir because apple books showed issues with relative imports
                        if (contentDir?.file(`${file.id}${ext}`, Buffer.from(buffer), { binary: true })) {
                            file.url = filePath;
                            file.mimeType = content.type;
                            return file;
                        }
                    }
                }
            }
            return file;
        }));
    }

    const document: DocumentData = {
        documentId: exportData.documentId,
        title: exportData.title?.document ? extractFirstHeading(exportData.title?.document) : 'Untitled document',
        subtitle: undefined,
        titlePart: exportData.parts.filter(p => p.type === 'title'),
        parts: exportData.parts,
        index: exportData.parts.filter(p => p.type !== 'title').map(p => p.id),
        metaData: {},
        images: (files || []).filter(file => file.type === 'image') as DocumentSnapshotImage[],
        files,
        tables: await generateListings(exportData.parts.map(p => ({
            ...p.document,
            attrs: {
                ...p.document.attrs,
                id: p.document.attrs?.id || p.partId || p.id,
                numbering: p.numbering,
                role: p.role,
                schema: p.schema,
                type: p.type
            }
        }))),
        authors: exportData.authors || [],
        references: exportData.references || []
    }

    class CitationRenderer extends NodeRenderer<HTMLElement | HTMLSpanElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.citation>) {
            const citation = SourceField.fromString(node.attrs.source);
            if (!citation) {
                return <span style="color: red;">Unreadable reference</span>
            }
            try {
                const rendered = this.engine.data.citationRenderer.renderCitation(citation);
                const citationEl = HTMLFactory.htmlStringToElement(`<a>${rendered.renderedCitation}</a>`) as HTMLLinkElement;
                const part = this.engine.data.document.tables.parts.find(p => p.type === 'bibliography');
                const id = citation.citationItems?.[0]?.id;
                if (part?.id && id) {
                    citationEl.setAttribute('href', `part_${part.id}.xhtml#ref-${escapeId(id)}`);
                } else {
                    return HTMLFactory.htmlStringToElement(`<span>${rendered.renderedCitation}</span>`) as HTMLSpanElement;
                }

                citationEl.setAttribute('backlink', 'doc-backlink');
                citationEl.setAttribute('epub:type', 'backlink');

                return citationEl;
            } catch (e) {
                if (e instanceof Error) {
                    this.engine.log('error', 'Could not render reference ' + citation?.citationItems?.map(i => i.id).join(), { message: e.message, citation });
                }
                return HTMLFactory.htmlStringToElement(` <citation style="color: red;">Missing reference ${citation?.citationItems?.map(i => i.id).join()}</citation>`);
            }
        }
    }

    /** Since cross references may go across files, we need to add the filename to the link */
    class LinkRenderer extends NodeRenderer<HTMLLinkElement, SciFlowDocumentData> {
        async render(node: DocumentNode<any>) {
            const label = getXRefLabel(node, this.engine.data.templateOptions, this.engine.data.document);
            const htmlEl = HTMLFactory.createElement('a', {}, [HTMLFactory.createTextNode(label ?? '')]) as HTMLLinkElement;
            const el = this.engine.data.document.tables.idMap[node.attrs.href?.replace('#', '')];
            if (el && el.partId) {
                htmlEl.setAttribute('href', `part_${el.partId}.xhtml#${el.id}`);
            }

            htmlEl.setAttribute('backlink', 'doc-backlink');
            htmlEl.setAttribute('epub:type', 'backlink');

            return htmlEl;
        }
    }

    const renderedFootnotes: { id: string; dom: any; }[] = [];
    // we render the footnote content here so we can maintain order of references inside footnotes
    class EpubFootnoteRenderer extends NodeRenderer<HTMLLinkElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.footnote>) {
            // the rest of the file renders XML, so switch the renderer
            const index = document.tables.footnotes.findIndex(fn => fn.id === node.attrs.id);
            // since the renderer will give us HTML we convert it into XML
            const footnoteContent = HTMLFactory.createElement('span', {}, await this.engine.renderContent(node));
            renderedFootnotes.push({
                id: node.attrs.id,
                dom: TSX.stringToElement(footnoteContent.outerHTML)
            });

            const link = HTMLFactory.createElement('a', {
                id: `fna${index + 1}`,
                href: `#fn${index + 1}`,
                role: 'doc-noteref',
                ['epub:type']: 'noteref'
            }, HTMLFactory.htmlStringToElement(`<sup class="small">[${index + 1}]</sup>`))
            return link as HTMLLinkElement;
        }
    }

    // the node renderer renders the document tree itself
    const engine: RenderingEngine<HTMLElement, SciFlowDocumentData> = getRenderer({
        document,
        configurations,
        citationRenderer,
        customRenderers: {
            [SFNodeType.footnote]: EpubFootnoteRenderer,
            [SFNodeType.link]: LinkRenderer,
            [SFNodeType.citation]: CitationRenderer
        },
        templateOptions: configuration,
        metaData: options?.metaData,
        metaDataSchema: options?.metaDataSchema
    });

    // components can be used to inject DOM into the document (e.g. for the table of contents)
    // or just render CSS (e.g. to style the @page)
    const componentRenderers: any[] = [
        ...defaultTemplateComponents,
        ...customTemplateComponents
    ]
        .reverse()
        .filter(r => typeof r === 'function')
        .map((r: any) => typeof r?.prototype?.constructor !== 'function' ? r(engine) : new r(engine));

    let renderedComponents: { id: string; dom: HTMLElement; placement?: string; }[] = [];

    // go through the template configuration values and create the styles and additional DOM elements
    if (configurations) {
        for (let configuration of configurations) {
            let rendererInstance: TemplateComponent<any, SciFlowDocumentData> = componentRenderers.find(templateComponent => templateComponent.kind === configuration.kind);
            try {
                const style = rendererInstance?.renderStyles({ runner });
                if (style) {
                    styles.push({
                        css: style
                    });
                }
                const dom = rendererInstance?.renderDOM();
                if (dom) { renderedComponents.push(dom); }
            } catch (e) {
                logger.warn('Could not render configuration styles', { message: e.message, configuration });
            }
        }
    }

    // we move the part information into the root of the part document itself
    const parts = exportData.parts.map(part => ({
        ...part.document,
        attrs: {
            ...part.document.attrs,
            id: part.document.attrs?.id || part.partId || part.id,
            numbering: part.numbering,
            role: part.role,
            schema: part.schema,
            type: part.type
        }
    }));

    let rootStyles = generateRootVarsFromMetaData(options?.metaData || {});

    const renderedManuscriptSections: any[] = [];
    for (let part of parts) {
        // render the parts in order
        const dom = await engine.render(part);
        renderedManuscriptSections.push({
            partId: part.attrs.id,
            title: extractFirstHeading(part),
            document: part,
            dom: TSX.stringToElement(dom.outerHTML?.replaceAll('&nbsp;', '&#160;'))
        });
    }

    // using metaData as root vars
    let rootVariables: any[] = [];
    for (let key in rootStyles) {
        if (rootStyles.hasOwnProperty(key)) {
            rootVariables.push(key + ":" + rootStyles[key])
        }
    }

    if (title && title?.length > 0 && !rootVariables.find(v => v.startsWith('--title'))) {
        rootVariables.push(`--title:"${title}";`);
    }

    rootVariables = rootVariables.toString().replaceAll(/,/g, ";\n") as any;

    const lang = configuration?.locale ?? 'en-US';

    if (options?.stylePaths && options?.stylePaths?.length > 0) {
        const templateStyles = (await Promise.all((options?.stylePaths || []).map(async (stylePath) => {
            try {
                const { styles } = await renderCss(stylePath.path, {
                    document: { title: title.length > 60 ? title.substring(0, 60) + '...' : title, ...(options?.metaData ?? {}) },
                    configurations: options?.configurations,
                    metaData: options?.metaData ?? {}
                });
                return {
                    css: styles.css,
                    position: stylePath.position || 'start'
                };
            } catch (e) {
                console.error(e);
                return null;
            }
        }))).filter(v => v && v?.css?.length > 0);

        styles.push(...templateStyles);
    }

    const renderedFrontSections = renderedComponents.filter(c => c.placement === 'front').map(c => c.dom);
    const renderedBodySections = renderedComponents.filter(c => c.placement === 'body').map(c => c.dom);
    const renderedBackSections = renderedComponents.filter(c => c.placement === 'back').map(c => c.dom);

    const firstStyles = styles.filter(style => style.position === 'start');
    const middleStyles = styles.filter(style => !style.position);
    const endStyles = styles.filter(style => style.position === 'end');
    zip.file('styles.css', [...firstStyles, ...middleStyles, ...endStyles].map(s => s?.css).join('\n\n'));

    const metaInf = zip.folder('META-INF');
    // TODO move these into their own 'renderers'
    metaInf?.file('container.xml', (<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
        <rootfiles>
            <rootfile
                full-path="content/content.opf"
                media-type="application/oebps-package+xml" />
        </rootfiles>
    </container>).end());

    const wrappedTOC = wrapTOC(document.tables.toc?.headings || []);
    const depth = 3;
    const renderHeadings = (headings, level = 0) => level <= depth ? <ol>
        {headings.map(heading => <li>
            {heading.title ? <a href={`${heading.partId ? `part_${heading.partId}.xhtml` : ''}#${heading.id}`}>
                {heading.numberingString ? heading.numberingString : ''}
                {heading.title ? heading.title : ''}
            </a> : ''}
            {heading.content?.length > 0 ? renderHeadings(heading.content, level + 1) : ''}
        </li>)}
    </ol> : '';

    const nav = renderHeadings(wrappedTOC);
    const content = (<package xmlns="http://www.idpf.org/2007/opf" xmlns:epub="http://www.idpf.org/2007/ops"
        version="3.0" xml:lang={lang} unique-identifier={document.documentId}>
        <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:opf="http://www.idpf.org/2007/opf">
            <dc:coverage>Content Documents</dc:coverage>
            <dc:creator>SciFlow</dc:creator>
            <dc:date>2021-05-26</dc:date>
            <dc:identifier id={document.documentId} opf:scheme="uuid">{document.documentId}</dc:identifier>
            {/* <dc:description>A Presentation MathML equation is displayed as part of the XHTML content.</dc:description>
            <dc:identifier id="pub-id">cnt-mathml-support</dc:identifier>
             */}<dc:language>{exportData.locale!}</dc:language>
            <dc:publisher>SciFlow</dc:publisher>
            <dc:title>{document.title}</dc:title>
            <link rel="dcterms:rights"
                href="https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document" />
            <link rel="dcterms:rightsHolder" href="https://www.w3.org" />
            <meta property="dcterms:isReferencedBy">
                https://www.w3.org/TR/epub-rs-33/#confreq-mathml-rs-behavior</meta>
            <meta property="dcterms:isReferencedBy">
                https://www.w3.org/TR/epub-rs-33/#confreq-mathml-rs-render</meta>
            <meta property="dcterms:modified">{(new Date().getUTCDate())}</meta>
        </metadata>
        <manifest>
            <item id="nav" properties="nav" href="nav.xhtml" media-type="application/xhtml+xml" />
            <item id="css" href="../styles.css" media-type="text/css" />
            {parts.map(section => (<item id={'part_' + section?.attrs?.id} properties="mathml" href={'part_' + section?.attrs?.id + '.xhtml'}
                media-type="application/xhtml+xml" />))}
            {files.map((file) => {
                if (file.type === 'image') {
                    return <item id={file.id} href={file.url} media-type={file.mimeType} />
                }
            }).filter(f => f != null)}
        </manifest>
        <spine>
            {parts.map(section => <itemref idref={'part_' + section?.attrs?.id} />)}
        </spine>
    </package>)
            .end({ prettyPrint: true });

    contentDir?.file('content.opf', content);

    const navContent = (<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en">
        <head>
            <title>{title}</title>
        </head>
        <body>
            <nav epub:type="toc">
                <h1>{document.title}</h1>
                {nav}
            </nav>
        </body>
    </html>).end({ prettyPrint: true });
    contentDir?.file('nav.xhtml', navContent);

    for (let section of renderedManuscriptSections) {
        let docEndnotes = '';
        const sectionListings = await generateListings(section.document);
        if (sectionListings.footnotes.length > 0) {
            docEndnotes = <section epub:type="endnotes" role="doc-endnotes">
                {sectionListings.footnotes.map(footnote => {
                    const index = document.tables.footnotes.findIndex(fn => fn.id === footnote.id);
                    const renderedFootnote = renderedFootnotes.find(fn => fn.id === footnote.id)?.dom || '';
                    return <aside id={'fn' + (index + 1)} epub:type="footnote" class="small">
                        <a href={'#fna' + (index + 1)} role="doc-backlink" title={`Go to note reference [${index + 1}]`}><sup>  [{index + 1}]</sup>
                            {renderedFootnote}
                        </a>
                    </aside>;
                })}
            </section>;
        }

        const sectionContent = (<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en">
            <head>
                <title>{section.title}</title>
                <link href="../styles.css" rel="stylesheet" type="text/css" />
            </head>
            <body>
                {section.dom}
                {docEndnotes}
            </body>
        </html>)
            .dtd({ name: 'html' })
            .end({ prettyPrint: true });
        contentDir?.file(`part_${section.partId}.xhtml`, sectionContent);
    }

    return await zip.generateAsync({
        type: 'nodebuffer',
        mimeType: 'application/epub+zip'
    });
};
