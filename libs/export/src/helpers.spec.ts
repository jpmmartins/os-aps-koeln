import { it, describe } from 'mocha';
import { expect } from 'chai';
import { join } from 'path';
import { readFileSync } from 'fs';
import { parsePandocAST } from '../../import/src/public-api';
import { createSectionHierarchy, wrapTOC } from './helpers';
import { generateListings } from './html';

const structuredDocumentPandocJSON = JSON.parse(readFileSync(join(__dirname, './fixtures/structured-document.json'), 'utf-8'));

describe('Helpers', async () => {
    it('Split document into sections and sub-sections', async () => {
        const { doc } = await parsePandocAST(structuredDocumentPandocJSON);
        if (!doc) { throw new Error('Doc cannot be empty'); }
        const jsonObject = doc?.toJSON();
        const sections = createSectionHierarchy(jsonObject.content.filter(t => t.type !== 'header'));
        expect(sections.length).to.equal(3);
        
        const methodsSection = sections.find(section => section.content?.[0]?.content?.[0]?.text === 'Methods');
        expect(methodsSection).to.exist;
        expect(methodsSection?.content?.[2]?.attrs?.level).to.equal(2);
        expect(methodsSection?.content?.[2]?.content?.[0]?.attrs?.level).to.equal(3);
    });
    it('Wraps the table of contents', async () => {
        const { doc } = await parsePandocAST(structuredDocumentPandocJSON);
        const listings = await generateListings(doc?.toJSON());
        const toc = wrapTOC(listings.toc.headings);
        expect(toc.length).to.equal(3);
        const methods = toc.find(s => s.title === 'Methods');
        expect(methods.content.length).to.equal(2);
        expect(methods.content?.[0]?.content[0]?.level).to.equal(3);
    });
});
