import { compileString, SassNumber, SassString, sassNull, SassBoolean, sassTrue, sassFalse } from 'sass';
import { FindFiles } from './css';
import { SciFlowDocumentData } from './interfaces';
import { RenderingEngine } from "./renderers";

interface TemplateComponentOptions {
    /**
     * A readme that explains what the component does to an end user configuring it.
     */
    readme?: string;
    /** **A scss string**
     * 
     * You can use *get(key, defaultValue)* to retrieve values from the configuration.
     */
    styles?: string;
    /**
     * The paths to style files to be used (alternative to using styles)
     */
    /** The component name (otherwiise the class name is used (must match the kind from the configuration)) */
    kind?: string;
    /**
     * A custom DOM renderer that overwrites the default implementation.
     */
    renderDOM?: any;
}

/**
 * Decorator function for template components.
 */
export function Component(options?: TemplateComponentOptions) {
    return (target) => {
        target.prototype.options = options;
        return target;
    }
}

/**
 * A component that emits CSS and possibly DOM into the document.
 */
export class TemplateComponent<ConfigFormat, DataFormat extends SciFlowDocumentData> {
    /** The options set for the component implementation (like styles, name, ..) */
    protected options?: TemplateComponentOptions;

    public setComponentOptions(opts: TemplateComponentOptions) {
        this.options = opts;
        if (opts.renderDOM) {
            this.options.renderDOM = opts.renderDOM.bind(this);
        }
    }

    /** The template configuration values set for this component */
    public get conf(): { spec: ConfigFormat } {
        return this.engine.data.configurations?.find(c => c.kind === (this.options?.kind || this.constructor.name));
    }
    /** The kind of component (equal to the class name of the template component implementation) */
    public get kind(): string { return this.options?.kind || this.constructor.name; };
    /** Gets the page (front, body, ..) the component was created for (empty for all pages) */
    public get page(): string { return (this.conf as any)?.page };

    constructor(
        /** the rendering engine for the actual document data and meta data */
        public engine: RenderingEngine<HTMLElement, SciFlowDocumentData>
    ) { }

    /**
     * Returns the configuration as an object (using the schema, the template configuration and any meta data from the document)
     */
    public getValues(): any {
        const configurationName = this.kind + 'Configuration';
        const paths = getPaths('', this.engine.data.metaDataSchema?.properties?.[configurationName] || {});
        const values = {};
        for (let path of paths) {
            const value = this.getValue(path);
            if (value != undefined) {
                if (typeof value !== 'object') {
                    setValue(path, values, value);
                }
            }
        }
        return values;
    }

    public getValue(key: SassString | string, configuration = this.conf) {
        return getValueForKind(key, configuration, this.engine.data.metaData, this.engine.data.metaDataSchema);
    }

    public renderDOM(): { id: string; dom: HTMLElement; placement?: string; } | undefined {
        return typeof this.options?.renderDOM === 'function' ? this.options?.renderDOM() : undefined;
    }

    /** Renders the component's CSS styles */
    public renderStyles(defaults?: { runner }): string {

        const styles = this.options?.styles;

        const importers = [new FindFiles()];
        const functions: any = {
            'isRunner($name)': ([runner]) => {
                console.log(runner?.toString(), (runner?.toString() === `"${defaults?.runner}"`) ? sassTrue : sassFalse);
                return (runner?.toString() === `"${defaults?.runner}"`) ? sassTrue : sassFalse;
            },
            'get($key, $defaultValue: null, $kind: null)': ([key, defaultValue, kind]) => {
                //FIXME: handle defaultValue as a sassString
                if (defaultValue === sassNull) {
                    defaultValue = null;
                }
                const configuration = this.engine.data.configurations?.find(c => c.kind === (kind.text || this.constructor.name));
                if (!configuration) { return defaultValue; }
                
                const retval = this.getValue(key, configuration) || defaultValue;
                if (!retval) {
                    throw new Error("No value for " + key?.toString());
                }

                // TODO we should respect the value type
                if (Number.isInteger(retval)) {
                    return new SassNumber(retval);
                } else if (Number.isFinite(retval)) {
                    return new SassNumber(retval);
                }
                return new SassString(retval);
            },
            'getDocumentValue($key)': ([key]) => {
                const keyValue = (key as SassString).toString();
                if (!document[keyValue]) {
                    debugger;
                    throw new Error('No value for ' + keyValue);
                }
                const retval = new SassString(document[keyValue]);
                return retval;
            }
        };

        return styles ? compileString(styles, { importers, functions })?.css : '';
    }
}

/** Gets a value either from the meta data or the schema defaults */
export const getValueOrDefault = (path: string, metaData = {}, schema = {}) => {
    const defaultValue = getValueFromSchema(path, schema || {});
    const value = getValue(path, metaData || {});
    if (value) {
        return value;
    }

    return defaultValue?.default;
}

/** Gets a value from an object if provided with a path (e.g. object.key.key2 ) */
const getValue = (path: string, object: any) => {
    if (!path || !object) { return undefined; }
    if (object[path]) { return object[path]; }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object[segments[0]] === undefined) { return undefined; }
    return getValue(segments.slice(1, segments.length).join('.'), object[segments[0]]);
}

/** Gets a value in an object if provided with a path (e.g. object.key.key2 ) */
const setValue = (path: string, object: any, value: any) => {
    if (!path) { return object; }

    const segments = path.split('.');
    if (segments.length === 1) {
        if (!object) { object = {}; }
        object[path] = value;
        return object;
    } else {
        object[segments[0]] = setValue(segments.slice(1, segments.length).join('.'), object[segments[0]] || {}, value);
        return object;
    }
}

export const getValueForKind = (key: SassString | string, configuration, metaData, metaDataSchema) => {
    try {
        const lookup = (key instanceof SassString ? key.toString() : key)?.replace(/"/gi, '',);

        const configurationName = configuration?.kind + 'Configuration';
        if (!configuration) { return undefined; }

        // read defaults from the meta data schema (and set a fallback if there, priority #3)
        const schema = getValueFromSchema(lookup, metaDataSchema?.properties?.[configurationName] || {});
        // get the default from the configuration (priority #2)
        const configurationValue = getValue(lookup, configuration.spec);
        // get the any user provided values (priority #1)
        const userSetMetaDataValue = getValue(configurationName + '.' + lookup, metaData || {});
        return userSetMetaDataValue || configurationValue || schema?.default;
    } catch (e) {
        debugger;
        console.error('Could not extract value for ' + key, { message: e.message });
    }
}

/** Gets a value from an object if provided with a path (e.g. object.key.key2 ) */
const getValueFromSchema = (path: string, object: any) => {
    if (!path || !object) { return undefined; }
    if (object?.properties?.[path]) { return object?.properties[path]; }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object?.properties?.[segments[0]] === undefined) { return undefined; }
    return getValueFromSchema(segments.slice(1, segments.length).join('.'), object?.properties[segments[0]]);
}

const getPaths = (prefix: string, schema): string[] => {
    let paths: string[] = [];
    if (!schema?.properties) { return [prefix]; }
    for (let key of Object.keys(schema.properties)) {
        //obj[key] = { ...(obj[key]  || {}), ...deepMerge(prefix + '.' + key, schema.properties[key]) };
        paths = [...paths, ...getPaths((prefix ? prefix + '.' : prefix) + key, schema.properties[key])];
    }

    return [prefix, ...paths];
}

export const mini = (styles: string, dom: any) => {
    return {
        styles,
        dom
    }
}

export const createComponentInstance = (options: TemplateComponentOptions) => (engine) => {
    const comp = new TemplateComponent(engine);
    comp.setComponentOptions(options);
    return comp;
}