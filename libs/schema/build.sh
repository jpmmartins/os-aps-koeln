#!/bin/sh

rm -rf dist

tsc --module commonjs --outDir dist/cjs
tsc --module es2015 --outDir dist/mjs

cat >dist/cjs/package.json <<!EOF
{
    "type": "commonjs"
}
!EOF

cat >dist/mjs/package.json <<!EOF
{
    "type": "module"
}
!EOF

cp package.json dist/
