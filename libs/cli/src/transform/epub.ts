#!/usr/bin/env yarn run ts-node

import { renderEPUB } from '@sciflow/export';
import { loadTemplate } from '@sciflow/tdk';
import { existsSync, readFileSync, writeFileSync } from 'fs';
import { join } from 'path';

const templateDir = join(__dirname, 'fixtures', 'templates');
const componentDir = join(__dirname, '..', '..', '..', 'libs', 'export', 'src', 'html');

/**
 * Generates a EPUB file.
 */
export const generateEPUB = async (source, target?, opts: { templateDir, template } = { templateDir, template: 'epub' }) => {

    if (!existsSync(source)) {
        throw new Error('Source file did not exist');
    }

    const documentSnapshot = JSON.parse(readFileSync(source, 'utf-8'));
    console.time('read template');
    const componentPath = join(__dirname, '..', '..', '..', 'export', 'src', 'html');
    const template = await loadTemplate({ projectId: undefined, templateId: opts.template }, { templateDir: opts?.templateDir, componentPath, fontPath: undefined, fontUrlPrefix: undefined });
    console.timeEnd('read template');

    const parts = documentSnapshot.parts.map(part => ({
        ...part,
        id: part.partId || part.id
    }));
    const references = Object.keys(documentSnapshot.references || {}).map(id => documentSnapshot.references[id]);
    const document = await renderEPUB(
        {
            documentId: 'test',
            ...documentSnapshot,
            parts,
            title: 'Test',
            references
        },
        {
            configurations: template.configurations,
            metaData: documentSnapshot.metaData,
            metaDataSchema: template.metaData,
            customTemplateComponents: template.customTemplateComponents,
            customRenderers: template.customRenderers,
            stylePaths: template.stylePaths || [],
            scripts: []
        });

    writeFileSync(target, document);
    console.log('Wrote epub ' + target);
};
