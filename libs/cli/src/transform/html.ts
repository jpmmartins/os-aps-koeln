#!/usr/bin/env yarn run ts-node

import { createHTMLZip } from '@sciflow/export';
import { loadTemplate } from '@sciflow/tdk';
import { copyFileSync, existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import JSZip from 'jszip';
import { dirSync } from 'tmp';
import Prince from 'prince';

const templateDir = join(__dirname, 'fixtures', 'templates');
const componentDir = join(__dirname, '..', '..', '..', 'libs', 'export', 'src', 'html');

/**
 * Generates a ZIP file of the HTML.
 */
export const generateHTMLZip = async (source, target?, opts: { templateDir, template, fontDir, fontUrlPrefix } = { templateDir, template: 'monograph', fontDir: undefined, fontUrlPrefix: undefined }) => {

    if (!existsSync(source)) {
        throw new Error('Source file did not exist at' + source);
    }

    const documentSnapshot = JSON.parse(readFileSync(source, 'utf-8'));
    const componentPath = join(__dirname, '..', '..', '..', 'export', 'src', 'html');
    const template = await loadTemplate({ projectId: undefined, templateId: opts.template }, { templateDir: opts?.templateDir, componentPath, fontPath: opts.fontDir, fontUrlPrefix: opts.fontUrlPrefix });

    const parts = documentSnapshot.parts.map(part => ({
        ...part,
        id: part.partId || part.id
    }));
    const references = Object.keys(documentSnapshot.references || {}).map(id => documentSnapshot.references[id]);
    const document = await createHTMLZip(
        {
            documentId: 'test',
            ...documentSnapshot,
            parts,
            title: 'Test',
            references
        },
        {
            configurations: template.configurations,
            metaData: documentSnapshot.metaData,
            metaDataSchema: template.metaData,
            customTemplateComponents: template.customTemplateComponents,
            customRenderers: template.customRenderers,
            stylePaths: template.stylePaths || [],
            scripts: []
        });

    if (target) { writeFileSync(target, document); }

    return document;
};

/**
 * Generates HTML output.
 */
export const generateHTML = async (source, target = Date.now() + '.html', opts?: { templateDir, template, fontDir, fontUrlPrefix }) => {
    const file = await generateHTMLZip(source, undefined, opts);
    const zip = new JSZip();
    const content = await zip.loadAsync(file);
    const manuscript = await content.file('manuscript.html')?.async('string');
    if (manuscript) {
        console.log('Writing ', target);
        writeFileSync(target, manuscript);
    }
};

export const generateHTMLPDF = async (source, target = Date.now() + '.html', opts?: { templateDir, template, fontDir, fontUrlPrefix }) => {
    const dir = dirSync({ unsafeCleanup: true });

    const file = await generateHTMLZip(source, undefined, opts);
    const zip = new JSZip();
    const content = await zip.loadAsync(file);

    console.log('Writing to temp dir ', dir.name);
    for (let path of Object.keys(content.files)) {
        const file = await content.file(path)?.async('array');
        if (file) {
            console.log('Writing ' + join(dir.name, path))
            writeFileSync(join(dir.name, path), Buffer.from(file));
        } else {
            mkdirSync(join(dir.name, path), { recursive: true });
        }
    }

    const manuscript = await content.file('manuscript.html')?.async('string');
    if (manuscript) {
        console.log('Writing ', target);
        writeFileSync(target, manuscript);
    }

    await Prince()
        .inputs(join(dir.name, 'manuscript.html'))
        .output(join(dir.name, 'manuscript.pdf'))
        .execute();

    if (!existsSync(join(dir.name, 'manuscript.pdf'))) {
        throw new Error('Could not find pdf file');
    }

    copyFileSync(join(dir.name, 'manuscript.pdf'), target);

    //dir.removeCallback();
};
