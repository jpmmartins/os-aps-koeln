import { program } from 'commander';
import { existsSync, watchFile, watch as watchDir, readdirSync } from 'fs';
import { debounceTime, Subject, takeUntil } from 'rxjs';
import { generateEPUB } from './transform/epub';

import { generateHTMLZip, generateHTML, generateHTMLPDF } from './transform/html';
import { createLogger, format, transports } from 'winston';
import { exportToSnapshot } from './snapshot';

const logformat = format.printf(({ level, message, label, timestamp }) => `${timestamp} [${level}]: ${message}`);
const logger = createLogger({
    level: 'info',
    format: format.combine(format.timestamp(), logformat),
    defaultMeta: { service: 'cli' },
    transports: [
        new transports.Console()
    ],
});

const stop$ = new Subject();
let watcher;

process.on('SIGTERM', () => {
    stop$.next(null);
    logger.warn('Terminating ..')
    watcher?.removeAllListeners();
});

program
    .name('sciflow')
    .command('transform')
    .description('transforms a document using a template')
    .option('-t, --target <file>', 'The output filename/path')
    .option('-s, --source <file>', 'Location of a SciFLow json snapshot file')
    .option('-f, --format <format>', 'epub, pdf, or html')
    .option('-td, --template-dir <path>', 'The path to the template directory')
    .option('-fd, --font-dir <path>', 'Path to where font files are downloaded')
    .option('-tpl, --template <slug>', 'The template slug to use')
    .option('-w, --watch', 'Whether to watch for changes in the template or document')
    .option('-f, --force', 'Overwrite existing files')
    .action(async ({ source, target, watch, force, format = 'html', templateDir, template, fontDir }, options) => {
        console.table({ __dirname, source, target, watch, force, format, templateDir, fontDir, template });
        if (!existsSync(source)) { throw new Error('Source file does not exist at ' + source); }
        if (existsSync(target) && !force) { program.error('Target file already existed, use --force to overwrite.') }

        // we use the global path and serve from the hard drive
        const fontUrlPrefix = fontDir;

        const updates$ = new Subject<string>();
        const generate = async () => {
            try {
                switch (format) {
                    case 'html':
                        if (target.endsWith('.html')) {
                            await generateHTML(source, target, { template, templateDir, fontDir, fontUrlPrefix });
                        } else {
                            await generateHTMLZip(source, target, { template, templateDir, fontDir, fontUrlPrefix });
                        }
                        break;
                    case 'epub':
                        await generateEPUB(source, target, { template, templateDir });
                        break;
                    case 'pdf':
                        await generateHTMLPDF(source, target, { template, templateDir, fontDir, fontUrlPrefix });
                }
            } catch (e) {
                program.error(e.message, { exitCode: 1, code: 'Processing error' });
            }
        }

        if (watch) {
            watchFile(source, async (_curr, _prev) => { updates$.next(source); });
            if (templateDir) { watchDir(templateDir, { recursive: true }, async (_event, filename) => { updates$.next(filename); }); }
            logger.info('Watching files for changes ..');
        }

        updates$.pipe(debounceTime(250), takeUntil(stop$)).subscribe(async (path) => {
            logger.info('Files changed for ' + path + ' ..');
            await generate();
        });

        updates$.next(source);
    });

program
    .command('snapshot')
    .description('Handles SciFlow snapshots')
    .option('-t, --target <file>', 'The output filename/path for the snapshot')
    .option('-s, --source <file>', 'An import file from SFO/OS-APS to transform')
    .option('-w, --watch', 'Whether to watch for changes in the template or document')
    .option('-f, --force', 'Overwrite existing files')
    .action(async ({ source, target }) => {
        console.table({ source, target });
        exportToSnapshot(source, target);
    });

program.parse();
