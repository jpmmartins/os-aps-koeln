# CLI

To continuously produce an epub from a template and document:

```
./sfo transform \
    -s tdk/src/e2e/fixtures/thesis-mockup-en.snapshot.json \
    -t tdk/src/e2e/test.epub \
    --format=epub \
    --template-dir=tdk/src/e2e/fixtures/templates \
    --template=basic-epub \
    --font-dir=${PWD}/fonts \
    --watch \
    --force
```

To display the epub, you can open a http server in tdk/src/e2e and open epubjs.html.
It will attempt to load the epub at localhost:8080/test.epub

Example for a single HTML file:
```
./sfo transform \
    --source=tdk/src/e2e/fixtures/thesis-mockup-en.snapshot.json \
    --target=tdk/src/e2e/test.html \
    --format=html \
    --template-dir=tdk/src/e2e/fixtures/templates \
    --font-dir=${PWD}/fonts \
    --template=monograph \
    --watch \
    --force
```

Example for a single PDF file:
```
./sfo transform \
    --source=tdk/src/e2e/fixtures/thesis-mockup-en.snapshot.json \
    --target=tdk/src/e2e/test.pdf \
    --format=pdf \
    --template-dir=tdk/src/e2e/fixtures/templates \
    --font-dir=${PWD}/local-fonts \
    --template=monograph \
    --watch \
    --force
```

Example to transform a sfo manuscript into a SciFlow snapshot.
```
./sfo snapshot \
    --source=tdk/src/e2e/fixtures/moby-dick.sfo.snapshot.json \
    --target=tdk/src/e2e/outfiles/moby-dick.snapshot.json \
    --watch \
    --force
```
