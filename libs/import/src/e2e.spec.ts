import { it, describe } from 'mocha';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import { pandocRenderers, renderContent } from './pandoc-renderers';
import { schemas } from '@sciflow/schema';
import { assert } from 'chai';

import { JSDOM } from 'jsdom';
const dom = new JSDOM('<!DOCTYPE html>');
import { DOMSerializer } from 'prosemirror-model';
const document = dom.window.document;

const e2eJSON = JSON.parse(readFileSync(join(__dirname, './fixtures/e2e.pandoc.json'), 'utf-8'));

describe('E2E Importer', async () => {
    // test file created through: pandoc --extract-media assets --from docx+styles --to json -s e2e.docx -o e2e.pandoc.json
    it('Parses the entire file', async () => {
        const fragments: any = [];
        let content;
        try {
            content = await renderContent(
                e2eJSON.blocks,
                {
                    renderers: pandocRenderers,
                    schema: schemas.manuscript,
                    assets: join(__dirname, 'fixtures')
                }
            );
        } catch (e) {
            console.error(e);
            assert.fail('Should not throw');
        }
        for (let node of content) {
            try {
                const serializer = DOMSerializer.fromSchema(schemas.manuscript);
                fragments.push(serializer.serializeNode(node, { document }));
            } catch (e) {
                console.error(e, node);
                throw new Error('Could not parse fragment');
            }
        }
        const output = fragments
            .map(o => o.outerHTML)
            .join('\n');

        writeFileSync(join(__dirname, './fixtures/e2e.html'), `<html><body>${output}</body></html>`);
    });
});
