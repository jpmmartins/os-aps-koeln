import { it, describe } from 'mocha';
import { expect } from 'chai';
import { readFileSync } from 'fs';
import { join } from 'path';

import { parsePandocAST } from './pandoc-to-pm';
import { SFNodeType, schemas } from '@sciflow/schema';
import { pandocRenderers, render, PandocNode } from './pandoc-renderers';

import { JSDOM } from 'jsdom';
const dom = new JSDOM('<!DOCTYPE html>');
import { DOMSerializer } from 'prosemirror-model';
const document = dom.window.document;

const simplePandocJSON = JSON.parse(readFileSync(join(__dirname, './fixtures/sample.pandoc.json'), 'utf-8'));
const pandocJSONTestFile = JSON.parse(readFileSync(join(__dirname, './fixtures/test-docx.json'), 'utf-8'));
const titleJSONTestFile = JSON.parse(readFileSync(join(__dirname, './fixtures/title-doc.json'), 'utf-8'));

/** Render a pandoc node as HTML by first converting it into a ProseMirror node,
 * which is then serialized as HTML. */
export const renderAsHtml = async (pandocNode: PandocNode) => {
    const proseMirrorNode = await render(
        pandocNode,
        { renderers: pandocRenderers, schema: schemas.manuscript }
    );
    const serializer = DOMSerializer.fromSchema(schemas.manuscript);
    const htmlFrag = serializer.serializeNode(proseMirrorNode, { document }) as HTMLElement;
    return htmlFrag.outerHTML;
}

describe('Importer', async () => {
    it('Reads the title of a document', async () => {
        const { doc, title } = await parsePandocAST(titleJSONTestFile);
        expect(title).to.equal('Test title');
    });

    it('imports from Pandoc (simple document)', async () => {
        const { doc } = await parsePandocAST(simplePandocJSON);
        const result = doc?.toString();
        expect(result).to.equal(
            `doc(header(heading("Untitled document")), paragraph("Test document"), paragraph("A sample paragraph."), heading("A heading 1"), paragraph("Another paragraph."))`
            , 'Document content is correct');

        //const pmDocument = doc.toJSON();
        //expect(pmDocument.content[0].attrs.id).to.equal('h1-id-1');
    });

    it('Transforms strong', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Strong",
                "c": [
                    {
                        "t": "Str",
                        "c": "bold"
                    }
                ]
            },
            {
                "t": "Space"
            },
            {
                "t": "Str",
                "c": "text"
            }]
        };

        const pmPara = await render(pandocPara, { renderers: pandocRenderers, schema: schemas.manuscript });
        const paraJSON = pmPara.toJSON();

        expect(paraJSON.type).to.equal(SFNodeType.paragraph);
        expect(paraJSON.content.length).to.equal(2);
        expect(paraJSON.content[0].text).to.equal('bold');
        expect(paraJSON.content[0].marks[0].type).to.equal('strong');
        expect(paraJSON.content[1].text).to.equal(' text');
    });

    it('Transforms emphasis', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Str",
                "c": "Behold!"
            }, {
                "t": "Space"
            }, {
                "t": "Emph",
                "c": [{
                    "t": "Str",
                    "c": "Emphasized"
                }, {
                    "t": "Space"
                }, {
                    "t": "Str",
                    "c": "text!"
                }]
            }]
        };

        const pmPara = await render(pandocPara, { renderers: pandocRenderers, schema: schemas.manuscript });
        const paraJSON = pmPara.toJSON();

        // this line is equivalent to the following more specific tests
        expect(await renderAsHtml(pandocPara)).to.equal('<p>Behold! <em>Emphasized text!</em></p>');

        expect(paraJSON.type).to.equal(SFNodeType.paragraph);
        expect(paraJSON.content.length).to.equal(2);
        // While the topmost pandoc JSON has three elements, the converted only
        // has two, as spaces are merged to the text.
        expect(paraJSON.content[1].text).to.equal('Emphasized text!');
        expect(paraJSON.content[1].marks[0].type).to.equal('em');
        expect(paraJSON.content[0].text).to.equal('Behold! ');
    });

    it('toggles emphasis when nested', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Emph",
                "c": [{
                    "t": "Str",
                    "c": "This"
                }, {
                    "t": "Space"
                }, {
                    "t": "Emph",
                    "c": [{
                        "t": "Str",
                        "c": "is"
                    }]
                }, {
                    "t": "Space"
                }, {
                    "t": "Str",
                    "c": "nice!"
                }]
            }]
        };
        expect(await renderAsHtml(pandocPara)).to.equal(
            '<p><em>This </em>is<em> nice!</em></p>'
        );
    });

    it('Overlapping emphasis/strong emphasis', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Str",
                "c": "Witness!"
            }, {
                "t": "Space"
            }, {
                "t": "Emph",
                "c": [{
                    "t": "Strong",
                    "c": [{
                        "t": "Str",
                        "c": "Strongly"
                    }]
                }, {
                    "t": "Space"
                }, {
                    "t": "Str",
                    "c": "emphasized"
                }, {
                    "t": "Space"
                }, {
                    "t": "Str",
                    "c": "text!"
                }]
            }]
        };
        expect(await renderAsHtml(pandocPara)).to.equal(
            '<p>Witness! <em><strong>Strongly</strong> ' +
            'emphasized text!</em></p>'
        );
    });

    it('flattens nested superscripts', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Str",
                "c": "n"
            }, {
                "t": "Superscript",
                "c": [{
                    "t": "Str",
                    "c": "a"
                }, {
                    "t": "Superscript",
                    "c": [{
                        "t": "Str",
                        "c": "2"
                    }]
                }]
            }]
        };
        expect(await renderAsHtml(pandocPara)).to.equal(
            '<p>n<sup>a2</sup></p>'
        );
    });

    it('allows subscript in superscript', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Str",
                "c": "A"
            }, {
                "t": "Superscript",
                "c": [{
                    "t": "Str",
                    "c": "b"
                }, {
                    "t": "Subscript",
                    "c": [{
                        "t": "Str",
                        "c": "2"
                    }]
                }]
            }]
        };
        expect(await renderAsHtml(pandocPara)).to.equal(
            '<p>A<sup>b<sub>2</sub></sup></p>'
        );
    });

    it('converts SoftBreak into a space', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                t: 'Str',
                c: "Don't"
            }, {
                t: 'SoftBreak'
            }, {
                t: 'Str',
                c: 'break!'
            }]
        }
        expect(await renderAsHtml(pandocPara)).to.equal(
            "<p>Don't break!</p>"
        );
    })

    // TODO: should be treated as a <br>, not as a space.
    it('accepts LineBreak as whitespace', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                t: 'Str',
                c: "Do"
            }, {
                t: 'LineBreak'
            }, {
                t: 'Str',
                c: 'break!'
            }]
        }
        expect(await renderAsHtml(pandocPara)).to.equal(
            "<p>Do<br>break!</p>"
        );
    })

    it('Transforms links', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Link",
                "c": [
                    ["", [], []],
                    [{ t: "Str", c: "Linkin" },
                    { t: "Space" },
                    { t: "Str", c: "Park" },
                    ],
                    ["https://linkinpark.com", "rocks"]
                ]
            }]
        };

        const expectedHtml = '<p><a href="https://linkinpark.com" title="rocks">Linkin Park</a></p>'
        expect(await renderAsHtml(pandocPara)).to.equal(expectedHtml);
    });

    it('Transforms links with emphasized text', async () => {
        const pandocPara = {
            t: 'Para',
            c: [{
                "t": "Link",
                "c": [
                    ["", [], []],
                    [{
                        t: "Emph",
                        c: [{ t: "Str", c: "Looook!" }]
                    }],
                    ["https://example.com", ""]
                ]
            }]
        };
        const expectedHtml = '<p><a href="https://example.com"><em>Looook!</em></a></p>'
        expect(await renderAsHtml(pandocPara)).to.equal(expectedHtml);
    });

    it('imports bullet lists', async () => {
        const pandocBulletList = {
            t: 'BulletList',
            c: [[{ t: 'Para', c: [{ t: 'Str', c: "Item 1" }] },
            { t: 'Para', c: [{ t: 'Str', c: "Second paragraph" }] }],
            [{ t: 'Plain', c: [{ t: 'Str', c: "Item 2" }] }],
            ]
        };
        const expectedHtml = '<ul>' +
            '<li><p>Item 1</p><p>Second paragraph</p></li>' +
            '<li><p>Item 2</p></li>' +
            '</ul>';
        expect(await renderAsHtml(pandocBulletList)).to.equal(expectedHtml);
    });

    it('imports nested bullet lists', async () => {
        const pandocBulletList = {
            t: 'BulletList',
            c: [[{ t: 'Para', c: [{ t: 'Str', c: "Item 1" }] }],
            [{
                t: 'BulletList',
                c: [[{ t: 'Plain', c: [{ t: 'Str', c: "Item 2.1" }] }]]
            }]
            ]
        };
        const expectedHtml = '<ul>' +
            '<li><p>Item 1</p></li>' +
            '<li><ul><li><p>Item 2.1</p></li></ul></li>' +
            '</ul>';
        expect(await renderAsHtml(pandocBulletList)).to.equal(expectedHtml);
    });

    it('Imports titles', async () => {
        const { title } = await parsePandocAST(pandocJSONTestFile);
        expect(title).to.equal('Document title');
    });

    it('Imports block quotes', async () => {
        const pandocBlockQuote = {
            "t": "BlockQuote",
            "c": [
                {
                    "t": "Para",
                    "c": [
                        {
                            "t": "Str",
                            "c": "The"
                        },
                        {
                            "t": "Space"
                        },
                        {
                            "t": "Str",
                            "c": "quote"
                        }
                    ]
                }
            ]
        };
        expect(await renderAsHtml(pandocBlockQuote)).to.equal(`<blockquote><p>The quote</p></blockquote>`);
    });

    it('Imports footnotes', async () => {
        const { doc } = await parsePandocAST(pandocJSONTestFile);
        let footnoteFound = false;
        doc?.descendants((node, _pos, parent) => {
            if (node.type.name === SFNodeType.footnote) {
                // TODO test hard_break
                // console.log(node);
                footnoteFound = true;
            }
        });

        expect(footnoteFound).to.be.true;
    });

    it('imports headings', async () => {
        const pandocHeader = {
            t: 'Header',
            c: [1,
                ["hello", ["unnumbered"], []],
                [{ "t": "Str", "c": "hello" }]]
        }
        const expectedHtml = '<h1 data-level="1" data-type="chapter" data-role="" data-numbering="">hello</h1>';
        expect(await renderAsHtml(pandocHeader)).to.equal(expectedHtml);
    });

    it('imports second-level heading', async () => {
        const pandocHeader = {
            t: 'Header',
            c: [2,
                ["moin", [], []],
                [{ "t": "Str", "c": "Moin!" }]]
        }
        const expectedHtml = '<h2 data-level="2" data-type="chapter" data-role="" data-numbering="">Moin!</h2>';
        expect(await renderAsHtml(pandocHeader)).to.equal(expectedHtml);
    });

    it('Imports Math', async () => {
        const tex = "x = \\frac{- b \\pm \\sqrt{b^{2} - 4\\text{ac}}}{2a}";
        const math = {
            "t": "Math",
            "c": [
                {
                    "t": "DisplayMath"
                },
                tex 
            ]
        };

        expect(await renderAsHtml(math)).to.equal(`<math data-tex="${tex}" data-style="inline"></math>`);
    });
});
