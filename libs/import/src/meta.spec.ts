import { it, describe } from 'mocha';
import { expect } from 'chai';
import { readFileSync } from 'fs';
import { join } from 'path';

import { extractReferences, parsePandocAST } from './pandoc-to-pm';
import { renderAsHtml } from './import.spec';

const zoteroJSON = JSON.parse(readFileSync(join(__dirname, './fixtures/zotero.pandoc.json'), 'utf-8'));

describe('Zotero importer', async () => {
    it('imports references created by the Word Zotero plugin', async () => {

        const references = await extractReferences(zoteroJSON.meta);

        expect(references.length).to.equal(2);
        const id272 = references.find(({ id }) => id === '272');
        expect(id272).to.exist;
        expect(id272.type).to.equal('book');
        expect(id272.author?.length).to.equal(1);
        expect(id272.author[0].family).to.equal('Smith');
        
        const id273 = references.find(({ id }) => id === '273');
        expect(id273).to.exist;
    });

    it('detects in text citations', async () => {
        const result = await parsePandocAST(zoteroJSON);
        expect(result.references?.length).to.equal(2);
        const html1 = await renderAsHtml(zoteroJSON.blocks[0]);
        expect(html1).to.equal(
            '<p><cite data-source="%5B%7B%22id%22:%22273%22%7D,%7B%22id%22:%22272%22%7D%5D" data-style="apa">(Jones, 1999; Smith, 2000)</cite> and the same book again <cite data-source="%5B%7B%22id%22:%22273%22%7D%5D" data-style="apa">(Jones, 1999)</cite></p>'
        );
        const html2 = await renderAsHtml(zoteroJSON.blocks[1]);
        expect(html2).to.equal(
            '<p><cite data-source="%5B%7B%22id%22:%22272%22,%22prefix%22:%22see%22,%22suffix%22:%22,%20chapter%2022%20and%20others%22%7D%5D" data-style="apa">(see Smith, 2000, Chapter 22 and others)</cite></p>'
        );
    });
});
