import { PandocRenderer, render, renderContent } from ".";
import * as Pandoc from './pandoc-types';

export const stringRenderers = {
    Str: async (text) => text,
    Space: async () => ' ',
    Strong: async (inlines, opts) => `*${(await renderContent(inlines, opts)).join('')}*`,
    Emph: async (inlines, opts) => `**${(await renderContent(inlines, opts)).join('')}**`,
    SoftBreak: async () => ' ',
    LineBreak: async () => ' ',
    Span: async (contents: [Pandoc.Attr, Pandoc.Inline[]], opts) => {
        return `${(await renderContent(contents[1], opts)).join('')}`;
    },
    // we purposefully do not transform sub- and superscript for meta data
    Subscript: async (inlines, opts) => `${(await renderContent(inlines, opts)).join('')}`,
    Superscript: async (inlines, opts) => `${(await renderContent(inlines, opts)).join('')}`,
    Para: async (contents: Pandoc.Inline[], opts) => `${(await renderContent(contents, opts, contents)).join('\n')}`,
};

/**
 * Takes a pandoc meta data node and returns JSON or a primitive type.
 */
export const renderers: { [pandocNodeName: string]: PandocRenderer } = {
    MetaList: async (metaListContent: Pandoc.MetaMap[], opts) => {
        const children = await renderContent(metaListContent, opts);
        return children;
    },
    MetaMap: async (metaMapContent: Pandoc.MetaMapContent, opts) => {
        let val = {};
        for (let key of Object.keys(metaMapContent)) {
            val[key] = await render(metaMapContent[key], opts);
        }
        return val;
    },
    MetaString: async (metaStringContent: string) => {
        return metaStringContent;
    },
    MetaInlines: async (inlines: Pandoc.Inline[], opts) => {
        // We want strings inside of meta data to actually be a 
        // string and not a text node (as needed in ProseMirror)
        const patchedRenderers = {
            ...renderers,
            ...stringRenderers
        }
        return (await renderContent(inlines, { ...opts, renderers: patchedRenderers })).join('');
    },
    MetaBlocks: async (blocks: Pandoc.Block[], opts) => {
        // We want strings inside of meta data to actually be a 
        // string and not a text node (as needed in ProseMirror)
        const patchedRenderers = {
            ...renderers,
            ...stringRenderers
        }
        return (await renderContent(blocks, { ...opts, renderers: patchedRenderers })).join('');
    }
}
