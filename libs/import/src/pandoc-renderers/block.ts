import { SFNodeType } from '@sciflow/schema';
import { Fragment } from 'prosemirror-model';
import { ImportError, PandocNode, PandocRenderer, ProseMirrorNode, render, renderContent } from '.';
import * as Pandoc from './pandoc-types';

/**
 * Takes a pandoc AST node and returns a ProseMirror node.
 * A good overview of Pandoc's data model is available at [Pandoc Lua Filters](https://pandoc.org/lua-filters.html).
 */
export const renderers: { [pandocNodeName: string]: PandocRenderer } = {
    // virtual root node
    'Document': async (contents: PandocNode[], opts, parentNode) => {
        return renderContent(contents, opts, parentNode);
    },
    'Note': async (noteContents: Pandoc.Block[], opts) => {
        const children = await renderContent(noteContents, opts, noteContents);
        let footnote = opts.schema.nodes[SFNodeType.footnote].create({}, children, []);
        let inlineNodes: any[] = [];
        footnote.descendants((node, pos) => {
            if (!node.type.isInline) {
                if (pos > 0) {
                    inlineNodes.push(opts.schema.nodes[SFNodeType.hardBreak].create({}));
                }
                return true;
            }
            inlineNodes.push(node);
        });

        footnote = opts.schema.nodes[SFNodeType.footnote].create({}, inlineNodes, []);
        footnote.check();
        return footnote;
    },
    'Para': async (contents: Pandoc.Inline[], opts, parentNode: PandocNode) => {

        // check for images inside the paragraph
        const children = await renderContent(contents, opts, contents);

        //const prev = parentNode?.c.indexOf()

        // if a paragraph contains a single image we asume it is a figure
        // unless it's inside a table
        // TODO we should probably find the caption above or below instead of copying the alt-text
        // TODO we should guess image type based on the parent node, not the width
        if (children.some(c => c.type.name === 'image') && children.length === 1 && children[0]?.attrs?.metaData?.width > 400) {
            let figure, figureContent;
            try {
                const figureImage = children[0];
                const caption = figureImage.attrs.alt?.length > 0 ? opts.schema.nodes[SFNodeType.paragraph].create({}, opts.schema.text(figureImage.attrs.alt)) : [];
                figureContent = [opts.schema.nodes[SFNodeType.caption].create(
                    {},
                    caption
                )];
                figure = opts.schema.nodes[SFNodeType.figure].create(
                    {
                        type: 'image',
                        src: figureImage.attrs.src, // TODO this should be a file reference, not the base64 rep
                        id: figureImage.attrs.id,
                        alt: figureImage.attrs.alt
                    },
                    figureContent
                );
                figure.check();
                return figure;
            } catch (e) {
                throw new ImportError('Could not render figure from image', {
                    message: e.message,
                    pandocNode: JSON.stringify(contents[0]),
                    parentNodeType: parentNode?.t,
                    figure,
                    figureContent
                });
            }
        }

        try {
            const para = opts.schema.nodes[SFNodeType.paragraph].create(
                {},
                children, []
            );
            para.check();
            return para;
        } catch (e) {
            throw new ImportError('Could not render paragraph', {
                message: e.message,
                pandocNode: JSON.stringify(children),
                parentNodeType: parentNode?.t
            });
        }
    },
    Quoted: ({ c }, opts) => {
        try {
            const [type, content] = c;
            return renderContent(content, opts);
        } catch (e) {
            throw new ImportError(e.message, { type: 'Quoted' });
        }
    },
    RawBlock: ({ c }, opts) => {
        const [type, command] = c;
        if (type === 'tex') {
            if (command === '\\newpage') {
                return opts.schema.nodes[SFNodeType.pageBreak].create();
            }
        }
    },
    'Plain': async (contents: Pandoc.Inline[], opts) => {
        const para = opts.schema.nodes[SFNodeType.paragraph].create(
            {},
            await renderContent(contents, opts, contents), []
        );
        para.check();
        return para;
    },
    'OrderedList': async (list: Pandoc.OrderedListContents, opts) => {
        const [type, content] = list;
        // TODO store the type of list

        let listContent: any[] = [];
        for (let items of content) {
            listContent.push(await opts.schema.nodes[SFNodeType.list_item].create(
                {},
                await renderContent(items, opts, list)
            ));
        }

        return opts.schema.nodes[SFNodeType.ordered_list].create(
            {},
            listContent
        )
    },
    'BulletList': async (items: Pandoc.BulletListContents, opts, parentNode) => {
        const renderedItems = await Promise.all(items.map(
            async (item) => opts.schema.nodes[SFNodeType.list_item].create(
                {},
                await renderContent(item, opts, items)
            )
        ));
        const list = opts.schema.nodes[SFNodeType.bullet_list].create(
            {},
            renderedItems
        )
        return list;
    },
    'DefinitionList': async (items, opts) => {
        // https://pandoc.org/lua-filters.html#type-definitionlist
        const renderedItems = await Promise.all(items.map(
            async (item) => opts.schema.nodes[SFNodeType.paragraph].create(
                {},
                await renderContent(item[0], opts, items)
            )
        ));

        return renderedItems;
    },
    'Div': async (pandocDiv: Pandoc.DivContents, opts, parentNode) => {
        // TODO this will fail when the div has multiple children
        return renderContent(pandocDiv[1], opts, parentNode);
    },
    'Header': async (header: Pandoc.HeaderContents, opts) => {
        const headingType = opts.schema.nodes[SFNodeType.heading];
        const level = header[0];
        const attrs = header[1];
        // TODO get style attribute (special-heading into role attr)
        // TODO test level 2
        const isUnnumbered = attrs[1].includes('unnumbered');
        // filter out hard breaks in headings
        const children = (await renderContent(header[2], opts, header))
            .filter(n => n.type?.name !== SFNodeType.hardBreak);

        const content = Fragment.from(children);
        if (!headingType.validContent(content)) {
            const payload = { content: content?.toString()?.substring(0, 400) };
            console.error('Could not create heading', payload);
            throw new ImportError('Invalid content for heading', payload);
        }

        const heading = headingType.create({ level }, content, []);
        heading.check();
        return heading;
    },
    'Table': async (tableContents: Pandoc.TableContents, opts) => {

        let rows: ProseMirrorNode[] = [];
        const [attr, pdCaption, colspecs, pdHead, pdBodies, pdFoot] = tableContents;
        rows.push(...await Promise.all(pdHead[1].map((row: Pandoc.Row) => render(
            {
                t: '_Row',
                c: {
                    part: TablePart.Head,
                    attr: row[0],
                    cells: row[1],
                    rowHeaders: 0
                }
            },
            opts,
            tableContents
        ))));
        for (let i = 0; i < pdBodies.length; i++) {
            const rowHeaders = pdBodies[i][1];
            const bodyRows = pdBodies[i][3];
            rows.push(...await Promise.all(bodyRows.map((row: Pandoc.Row) => render(
                {
                    t: '_Row',
                    c: {
                        part: TablePart.Body,
                        attr: row[0],
                        cells: row[1],
                        rowHeaders
                    }
                },
                opts,
                row
            ))));
        }
        rows.push(...await Promise.all(pdFoot[1].map((row: Pandoc.Row) => render(
            {
                t: '_Row',
                c: {
                    part: TablePart.Foot,
                    attr: row[0],
                    cells: row[1],
                    rowHeaders: 0
                }
            },
            opts,
            row
        ))));
        const caption = await render({ t: '_Caption', c: pdCaption }, opts);
        const table = opts.schema.nodes[SFNodeType.table].createChecked({}, rows, []);
        return opts.schema.nodes[SFNodeType.figure].create(
            { type: 'native-table' },
            [table, caption]
        );
    },
    /*
    ** Pseudo-elements, which don't have a tag in the native pandoc representation
    */
    '_TableHeaderCell': (pdHeaderCell: Pandoc.Cell, opts, parentNode) => {
        return renderCell(SFNodeType.table_header, pdHeaderCell, opts, parentNode);
    },
    '_TableBodyCell': (pdBodyCell: Pandoc.Cell, opts, parentNode) => {
        return renderCell(SFNodeType.table_cell, pdBodyCell, opts, parentNode);
    },
    '_Row': async (
        pandocRowComponents: RowComponents,
        opts
    ): Promise<ProseMirrorNode> => {
        const { part, cells, rowHeaders } = pandocRowComponents;
        const defaultElementType = part == TablePart.Body
            ? '_TableBodyCell'
            : '_TableHeaderCell';
        let pmCells: ProseMirrorNode[] = await Promise.all(cells.map(async (cell, i) => {
            try {
                const cellType = i < rowHeaders ? '_TableHeaderCell' : defaultElementType;
                return await render(
                    { t: cellType, c: cell },
                    opts
                );
            } catch (e) {
                if (e instanceof ImportError) { throw e; }
                throw new ImportError('Could not render table cell ' + i, {
                    message: e.message,
                    pandocNode: JSON.stringify(cells[i])
                });
            }
        }));
        return opts.schema.nodes[SFNodeType.table_row].createChecked({}, pmCells, []);
    },
    '_Caption': async (pandocCaption: Pandoc.Caption, opts) => {
        const [_, longCaption] = pandocCaption;
        return opts.schema.nodes[SFNodeType.caption].createChecked(
            {},
            await renderContent(longCaption, opts, pandocCaption)
        );
    },
    'BlockQuote': async (pandocNode: Pandoc.BlockQuote, opts) => {
        return opts.schema.nodes[SFNodeType.blockquote].createChecked(
            {},
            await renderContent(pandocNode, opts, pandocNode)
        );
    }
};

/**
 * Type collecting all information required to render a row.
 */
interface RowComponents {
    part: TablePart,
    attr: Pandoc.Attr,
    cells: Pandoc.Cell[],
    rowHeaders: number
}


/**
 * Parts of a table; either refers to the table head, table body, or
 * table foot.
 */
const enum TablePart {
    Head,
    Body,
    Foot
};

/**
 * Converts pandoc alignment information into a `text-align` attribute
 * value.
 */
const toTextAlign = (align: Pandoc.Alignment) => {
    switch (align) {
        case Pandoc.Alignment.AlignLeft: return 'left';
        case Pandoc.Alignment.AlignRight: return 'right';
        case Pandoc.Alignment.AlignCenter: return 'center';
        default: return null;
    }
}

/**
 * Helper function for table cell rendering.
 */
const renderCell = async (
    nodeType: SFNodeType,
    cell: Pandoc.Cell,
    opts,
    _parentNode: PandocNode
): Promise<ProseMirrorNode> => {
    const cellContents = cell[4];
    const rowspan = cell[2];
    const colspan = cell[3];
    const align = toTextAlign(cell[1].t);

    try {
        const content = (await renderContent(cellContents, opts, cell)).map((x) => {
            if (x.type.name == 'paragraph') {
                x.attrs['text-align'] = align;
            }
            return x;
        });

        const node = opts.schema.nodes[nodeType].create(
            { rowspan, colspan },
            content,
            []
        );

        try {
            node.check();
            return node;
        } catch (e) {
            throw new ImportError('Could not render table cell content', {
                message: e.name + ': ' + e.message,
                type: nodeType,
                pandocNode: JSON.stringify(content),
                pmNode: node.toString()
            });
        }

    } catch (e) {
        if (e instanceof ImportError) { throw e; }
        throw new ImportError('Could not render table cell', {
            message: e.name + ': ' + e.message,
            type: nodeType,
            pandocNode: JSON.stringify(cellContents)
        });
    }
}
