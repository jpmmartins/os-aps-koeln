import { renderers as inlineRenderers } from './inline';
import { renderers as blockRenderers } from './block';
import { renderers as metaRenderers } from './meta';
import * as Pandoc from './pandoc-types';
import { SFNodeType } from '@sciflow/schema';

export class ImportError extends Error {
    constructor(message: string, public payload: any) {
        super(message);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, ImportError.prototype);
    }

    toString() {
        return `${this.name}: ${this.message}`;
    }

    toJSON() {
        return { message: this.message, payload: this.payload };
    }
}

export type ProseMirrorNode = any;
export type ProseMirrorMark = any;

export interface RendererOptions {
    renderers: {
        [key: string]: PandocRenderer
    };
    schema: any;
    marks?: ProseMirrorMark[],
    /** The absolute path to a directory that holds any assets/media referenced */
    assets?: string;
}

export type PandocRendererReturnType = ProseMirrorNode | ProseMirrorNode[];

/**
 * Renders pandoc nodes as ProseMirror objects.
 */
export interface PandocRenderer {
    (pandocNode: PandocNode, opts: RendererOptions, parentNode?: PandocNode): Promise<PandocRendererReturnType>
}

export type PandocDefaultC = PandocNode[] | string | any[];
export type PandocNode =
    | Pandoc.Inline
    | Pandoc.Block
    | Pandoc.MetaValue
    | Pandoc.Document;

export const pandocRenderers = {
    ...inlineRenderers,
    ...blockRenderers,
    ...metaRenderers
};

export const splitPartDiv = async (c, opts) => {
    let [[id, _b, attrList], content] = c;
    // check the heading1 for information as well
    const heading1 = content.find(child => child.t === 'Header' && child.c && child.c[0] === 1);
    if (heading1) {
        for (let className of heading1.c[1][1]) {
            if (className === 'unnumbered') {
                attrList = [...attrList.filter(([key]) => key !== 'numbering'), ['numbering', 'none']];
            }
        }
    }

    const attrs = attrList.reduce((obj, [key, value]) => ({ ...obj, [key.replace('data-', '')]: value }), {});
    // we ignore the refs id since we have our own way of placing a bibliography
    if (id === 'refs') { return null; }
    try {
        const node = opts.schema.nodes[SFNodeType.document].createChecked(attrs, await renderContent(content, opts));
        return node;
    } catch (e) {
        if (e instanceof ImportError) {
            throw new ImportError(e.message, e.payload);
        } else {
            throw new ImportError(e.message, { type: 'Div', attrs, id });
        }
    }
};


/** Returns an array of pandoc notes for any pandoc node array or [] if the
 * pandocNode array is not defined. */
export const renderContent = async (
    pandocNodes: PandocNode[],
    opts: RendererOptions,
    parentNode?: PandocNode
): Promise<ProseMirrorNode[]> => {

    if (!pandocNodes) { return []; }
    let renderResult: any[] = [];

    for (let child of pandocNodes) {
        try {
            const result = await render(child, opts, parentNode);
            renderResult.push(result);
        } catch (e) {
            if (e instanceof ImportError) { throw e; }
            throw new ImportError(e.message, {
                ...(e?.payload || {}),
                pandocNode: JSON.stringify(child),
                parentNodeType: parentNode?.t
                // add additional details here if needed
            });
        }
    }

    return [].concat(...renderResult);
}

/** Renders a single pandoc node */
export const render: PandocRenderer = async (
    pandocNode: PandocNode,
    opts: RendererOptions,
    parentNode?: PandocNode
): Promise<PandocRendererReturnType> => {

    if (Array.isArray(pandocNode)) {
        throw new ImportError('Expected Pandoc node but received array', { node: JSON.stringify(pandocNode).substring(0, 200), parentNodeType: parentNode?.t })
    }

    try {
        if (opts.renderers[pandocNode.t]) {
            const results = await opts.renderers[pandocNode.t](pandocNode.c, opts, parentNode);
            return Array.isArray(results) ? [].concat(...results) : results;
        }
    } catch (e) {
        if (e instanceof ImportError) { throw e; }
        throw new ImportError('Could not render Pandoc node', { type: pandocNode.t, error: e.message, ...(e.payload || {}), parentNodeType: parentNode?.t });
    }

    debugger;
    throw new ImportError('No renderer defined for ' + pandocNode.t, { type: pandocNode.t, node: JSON.stringify(pandocNode).substring(0, 200), parentNodeType: parentNode?.t, availableRenderers: Object.keys(opts.renderers) });
}
