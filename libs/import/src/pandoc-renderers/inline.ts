import { PandocNode, renderContent, PandocRenderer, ProseMirrorNode, ProseMirrorMark } from '.';
import { SFMarkType, SFNodeType, createId } from '@sciflow/schema';
import { CitationItem, SourceField } from '@sciflow/cite';
import * as Pandoc from './pandoc-types';
import { stringRenderers } from './meta';
import sharp from 'sharp';
import { existsSync, readFileSync } from 'fs';
import { extname, join } from 'path';
import { placeholder } from './error-placeholder';

/** How to handle an element that is nested below another element of the
same type. */
type NestingBehavior = (mark: ProseMirrorMark, marks: ProseMirrorMark[])
    => ProseMirrorMark[]

const ensureMark: NestingBehavior = (mark, marks) =>
    mark.addToSet(marks)

const toggleMark: NestingBehavior = (mark, marks) =>
    mark.isInSet(marks)
        ? mark.removeFromSet(marks)
        : mark.addToSet(marks);

const renderTextWithMark =
    (markType: SFMarkType, nesting: NestingBehavior): PandocRenderer => {
        return async (contents: Pandoc.Inline[], opts) => {
            const mark = opts.schema.marks[markType].create({});
            return renderContent(
                contents,
                { ...opts, marks: nesting(mark, opts.marks ?? []) }
            );
        };
    };

export const renderers: { [key: string]: PandocRenderer } = {
    Strong: renderTextWithMark(SFMarkType.strong, toggleMark),
    Emph: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    Subscript: renderTextWithMark(SFMarkType.subscript, ensureMark),
    Superscript: renderTextWithMark(SFMarkType.superscript, ensureMark),
    // The doc model doesn't support deleted, smallcaps, or underlined
    // text, so those are treated just like emphasized text.
    Underline: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    Strikeout: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    SmallCaps: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    Str: (str: string, { schema, marks }) => schema.text(str, marks ?? []),
    SoftBreak: (_, { schema, marks }) => schema.text(' ', marks ?? []),
    Space: (_, { schema, marks }) => schema.text(' ', marks ?? []),
    LineBreak: async (_, { schema, marks }) => {
        return schema.nodes[SFNodeType.hardBreak].create({}, [], marks ?? []);
    },
    Span: async (contents: [Pandoc.Attr, Pandoc.Inline[]], opts) => {
        return renderContent(contents[1], opts);
    },
    Link: (link: Pandoc.LinkContents, opts) => {
        const [attributes, content, [href, title]] = link;
        const anchorMark = opts.schema.marks[SFMarkType.anchor].create({
            href,
            title: title?.length === 0 ? null : title
        });
        // TODO if the target contains a # it should be made a xref and not an anchor mark
        return renderContent(
            content,
            { ...opts, marks: [...(opts.marks ?? [])?.filter(m => m.type.name !== 'anchor'), anchorMark] }
        );
    },
    Code: (codeContents: [Pandoc.Attr, string], { schema, marks }) => {
        const code = codeContents[1];
        return schema.text(code, marks ?? []);
    },
    RawInline: async () => {
        return [];
    },
    Quote: async (quoteContents, opts) => {
        const textMarks = opts.marks ?? []
        return [
            opts.schema.text('"', textMarks),
            await renderContent(quoteContents[1], opts),
            opts.schema.text('"', textMarks)
        ];
    },
    Cite: async (citeContents: Pandoc.CiteContents, opts) => {
        const citationData = citeContents[0];

        // we only expect primitives inside the citation data
        let source;
        if (citationData?.length > 0) {
            const citationItems: CitationItem[] = await Promise.all(citationData.map(async (data) => {
                let item: CitationItem = {
                    id: data.citationId
                };

                // TODO implement citationMode: NormalCitation, AuthorInText

                if (data.citationPrefix?.length > 0) {
                    item.prefix = (await renderContent(data.citationPrefix, { ...opts, renderers: stringRenderers })).join('');
                }

                if (data.citationSuffix?.length > 0) {
                    item.suffix = (await renderContent(data.citationSuffix, { ...opts, renderers: stringRenderers })).join('');
                }

                return item;
            }));
            source = encodeURI(SourceField.toString({
                citationItems,
                properties: {
                    noteIndex: 0
                }
            }));
        }

        const inlines = citeContents[1];
        const text = await renderContent(
            inlines,
            opts
        );
        return opts.schema.nodes[SFNodeType.citation].createChecked({
            source
        }, text);
    },
    Math: (mathContents: Pandoc.MathContents, { schema, marks }) => {
        const mathTeX = mathContents[1]
        return schema.nodes[SFNodeType.math].createChecked(
            { tex: mathTeX },
            [],
            []
        );
    },
    Image: async (imageContents: Pandoc.LinkContents, opts) => {
        const [meta, alt, data] = imageContents;
        const altText = (await renderContent(alt, { ...opts, renderers: stringRenderers })).join('');
        let imagePath = data[0];
        let id = createId();
        let imagePlaceholder = '';
        let metaData;
        if (opts.assets != undefined && imagePath?.length > 0) {
            try {
                if (!imagePath.includes(opts.assets)) {
                    // we did not receive an absolute path
                    imagePath = join(opts.assets, imagePath);
                }
                const localFilePath = imagePath.replace(opts.assets + '/', '');
                id = localFilePath//?.replaceAll('/', '-');
                let image;
                try {

                    // try to see if a .svg file has been placed instead
                    switch (extname(imagePath)) {
                        case '.emf':
                            if (existsSync(imagePath.replace('.emf', '.svg'))) {
                                imagePath = imagePath.replace('.emf', '.svg');
                                id = id.replace('.emf', '.svg');
                            } else {
                                throw new Error('No svg alternative found for .emf at ' + imagePath.replace('.emf', '.svg'));
                            }
                            break;
                        case '.wmf':
                            if (existsSync(imagePath.replace('.wmf', '.svg'))) {
                                imagePath = imagePath.replace('.wmf', '.svg');
                                id = id.replace('.wmf', '.svg');
                            } else {
                                throw new Error('No svg alternative found for .wmf at ' + imagePath.replace('.wmf', '.svg'));
                            }
                            break;
                    }

                    image = await sharp(readFileSync(imagePath)).withMetadata();
                    metaData = await image.metadata();

                    image = await image
                        .resize({ width: 480, withoutEnlargement: true })
                        .flatten({ background: 'white' })
                        .webp({ lossless: false, quality: 65 })
                        .toBuffer();

                    const thumb = image.toString('base64');
                    imagePlaceholder = `data:image/webp;base64,${thumb}`;
                } catch (e) {
                    console.error('Could not transform image', { message: e.message, altText, imagePath });
                    imagePlaceholder = placeholder;
                }
            } catch (e) {
                console.error('Could not transform asset', e);
            }
        }
        return opts.schema.nodes[SFNodeType.image].createChecked({ src: imagePlaceholder, alt: altText, id, metaData });
    }
}
