import { DocumentSnapshotPart, schemas } from '@sciflow/schema';
import { SFNodeType } from '@sciflow/schema';
import { PandocNode, renderContent, render, ProseMirrorNode, ImportError } from './pandoc-renderers';
import { pandocRenderers, splitPartDiv } from './pandoc-renderers';
import { DOMSerializer } from 'prosemirror-model';
import { MetaList, MetaMap, TaggedObject } from './pandoc-renderers/pandoc-types';
import { JSDOM } from 'jsdom';

const dom = new JSDOM();
const virtualDocument = dom.window.document;

const schema = schemas.manuscript;

/** Extacts the reference meta data */
const extractReferences = async (meta: { references?: MetaList }): Promise<any[]> => {
    if (!meta?.references) {
        return [];
    }

    return render(meta.references, { renderers: pandocRenderers, schema });
};

const proseMirrorToHTML = async (proseMirrorDocument, schema) => {
    const serializer2 = DOMSerializer.fromSchema(schema);
    const content = serializer2.serializeFragment(proseMirrorDocument.content, { document: virtualDocument });
    const el = virtualDocument.createElement('div');
    el.append(content);
    return el.innerHTML;
}

/**
 * Extracts the document and meta data from a Pandoc JSON document.
 * @param pandocDocument the document as it comes from pandoc -t jsonma
 * @param splitParts split into parts/chapters or do a linear import
 */
const parsePandocAST = async (pandocDocument: { blocks: PandocNode[], meta?: any; }, mediaDir?: string, splitParts = false) => {

    let errors: any[] = [];
    if (splitParts) {
        pandocRenderers['Div'] = splitPartDiv;
    }

    let title: string = 'Untitled document';

    try {
        // (!) meta inlines do not render as ProseMirror but json / primitive types
        let titleContent: ProseMirrorNode[] = [schema.text(title)];
        if (pandocDocument.meta?.title?.c) {
            titleContent = await renderContent(pandocDocument.meta?.title?.c, { renderers: pandocRenderers, schema });
            if (titleContent.some(c => c.isBlock)) {
                // collapse block nodes
                titleContent = titleContent.map(n => (n.content || [])).flat();
            }
            title = await render(pandocDocument.meta.title, { renderers: pandocRenderers, schema });
        }

        const references = pandocDocument.meta?.references ? await extractReferences(pandocDocument.meta) : [];

        let content: ProseMirrorNode[] = [];
        try {
            // render virtual root node
            const root = { t: 'Document', c: pandocDocument.blocks };
            content = await render(root, {
                renderers: pandocRenderers,
                schema,
                assets: mediaDir
            }, root);
        } catch (e) {
            if (e instanceof ImportError) {
                errors.push(e);
            } else {
                throw e;
            }
        }

        let header;

        try {
            header = schema.nodes[SFNodeType.header].create({}, [
                schema.nodes[SFNodeType.heading].create({ id: 'title', role: 'title', level: 1 }, titleContent)
            ]);
            header.check();
        } catch (e) {
            console.error(e);
            header = schema.nodes[SFNodeType.header].create({}, [
                schema.nodes[SFNodeType.heading].create({ id: 'title', role: 'title', level: 1 }, [schema.text(title)])
            ]);
        }

        let doc, parts;
        if (splitParts) {
            parts = content;
            parts.forEach(part => part.check());
        } else {
            doc = schema.nodes[SFNodeType.document].create({}, [header, ...content]);
            try {
                doc.check();
            } catch (e) {
                debugger;
                console.error(e);
                throw e;
            }
        }

        return {
            title,
            metaData: {},
            doc,
            parts,
            references,
            schema,
            errors
        };

    } catch (e) {
        console.error(e);
        return {
            title: null,
            metaData: {},
            doc: null,
            references: null,
            schema: null,
            errors: [...errors, e]
        }
    }
};

export {
    parsePandocAST,
    proseMirrorToHTML,
    extractReferences
}