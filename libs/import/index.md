# Libraries

## Mandatory

The main library needed to import DOCX is Pandoc.

## Optional

We use libemf2svg to transform extracted emf files into SVG.
