# Infrastructure setup

There are no scripts for all platforms and cloud environments at this point that will setup a Kubernetes cluster that works in all development setups (with difference between Windows,Mac and Linux).

Depending on your setup you may want to skip installing one or the other component. If you are running this on a baremetal Linux cluster you will need additional components like MetalLB to enable networking.

So here are the comonents you will need to have running:

## Development environment setup

This guide asumes that you have basic knowledge of Kubernetes. Keep in mind that the entire setup might need some time in the background for all images to be downloaded. You can monitor progress using the command line or a tool like [Lens](https://k8slens.dev/).

To get the app running you will need access to a Kubernetes cluster (e.g. through Docker Desktop) as well has kubectl and helm 3 on your system.

We will set up:

 * Routing with a local (and valid) ssl certificate
 * S3 storage
 * OpenFaaS runner

## A router for traefik.me

We use [traefik](https://traefik.io/) for routing. For local setup we will use the domain traefik.me to get valid SSL certificates out of the box:

```
wget --quiet traefik.me/cert.pem -O tls.crt
wget --quiet traefik.me/privkey.pem -O tls.key
kubectl create secret generic traefik-me-default-certs \
  --from-file=./tls.crt \
  --from-file=./tls.key
```

To get a simple traefik setup running install the helm chart (from the root of our repository):

```
helm upgrade -i traefik traefik/traefik \
  --version 10.9.1 -f ./infrastructure/sfo/traefik.values.yml \
  --set persistence.enabled=false,deployment.initContainers=null
```

Then make our custom ssl certificate known to Traefik.

```
cat <<EOF | kubectl apply -f -
    apiVersion: traefik.containo.us/v1alpha1
    kind: TLSStore
    metadata:
      name: default
    spec:
      defaultCertificate:
        secretName: traefik-me-default-certs
EOF
```

## OpenFaaS runner (exporter/importer etc.)

We'll also want to prepare a namespace to run our code through [OpenFaaS](https://www.openfaas.com/):
At this point, going to https://openfaas.traefik.me/ should show a 404 (but a valid SSL certificate for traefik.me).

```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Namespace
metadata:
  name: openfaas
  annotations:
    linkerd.io/inject: enabled
    config.linkerd.io/skip-inbound-ports: "4222"
    config.linkerd.io/skip-outbound-ports: "4222"
  labels:
    role: openfaas-system
    access: openfaas-system
    istio-injection: enabled
EOF
kubectl annotate --overwrite namespace/openfaas openfaas="1"
```

Let's now install OpenFaaS:

```
helm upgrade -i openfaas openfaas/openfaas \
  --namespace=openfaas \
  --version 10 \
  -f ./infrastructure/sfo/openfaas.values.yml
```

To make OpenFaaS available to receive functions we create an Ingress:

```
kubectl apply -f ./infrastructure/sfo/openfaas-ingress.yml -n openfaas
```

At this point, navigating to [https://openfaas.traefik.me](https://openfaas.traefik.me) will prompt for a user and password.

The user is admin, the password you can get with the following (which will also login your local faas-cli - you may need to [install it](https://github.com/openfaas/faas-cli))

```
export OPENFAAS_URL=https://openfaas.traefik.me
export OPENFAAS_PASSWORD=$(kubectl get secret -n openfaas basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode; echo)
echo -n $OPENFAAS_PASSWORD | faas-cli login --tls-no-verify --gateway $OPENFAAS_URL --username admin --password-stdin
echo $OPENFAAS_PASSWORD
```

## S3 storage

The app asumes S3 storage to be available, so locally we can install minio:

```
helm upgrade minio minio/minio -i \
--namespace minio \
--create-namespace \
--set rootUser=REPLACEME,rootPassword=REPLACEME,replicas=4,persistence.size=1Gi,resources.requests.memory=1Gi
```

No let's store the S3 access key and secret for later

```
export ACCESSKEY=$(kubectl get secret -n minio minio -o jsonpath="{.data.rootUser}" | base64 --decode; echo)
export SECRETKEY=$(kubectl get secret -n minio minio -o jsonpath="{.data.rootPassword}" | base64 --decode; echo)
export NAMESPACE=sfo
```

and store them in a secret inside our namespace 'sfo' (which is only the default we are using here). We will need this secret for the openfaas namespace as well (since the runners need access to S3)

Minio should be accessible through https://minio.traefik.me

```
kubectl create secret generic s3-access-key \
  --from-literal s3-access-key=$ACCESSKEY \
  --namespace $NAMESPACE

  kubectl create secret generic s3-access-key \
  --from-literal s3-access-key=$ACCESSKEY \
  --namespace openfaas

kubectl create secret generic s3-access-secret \
  --from-literal s3-access-secret=$SECRETKEY \
  --namespace $NAMESPACE

kubectl create secret generic s3-access-secret \
  --from-literal s3-access-secret=$SECRETKEY \
  --namespace openfaas
```

We did not add an ingress for minio since our app will use internal routing to access it. But if you wanted to, you could forward port 9001 on the minio service (e.g. through Lens) and then use the web ui there.

We are now ready to install the runners, app and main API server.

# Deploy the app and services

Let's setup variables from the basic setup (replace them with other services if you did not follow the setup).

```
export DOMAIN=traefik.me
export OPENFAAS_URL=https://openfaas.${DOMAIN}
export S3_ENDPOINT=http://minio.minio.svc.cluster.local:9000
export ACCESSKEY=$(kubectl get secret -n minio minio -o jsonpath="{.data.rootPassword}" | base64 --decode; echo)
export SECRETKEY=$(kubectl get secret -n minio minio -o jsonpath="{.data.rootUser}" | base64 --decode; echo)
export NAMESPACE=sfo
export OPENFAAS_URL=https://openfaas.${DOMAIN}
export BRANCH=development
export CI_COMMIT_REF_SLUG=$BRANCH
```

We'll be using the latest development images for our setup (see BRANCH variable).

First let's use helm to install the api server and app

```
echo -e "\n✅ Installing release for ${BRANCH}\n"
helm upgrade \
    -i sfo-stack ./infrastructure/sfo \
    -f infrastructure/sfo/values.yml \
    -n $NAMESPACE \
    --set image.tag=$BRANCH,namespace=${NAMESPACE},ingress.domain=${DOMAIN},accessKey.password=$ACCESSKEY,minio.secretKey.password=$SECRETKEY,podAnnotations.commit_sha=$(date +"%d-%m-%y-%T")
```

Now deploy the runners (for import/export etc.). To do that we cd into the runner directory and make sure we're logged into the faas-cli.

```
cd runners
OPENFAAS_PASSWORD=$(kubectl get secret -n openfaas basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode; echo)
echo -n $OPENFAAS_PASSWORD | faas-cli login --tls-no-verify --gateway $OPENFAAS_URL --username admin --password-stdin
```

To deploy:

```
faas-cli deploy \
    --tls-no-verify \
    --gateway $OPENFAAS_URL \
    -f functions.yml \
    --namespace $NAMESPACE \
    --env write_debug=true
```

Your https://openfaas.traefik.me dashboard should now show runners for the $NAMESPACE (e.g. SFO). To deploy new versions of either the runners or the app stack all commands from this file can be repeated individually.