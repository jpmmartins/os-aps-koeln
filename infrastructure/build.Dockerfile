FROM docker:20.10.21-alpine3.16

COPY --from=docker/buildx-bin:latest /buildx /usr/libexec/docker/cli-plugins/docker-buildx
RUN docker buildx version
RUN apk update && apk add --no-cache --virtual .gyp \
    nodejs \
    python3 \
    py3-pip \
    scons \
    py3-psutil \
    py3-yaml \
    py3-cheetah \
    git \
    gcc \
    g++ \
    make \
    openssl-dev \
    linux-headers \
    cmake \
    curl-dev \
    yarn \
    openssh

RUN node --version

RUN echo -e "http://dl-cdn.alpinelinux.org/alpine/edge/testing" > /etc/apk/repositories
#RUN yarn global add node-gyp

RUN rm -rf /var/cache/apk/*
