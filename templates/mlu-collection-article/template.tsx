import { TSX, createComponentInstance, SciFlowDocumentData, TemplateComponent } from '@sciflow/export';
const JournalTitle = createComponentInstance({
    kind: 'JournalTitle',
    styles: `
    .Title {
        font-family: 'Work Sans', sans-serif;
        break-before: right;
        h1 {
            margin: 0;
            padding: 0;
            font-size: 2rem;
        }
        margin-bottom: 4rem;
    }
    .authors {
        padding: 0;
        font-weight: bold;
        margin-top: 0;
    }
    `,
    renderDOM() {
        // TODO find a better way to access the component data
        const instance = this as TemplateComponent<any, SciFlowDocumentData>;
        const authors = instance.engine.data.document.authors;
        const getName = (author) => {
            if (!author) { return 'Unknown author'; }
            if (author.firstName || author.lastName) {
                return [author.firstName, author.lastName].filter(n => n !== null).join(' ');
            }
            if (author.name) { return author.name; }
            return 'Unknown author';
        };
        const authorSection = <section class="authors">{
            ...authors.map((author, index) => <span class="author">{getName(author)}</span>)
        }</section>;

        return {
            id: 'journal-title',
            placement: 'body',
            dom: <section class="Title">
                {authorSection}
                <h1>{instance.engine.data.document.title}</h1>
            </section>
        };
    }
});

export const customRenderers = {};
export const customTemplateComponents = [JournalTitle];
export const stylePaths = [
    { path: 'styles.scss', position: 'start' },
    { path: 'overrides.scss', position: 'end' }
];
